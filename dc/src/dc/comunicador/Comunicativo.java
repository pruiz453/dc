package dc.comunicador;

/**
 * <p>Interfaz que ha de implementar toda clase con la prentensión de comunicarse mediante un
 * {@link Comunicador}.</p>
 *
 * <p>Además deberá poseer un atributo {@link Comunicador} e implementar un
 * Selector {@link Comunicativo#getComunicador()}.</p>
 *
 * <p>Para facilitar el acceso al {@link Comunicador} la interfaz proporciona un conjunto de
 * métodos <i>default</i> que permiten aplicar los métodos de {@link Comunicador} sobre la
 * instancia  {@link Comunicativo}, aportando además la posibilidad de omitir objetos
 * {@link dc.comunicador.Comunicador.Transformación} (usarán transformaciones triviales) y
 * omitir el canal, usando un canal por defecto.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface Comunicativo {

	/**
	 * Selector de la instancia {@link Comunicador}.
	 *
	 * <p>No es necesario utilizarlo gracias a los métodos <i>default</i> de
	 * {@link Comunicativo}</p>
	 *
	 * @return {@link Comunicador}
	 */
	Comunicador getComunicador();

	/**
	 * Método procesador de Mensajes recibidos.
	 *
	 * <p>La devolución determina el reenvío del mensaje ({@link Reacción#Propagar}),
	 * su bloqueo ({@link Reacción#Bloquear}) o simplemente no hacer nada
	 * ({@link Reacción#No_Propagar}).</p>
	 *
	 * @param mensaje Mensaje polimórfico recibido.
	 * @return {@link Reacción} del {@link Comunicativo} receptor.
	 */
	Reacción procesarMensaje(Object mensaje);


	///  Métodos de Comunicador usando default ///

	default void comunicar(String canal, Object mensaje) {
		getComunicador().comunicar(canal,mensaje);
	}

	default void agregarAcción(String canal, Comunicador.Acción acción) {
		getComunicador().agregarAcción(canal,acción);
	}

	default void subscribir(String canal, Comunicativo comunicativo){
		subscribir(canal,comunicativo, mensaje -> mensaje);
	}

	default void subscribir(String canal, Comunicativo receptor, Comunicador.Transformación transformación){
		getComunicador().subscribir(canal,receptor,transformación);
	}

	default void emisiónGlobal(String canal, String palabraClave){
		getComunicador().emisiónGlobal(canal,palabraClave);
	}

	default void subscripciónGlobal(String palabraClave){
		subscripciónGlobal(palabraClave, mensaje->mensaje);
	}

	default void subscripciónGlobal(String palabraClave, Comunicador.Transformación transformación){
		getComunicador().subscripciónGlobal(palabraClave,transformación);
	}

	default void eliminarAcción(String canal, Comunicador.Acción acción) {
		getComunicador().eliminarAcción(canal,acción);
	}

	default void eliminarSubscripción(String canal,Comunicativo comunicativo){
		getComunicador().eliminarSubscripción(canal,comunicativo);
	}

	default void eliminarEmisiónGlobal(String canal, String palabraClave){
		getComunicador().eliminarEmisiónGlobal(canal,palabraClave);
	}

	default void eliminarSubscripciónGlobal(String palabraClave){
		getComunicador().eliminarSubscripciónGlobal(palabraClave);
	}


	///  Usando canal por defecto ///

	default String getCanalDefecto() {
		return "DC";
	}

	default void comunicar(Object mensaje) {
		getComunicador().comunicar(getCanalDefecto(),mensaje);
	}

	default void agregarAcción(Comunicador.Acción acción) {
		getComunicador().agregarAcción(getCanalDefecto(),acción);
	}

	default void subscribir(Comunicativo comunicativo){
		subscribir(getCanalDefecto(),comunicativo, mensaje -> mensaje);
	}

	default void subscribir(Comunicativo receptor, Comunicador.Transformación transformación){
		getComunicador().subscribir(getCanalDefecto(),receptor,transformación);
	}

	default void emisiónGlobal(String palabraClave){
		getComunicador().emisiónGlobal(getCanalDefecto(),palabraClave);
	}

	default void eliminarAcción(Comunicador.Acción acción) {
		getComunicador().eliminarAcción(getCanalDefecto(),acción);
	}

	default void eliminarSubscripción(Comunicativo comunicativo){
		getComunicador().eliminarSubscripción(getCanalDefecto(),comunicativo);
	}

	default void eliminarEmisiónGlobal(String palabraClave){
		getComunicador().eliminarEmisiónGlobal(getCanalDefecto(),palabraClave);
	}

}
