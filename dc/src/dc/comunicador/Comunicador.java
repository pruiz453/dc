package dc.comunicador;

import java.util.*;

/**
 * <p>Permite almacenar los objetos {@link Comunicador.Transformación} a aplicar al mensaje de los
 * mensajes junto a sus correspondientes instancias {@link Comunicativo}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
class Conexión {
	private final Comunicativo comunicativo;
	private final Comunicador.Transformación transformación;


	/**
	 * Constructor
	 *
	 * @param comunicativo
	 * @param transformación {@link Comunicador.Transformación} Se aplicará al mensaje de los mensajes.
	 */
	public Conexión(Comunicativo comunicativo, Comunicador.Transformación transformación) {
		this.comunicativo = comunicativo;
		this.transformación = transformación;
	}

	@Override
	public int hashCode() {
		return comunicativo.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Conexión) {
			return comunicativo.equals(((Conexión)obj).comunicativo);
		}
		return false;
	}

	/// Selectores y Mutadores ///

	Comunicativo getComunicativo() {
		return comunicativo;
	}

	Comunicador.Transformación getTransformación() {
		return transformación;
	}
}

/**
 * <p>Instancia que almacena un mensaje de tipo polimórfico ({@link java.lang.Object})
 * que será transmitido entre clases
 * {@link Comunicativo}, pudiendo ser potencialmente transformado por una
 * {@link Comunicador.Transformación}.</p>
 *
 * <p>Posee un conjunto de conexiones {@link Conexión}, que alberga a cada destinatario
 * {@link Comunicativo}, que recibirá el mensaje, y su {@link Comunicador.Transformación}
 * asociada. Además posee un conjunto de destinatarios {@link Comunicativo} alcanzados,
 * inicialmente vacío, que evita que un mensaje llegue repetidamente
 * al mismo {@link Comunicativo}.</p>
 *
 * <p>La instancia está asociada a un canal por el que es transmitida.</p>
 *
 * <p>La transmisión del mensaje puede ser bloqueada llamando a
 * {@link Sobre#setBloqueado(boolean)}</p>
 *
 * @author Pablo Ruiz Sánchez
 */
class Sobre {
	private Set<Conexión> conexiones;
	private Set<Comunicativo> destinatariosAlcanzados;
	private Object mensaje;
	private String canal;
	private boolean bloqueado = false;

	/**
	 * Constructor Defecto.
	 *
	 * @param canal Canal de transmisión.
	 * @param remitente Emisor original.
	 * @param mensaje Mensaje a transmitir.
	 * @param conexiones Conjunto de {@link Conexión} que recoge los destinatarios
	 *                   y sus {@link Comunicador.Transformación}
	 */
	public Sobre(String canal, Comunicativo remitente, Object mensaje, Set<Conexión> conexiones) {
		this.canal = canal;
		destinatariosAlcanzados = new HashSet<>();
		destinatariosAlcanzados.add(remitente);
		this.conexiones = conexiones;
		this.mensaje = mensaje;
	}


	/**
	 * Agregamos un destinatario que ya ha recibido el mensaje.
	 * @param destinatario {@link Comunicativo} alcanzado.
	 */
	public void agregarDestinatarioAlcanzado(Comunicativo destinatario) {
		destinatariosAlcanzados.add(destinatario);
	}


	/// Selectores y Mutadores ///


	public boolean bloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getCanal() {
		return canal;
	}

	public Set<Conexión> getConexiones() {
		return conexiones;
	}

	public void setConexiones(Set<Conexión> conexiones) {
		this.conexiones = conexiones;
	}

	public Set<Comunicativo> getDestinatariosAlcanzados() {
		return destinatariosAlcanzados;
	}

	public Object getMensaje() {
		return mensaje;
	}

	public void setMensaje(Object mensaje) {
		this.mensaje = mensaje;
	}
}

/**
 * <p>Mecanismo de comunicación de clases java por paso de mensajes.</p>
 *
 * <p>La comunicación transcurre a través de Canales que se identifican por cadenas de caracteres.
 * Las clases que deseen comunicarse implementarán la interfaz {@link Comunicativo}</p>
 *
 * <IMG SRC="../../Imágenes/dc.comunicador.jpg"/>
 *
 * <p>Existen tres modos de comunicación: local, global o por acciones.</p>
 *
 * <br><br><br>
 * <p><b>COMUNICACIÓN LOCAL</b></p>
 *
 * <p>Permite la comunicación directa entre dos instancias.</p>
 *
 * <p>comunicativoB.subscribir("Canal Ejemplo", comunicativoA);</p>
 *
 * <p>A partir de dicho momento, cada vez que comunicativoB emita un mensaje en dicho canal:</p>
 *
 * <p>comunicar("Canal Ejemplo", mensaje);</p>
 *
 * <p>El método procesarMensaje(Object mensaje) de comunicativoA será invocado.</p>
 *
 * <p>Las Subscripciones se implementarán por Mapas Hash, que dado un Canal, devuelve el conjunto de todos los
 * Comunicativos subscritos al mismo.</p>
 *
 * <br><br><br>
 * <p><b>COMUNICACIÓN GLOBAL</b></p>
 *
 * <p>Paso de mensajes entre instancias sin visibilidad y la comunicación estática entre clases. Además de poseer
 * canales, como la local, posee Palabras Clave, que constituyen un subcanal. Para comenzar a emitir mensajes globales 
 * debe ejecutar:</p>
 *
 * <p>emisiónnGlobal("Canal Ejemplo", "Palabra Clave Ejemplo");</p>
 *
 * <p>Por otro lado, para recibir mensajes:</p>
 *
 * <p>subscripciónGlobal("Palabra Clave Ejemplo");</p>
 *
 * <p>De esta forma, cuando comunicativoB lleve a cabo una comunicación:</p>
 *
 * <p>comunicar("Canal Ejemplo", mensaje);</p>
 *
 * <p>El Comunicador obtiene todas las Palabras Clave asociadas a "Canal Ejemplo" mediante
 * {@link Comunicador#mapaPalabrasClave},
 * un Mapa Hash, y envía un mensaje a todos los Comunicativo subscritos a dichas Palabras Clave.</p>
 *
 * <p>Para la comunicación estática entre todos los elementos de una clase, basta que se realice la subscripción
 * global en la creación de cada instancia. Cada una de ellas podría emitir cambios en dicho subcanal,
 * quizás solo un subconjunto o incluso podría delegarse la emisión en terceros.</p>
 *
 * <p>Para la elaboración de esta clase No se ha tenido en cuenta las posibles vulnerabilidades de Seguridad.
 * De forma que cualquier clase Comunicativa conocedora de una Palabra Clave podría emitir mensajes alterando
 * las clases subscritas y, potencialmente, el correcto funcionamiento de la aplicación.</p>
 *
 * <br><br><br>
 * <p><b>COMUNICACIÓN POR ACCIONES</b></p>
 *
 * <p>Éste constituye el mecanismo más genérico de comunicación del sistema, pues permite la comunicación de clases
 * Comunicativas con elementos que no implementan dicha interfaz, potencialmente externos. 
 * Una Acción es un fragmento de código asociado a un Canal, que se ejecuta cuando el Comunicativo realice
 * una comunicación en el mismo.</p>
 *
 * <p>Supongamos que el comunicativoB desea que se imprima por pantalla  los mensajes que envía. Deberá ejecutar:</p>
 *
 * <p>agregarAcción("Canal Ejemplo", mensaje -> System.out.println(mensaje.toString()) );</p>
 *
 * <p>De manera que al realizar una comunicación, previo al envío de mensaje, el Comunicador obtiene el conjunto de
 * Acciones asociada al canal mediante mapaAcciones, un Mapa Hash, y las ejecuta, pasando
 * como argumento el mensaje.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Comunicador {

	/**
	 * <p>Transforma un objeto {@link java.lang.Object}.</p>
	 *
	 * @author Pablo Ruiz Sánchez
	 */
	public interface Transformación {
		Object transformar(Object valor);
	}

	/**
	 * <p>Ejecución de código ante invocaciones de {@link Comunicador#comunicar(String, Object)}</p>
	 *
	 * <p>Permite el funcionamiento del Modo de comunicación <b>Acciones</b>.</p>
	 * 
	 * @author Pablo Ruiz Sánchez
	 */
	public interface Acción {
		void ejecutar(Object valor);
	}

	/// Campos estáticos ///

	/**
	 * Asoción entre palabras clave y {@link Conexión}, 
	 * que se divide en {@link Comunicativo} y {@link Comunicador.Transformación}
	 * 
	 * <p><b>Comunicación Global</b></p>
	 */
	private static final Map<String, Set<Conexión>> subcripcionesGlobales = new HashMap<>();

	/**
	 * Las instancias tomarán su {@link Comunicador#modoDepuración} de este valor al crearse.
	 */
	private static boolean modoDepuraciónGlobal = false;

	/// Campos de instancia ///

	/**
	 * Asociación entre Canales y Palabras Clave.
	 */
	private final Map<String, Set<String>> mapaPalabrasClave = new HashMap<>();


	/**
	 * Asociación entre canales y y {@link Conexión}, 
	 * que se divide en {@link Comunicativo} y {@link Comunicador.Transformación}.
	 * 	 
	 * 	<p><b>Comunicación Local</b></p>
	 */
	private final Map<String, Set<Conexión>> subcripciones = new HashMap<>();

	/**
	 * Asociación entre canales y el conjunto de {@link Comunicador.Acción} asociadas.
	 * Los miembros del conjunto serán ejecutados al enviar un mensaje: ejecución de
	 * {@link Comunicador#comunicar(String, Object)}.
	 */
	private final Map<String, Set<Acción>> mapaAcciones = new HashMap<>();

	/**
	 * Al activarse, imprime mensajes por pantalla con cada emisión de mensajes.
	 */
	private boolean modoDepuración;

	/**
	 * Comunicativo al que está asociado el Comunicador
	 */
	private Comunicativo remitente;

	/**
	 * Constructor
	 * @param remitente Objeto {@link Comunicativo} asociado al {@link Comunicador}.
	 */
	public Comunicador(Comunicativo remitente) {
		this.remitente = remitente;
		modoDepuración = modoDepuraciónGlobal;
	}

	/**
	 * Conjunto de {@link Conexión} globales de un determinado canal.
	 * 
	 * @param canal
	 * 
	 * @return Conjunto de {@link Conexión}. <i>Retorno No Nulo</i>.
	 */
	private Set<Conexión> conexionesGlobales(String canal) {
		Set<Conexión> destinatarios = new HashSet<>();

		Set<String> palabrasClave = mapaPalabrasClave.get(canal);

		if(palabrasClave != null) {
			for(String palabra : palabrasClave) {
				Set<Conexión> destinatariosPalabra = subcripcionesGlobales.get(palabra);

				if(destinatariosPalabra != null) {
					destinatarios.addAll(destinatariosPalabra);
				}
			}
		}

		return destinatarios;
	}

	/**
	 * Conjunto de {@link Conexión} globales y locales de un determinado canal.
	 *
	 * @param canal
	 * @return Conjunto de {@link Conexión}. <i>Retorno No Nulo</i>.
	 */
	private Set<Conexión> conexiones(String canal) {
		Set<Conexión> destinatarios = conexionesGlobales(canal);

		Set<Conexión> destinatariosLocales = subcripciones.get(canal);

		if(destinatariosLocales != null) destinatarios.addAll(destinatariosLocales);

		return destinatarios;
	}

	/**
	 * Envía un mensaje, que posee asociado un conjunto de destinatarios subscritos al canal
	 * local o globalmente.
	 *
	 * <IMG SRC="../../Imágenes/enviarMensaje.jpg"/>
	 *
	 * @param sobre {@link Sobre}
	 */
	private void enviarMensaje(Sobre sobre) {

		if(modoDepuración) {
			//Hallamos destinatarios no alcanzados
			Set<Comunicativo> destinatarios = new HashSet<>();

			for(Conexión conexión : sobre.getConexiones()) {
				Comunicativo destinatario = conexión.getComunicativo();
				if(! sobre.getDestinatariosAlcanzados().contains(destinatario))
					destinatarios.add(destinatario);
			}

			if(destinatarios.size() > 0) {
				System.out.println("----------");
				System.out.println("Envío de Mensaje");
				System.out.println("De: " + remitente);
				System.out.println("Por: " + sobre.getCanal());
				System.out.println("Mensaje: " + sobre.getMensaje());
				for(Comunicativo destinatario : destinatarios)
					System.out.println("A: " + destinatario);

				System.out.println("----------");
				System.out.println();
			}
		}

		Set<Acción> acciones = mapaAcciones.get(sobre.getCanal());

		if(acciones != null) {
			for(Acción acción : acciones) {
				acción.ejecutar(sobre.getMensaje());
			}
		}

		for(Conexión conexión : sobre.getConexiones()) {
			//Si el mensaje ha sido bloqueado, interrumpimos el envío.
			if(sobre.bloqueado()) {
				return;
			}

			if(! sobre.getDestinatariosAlcanzados().contains(conexión.getComunicativo())) {

				Object mensajeTransformado = conexión.getTransformación().transformar(sobre.getMensaje());

				//Depuración
				if(modoDepuración)
					System.out.println(remitente + " -> " +
							mensajeTransformado + " -> " +
							conexión.getComunicativo() + "\n");

				//Enviamos el mensaje
				Reacción reacción = conexión.getComunicativo().procesarMensaje(mensajeTransformado);

				//Incluimos el destinatario alcanzado
				sobre.agregarDestinatarioAlcanzado(conexión.getComunicativo());

				//Reaccionamos
				//En caso de Reacción.No_Propagar no realizamos acción alguna.
				switch (reacción) {
					case Propagar:
						//El mensaje debe ser propagado con el mensaje transformado
						Object mensajeAnterior = sobre.getMensaje();
						sobre.setMensaje(mensajeTransformado);

						conexión.getComunicativo().getComunicador().reenviarMensaje(sobre);

						sobre.setMensaje(mensajeAnterior);
						break;

					case Bloquear:
						sobre.setBloqueado(true);
						return;
				}
			}
		}
	}

	/**
	 * <p><b>Reenvío de Mensajes</b></p>
	 * 
	 * <p>Modifica el conjunto de conexiones para agregar el asociado con el nuevo emisor.</p>
	 * 
	 * @param sobre
	 */
	private void reenviarMensaje(Sobre sobre) {
		Set<Conexión> conexionesAnteriores = sobre.getConexiones();

		sobre.setConexiones(conexiones(sobre.getCanal()));

		enviarMensaje(sobre);

		sobre.setConexiones(conexionesAnteriores);
	}

	//////
	// Métodos Públicos
	/////

	/**
	 * Ejecuta las acciones asociadas al canal y envía mensajes a todas las instancias
	 * {@link Comunicativo} subscritas local o globalmente al canal argumento.
	 *
	 * <IMG SRC="../../Imágenes/comunicar.jpg"/>
	 *
	 * @param canal String
	 * @param mensaje Object
	 */
	public void comunicar(String canal, Object mensaje) {
		Sobre sobre = new Sobre(canal,remitente,mensaje, conexiones(canal));

		enviarMensaje(sobre);
	}

	/**
	 * <p><b>Comunicación por Acciones</b></p>
	 * 
	 * <p>Agrega una acción que será ejecutada al invocar {@link Comunicador#comunicar(String, Object)}
	 *  por el canal argumento.</p>
	 * 
	 * @param canal
	 * @param acción
	 */
	public void agregarAcción(String canal, Acción acción) {
		Set<Acción> acciones = mapaAcciones.get(canal);

		if(acciones == null) {
			acciones = new HashSet<>();
			mapaAcciones.put(canal,acciones);
		}

		acciones.add(acción);
	}

	/**
	 * <p><b>Comunicación Local</b></p>
	 * 
	 * <p>Los panales subscritos recibirán mensajes cuando la instancia invoque {@link Comunicador#comunicar(String, Object)}</p>
	 * 
	 * @param canal
	 * @param receptor
	 * @param transformación
	 */
	public void subscribir(String canal, Comunicativo receptor, Transformación transformación) {
		Set<Conexión> conexiones = subcripciones.get(canal);

		if (conexiones == null) {
			conexiones = new HashSet<>();
			subcripciones.put(canal,conexiones);
		}

		conexiones.add(new Conexión(receptor,transformación));
	}


	/**
	 * <p><b>Comunicación Global</b></p>
	 * 
	 * <p>Asocia una Palabra Clave a un Canal. Al ejecutar {@link Comunicador#comunicar(String, Object)}
	 * por el canal argumento, enviará mensajes a todos los {@link Comunicativo} 
	 * subscritos globalmente a la Palabra Clave, que hayan invocado 
	 * {@link Comunicador#subscripciónGlobal(String, Transformación)}
	 * con dicha Palabra Clave.</p>
	 * 
	 * @param canal
	 * @param palabraClave
	 */
	public void emisiónGlobal(String canal, String palabraClave) {
		Set<String> palabrasClave = mapaPalabrasClave.get(canal);

		if (palabrasClave == null) {
			palabrasClave = new HashSet<>();
			mapaPalabrasClave.put(canal,palabrasClave);
		}

		palabrasClave.add(palabraClave);
	}


	/**
	 * <p><b>Comunicación Global</b></p>
	 * 
	 * <p>Subscripción de la instancia a las comunicaciones globales con la Palabra Clave argumento.
	 * Si un {@link Comunicativo} invoca {@link Comunicador#comunicar(String, Object)}
	 * con la palabra Palabra Clave argumento, la instancia recibirá el mensaje, transformado
	 * por la {@link Comunicador.Transformación} argumento.</p>
	 * 
	 * @param palabraClave
	 * @param transformación
	 */
	public void subscripciónGlobal(String palabraClave, Transformación transformación) {
		Set<Conexión> colección = subcripcionesGlobales.get(palabraClave);
		Conexión conexión = new Conexión(remitente, transformación);

		if(colección == null) { //No hay ningún panel subscrito al canal, inicializamos la colección.
			colección = new HashSet<>();
			subcripcionesGlobales.put(palabraClave,colección);
		}

		colección.add(conexión);
	}

	/**
	 * <p><b>Comunicación por Acciones</b></p>
	 * 
	 * <p>La acción argumento dejará de aplicarse ante invocaciones de {@link Comunicador#comunicar(String, Object)}.
	 * Incompatible con la instroducción <i>lambda</i> de objetos {@link Comunicador.Acción}.</p>
	 * 
	 * @param canal
	 * @param acción {@link Comunicador.Acción}
	 */
	public void eliminarAcción(String canal, Acción acción) {
		Set<Acción> acciones = mapaAcciones.get(canal);

		if(acciones != null)
			acciones.remove(acción);
	}

	/**
	 * <p><b>Comunicación Local</b></p>
	 * 
	 * <p>El {@link Comunicativo} argumento dejará de recibir mensajes cuando 
	 * la instancia comunique por el canal argumento.</p>
	 * 
	 * @param canal
	 * @param comunicativo {@link Comunicativo}
	 */
	public void eliminarSubscripción(String canal, Comunicativo comunicativo) {
		Set<Conexión> conexiones = subcripciones.get(canal);

		if(conexiones != null)
			conexiones.remove(new Conexión(comunicativo,valor->valor));
	}


	/**
	 * <p><b>Comunicación Global</b></p>
	 *
	 * <p>La instancia dejará de enviar mensajes con la Palabra Clave argumento cuando realice
	 * comunicaciones por el canal.</p>
	 *
	 * @param canal
	 * @param palabraClave
	 */
	public void eliminarEmisiónGlobal(String canal, String palabraClave) {
		Set<String> palabrasClave = mapaPalabrasClave.get(canal);

		if(palabrasClave != null)
			palabrasClave.remove(palabraClave);
	}

	/**
	 * <p><b>Comunicación Global</b></p>
	 *
	 * <p>La instancia dejará de recibir mensajes asociados a la Palabra Clave argumento.</p>
	 *
	 * @param palabraClave
	 */
	public void eliminarSubscripciónGlobal(String palabraClave) {

		Set<Conexión> colección = subcripcionesGlobales.get(palabraClave);

		if(colección != null) colección.remove(new Conexión(remitente, valor -> valor));
	}

	//Selectores y Mutadores

	public void setModoDepuración(boolean modoDepuración) {
		this.modoDepuración = modoDepuración;
	}

	public static void setModoDepuraciónGlobal(boolean modoDepuraciónGlobal) {
		Comunicador.modoDepuraciónGlobal = modoDepuraciónGlobal;
	}
}
