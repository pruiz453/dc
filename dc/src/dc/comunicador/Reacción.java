package dc.comunicador;

/**
 * Comportamiento al recibir un mensaje:
 *
 * <br>
 * <p><b>{@link Reacción#No_Propagar}</b></p>
 * <p>No se realizará acción alguna.</p>
 *
 * <br>
 * <p><b>{@link Reacción#Propagar}</b></p>
 * <p>El mensaje será reenviado a los {@link Comunicativo} asociados al
 * receptor sin repetir, en ningún, destino.</p>
 *
 * <br>
 * <p><b>{@link Reacción#Bloquear}</b></p>
 * <p>El mensaje no llegará a ningún otro destino.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public enum Reacción {
    No_Propagar,
    Propagar,
    Bloquear
}
