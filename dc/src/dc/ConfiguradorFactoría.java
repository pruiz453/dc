package dc;

import dc.gui.AspectoConfigurable;
import dc.gui.ConfiguradorAspecto;
import dc.gui.MarcoConfigurador;
import dc.gui.Utilidades;
import dc.gui.distribuciones.Alineación;
import dc.gui.paneles.PanelBotones;
import dc.gui.paneles.PanelMatricial;
import dc.gui.paneles.extensibles.PanelBarras;
import dc.gui.paneles.extensibles.PanelÁrbol;
import dc.gui.paneles.simples.*;

import javax.swing.*;
import java.awt.*;

/**
 * Crea un diálogo configurador para {@link FactoríaConfiguración}.
 *
 * <p>El diálogo tendrá estructura de árbol ({@link PanelÁrbol}) y contendrá
 * opciones para configurar el {@link AspectoConfigurable} de {@link PanelSimple},
 * {@link PanelTítulo} y {@link MarcoConfigurador}. Así como parámetros asociados
 * a las dimensiones y posiciones.</p>
 *
 * <p>Además tendrá una sección para configurar los elementos gráficos de los
 * {@link ConfiguradorExtensible}, y dará la posibilidad de guardar o cargar
 * la configuración en una sección <i>Archivo</i>.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorFactoría {

    /**
     * <p>Crea un diálogo de configuración para configurar la instancia de
     * {@link FactoríaConfiguración}
     * por defecto: {@link FactoríaConfiguración#getInstancia()}</p>.
     *
     * <p>Véase {@link ConfiguradorFactoría}.</p>
     *
     * @param terminar Se ejecutará al configurar con éxito.
     * @param cancelar Se ejecutará si no se ha configurado con éxito.
     */
    public static void configurar(FactoríaConfiguración.Acción terminar, FactoríaConfiguración.Acción cancelar) {
        configurar(FactoríaConfiguración.getInstancia(),terminar,cancelar);
    }

    /**
     * Función recursiva que aplica un {@link AspectoConfigurable} a todos los
     * {@link PanelSimple} incluidos directa o indirectamente en un
     * {@link ConfiguradorExtensible}.
     *
     * <p>Obsérvese que pueden haber otros {@link PanelSimple} que no están
     * asociados a {@link Configurador}, luego hay que iterar sobre el contenedor
     * gráfico.</p>
     *
     * @param raíz {@link ConfiguradorExtensible} donde comenzar a aplicar el {@link AspectoConfigurable}
     * @param aspecto {@link AspectoConfigurable a aplicar}.
     */
    private static void aplicarAspectoSimples(ConfiguradorExtensible raíz, AspectoConfigurable aspecto) {

        Container contenedor = raíz.getComponenteGráfico();

        //Si posee barras de desplazamiento, obtenemos su contenedor
        if(raíz.getComponenteGráfico() instanceof PanelBarras) {
            contenedor = (Container) ((PanelBarras) raíz.getComponenteGráfico()).getComponente();
        }

        //Aplicamos aspecto
        for(Component componente : contenedor.getComponents()) {
            if(componente instanceof PanelSimple)
                aspecto.aplicar((JComponent) componente);
        }

        //LLamada recursiva
        for (Configurador configurador : raíz.getConfiguradores()) {
            if(configurador instanceof ConfiguradorExtensible) {
                aplicarAspectoSimples((ConfiguradorExtensible) configurador, aspecto);
            }
        }
    }

    /**
     * Función recursiva que aplica un {@link AspectoConfigurable} a todos los
     * {@link PanelTítulo} incluidos directa o indirectamente en un componente gráfico
     * asociado a un {@link ConfiguradorExtensible}.
     *
     * @param raíz {@link ConfiguradorExtensible} donde comenzar a aplicar el {@link AspectoConfigurable}
     * @param aspecto {@link AspectoConfigurable a aplicar}.
     */
    private static void aplicarAspectoTítulos(ConfiguradorExtensible raíz, AspectoConfigurable aspecto) {
        Container contenedor = raíz.getComponenteGráfico();

        //Si posee barras de desplazamiento, obtenemos su contenedor
        if(raíz.getComponenteGráfico() instanceof PanelBarras) {
            contenedor = (Container) ((PanelBarras) raíz.getComponenteGráfico()).getComponente();
        }

        //Aplicamos aspecto
        for(Component componente : contenedor.getComponents()) {
            if(componente instanceof PanelTítulo) {
                aspecto.aplicar((PanelTítulo) componente);
            }
        }

        //Llamada recursiva
        for (Configurador configurador : raíz.getConfiguradores()) {
            if(configurador instanceof ConfiguradorExtensible) {
                aplicarAspectoTítulos((ConfiguradorExtensible) configurador, aspecto);
            }
        }
    }

    /**
     * Crea un diálogo de configuración para configurar una {@link FactoríaConfiguración}.
     *
     * <p>Véase {@link ConfiguradorFactoría}.</p>
     *
     * @param factoría Instancia de {@link FactoríaConfiguración} a configurar.
     * @param terminar Se ejecutará al configurar con éxito.
     * @param cancelar Se ejecutará si no se ha configurado con éxito.
     */
    public static void configurar(FactoríaConfiguración factoría, FactoríaConfiguración.Acción terminar, FactoríaConfiguración.Acción cancelar) {

        ConfiguradorExtensible árbol = factoría.extensibleÁrbol("Configuración");

        //////////////// Marco ////////////////

        ConfiguradorExtensible árbolMarco = factoría.extensibleÁrbol("Marco");
        árbol.agregar(árbolMarco);

        Configurador cAspectoMarco = ConfiguradorAspecto.configurar(factoría.getAspectoMarco());
        árbolMarco.agregar(cAspectoMarco);

        ConfiguradorExtensible verticalDimensionesMarco = factoría.extensibleVertical("Dimensiones");

        Configurador cMargen = factoría.desplazador("Margen", new ConfiguradorInt(
                factoría.getMargenMarco(),5,95) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setMargenMarco(getValor());
            }
        });
        verticalDimensionesMarco.agregar(cMargen);


        Configurador cEspacioBotones = factoría.desplazador("Espacio Botones", new ConfiguradorInt(
                factoría.getEspacioBotones(),4,36) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setEspacioBotones(getValor());
            }
        });
        verticalDimensionesMarco.agregar(cEspacioBotones);

        árbolMarco.agregar(verticalDimensionesMarco);


        //////////////// Paneles Simples ////////////////

        ConfiguradorExtensible árbolPanelesSimples = factoría.extensibleÁrbol("Paneles Simples");
        árbol.agregar(árbolPanelesSimples);

        Configurador cAspectoSimples = ConfiguradorAspecto.configurar(factoría.getAspectoPanelesSimples());
        árbolPanelesSimples.agregar(cAspectoSimples);

        cAspectoSimples.agregarAcción(valor ->
                aplicarAspectoSimples(árbol,factoría.getAspectoPanelesSimples().getMuestra()));


        ////// Dimensiones ///////
        ConfiguradorExtensible verticalDimensionesPaneles = factoría.extensibleVertical("Dimensiones");
        árbolPanelesSimples.agregar(verticalDimensionesPaneles);

        Configurador cMargenPaneles = factoría.desplazador("Margen", new ConfiguradorInt(factoría.getMargenPaneles(),5,30) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setMargenPaneles(getValor());
            }
        });
        verticalDimensionesPaneles.agregar(cMargenPaneles);


        Configurador cGanancia = factoría.desplazador("Ganancia Altura", new ConfiguradorInt(factoría.getGananciaAltura(),5,120) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setGananciaAltura(getValor());
            }
        });
        verticalDimensionesPaneles.agregar(cGanancia);

        cMargenPaneles.agregarAcción(valor -> {
            ((PanelMatricial) cMargenPaneles.getComponenteGráfico()).getDistribución().setMargen(((Number)valor).intValue());
            ((PanelMatricial) cGanancia.getComponenteGráfico()).getDistribución().setMargen(((Number)valor).intValue());
            ((PanelMatricial) cMargenPaneles.getComponenteGráfico()).actualizar();
            ((PanelMatricial) cGanancia.getComponenteGráfico()).actualizar();
        });


        cGanancia.agregarAcción(valor -> {
            ((PanelMatricial) cMargenPaneles.getComponenteGráfico()).getDistribución().setGananciaAltura(((Number)valor).intValue());
            ((PanelMatricial) cGanancia.getComponenteGráfico()).getDistribución().setGananciaAltura(((Number)valor).intValue());
            ((PanelMatricial) cMargenPaneles.getComponenteGráfico()).actualizar();
            ((PanelMatricial) cGanancia.getComponenteGráfico()).actualizar();
        });



        ////// Paneles //////

        ConfiguradorExtensible árbolPaneles = factoría.extensibleÁrbol("Paneles");
        árbolPanelesSimples.agregar(árbolPaneles);

        ////// Casilla //////

        ConfiguradorExtensible verticalCasilla = factoría.extensibleVertical("Casilla");
        árbolPaneles.agregar(verticalCasilla);

        Configurador muestraCasilla = factoría.casilla("Muestra", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalCasilla.agregar(muestraCasilla);

        String[] nombresEstilos = Casilla.Estilo.getNombres();

        Configurador cEstilo = factoría.lista("Estilo", new ConfiguradorString(
                Utilidades.getÍndice(nombresEstilos,factoría.getEstiloCasilla().toString()),nombresEstilos
        ) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setEstiloCasilla(Casilla.Estilo.convertir((String) getValor()));
            }
        });
        verticalCasilla.agregar(cEstilo);

        cEstilo.agregarAcción(valor -> ((Casilla) muestraCasilla.getComponenteGráfico()).setEstilo(
                Casilla.Estilo.convertir((String) valor)
        ));


        ////// Lista //////

        ConfiguradorExtensible verticalLista = factoría.extensibleVertical("Lista");
        árbolPaneles.agregar(verticalLista);

        Configurador muestraLista = factoría.lista("Muestra", new ConfiguradorChar(
                new char[]{'a','b','c'}
        ) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalLista.agregar(muestraLista);

        Configurador cAnchoMínimoLista = factoría.desplazador("Ancho Mínimo", new ConfiguradorInt(factoría.getAnchoMínimoLista(),10,150) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAnchoMínimoLista(getValor());
            }
        });
        verticalLista.agregar(cAnchoMínimoLista);

        cAnchoMínimoLista.agregarAcción(valor ->
                ((Lista) muestraLista.getComponenteGráfico()).setAnchoLista(((Number)valor).intValue()));


        ////// Recuadro //////

        ConfiguradorExtensible verticalRecuadro = factoría.extensibleVertical("Recuadro");
        árbolPaneles.agregar(verticalRecuadro);

        Configurador muestraRecuadro = factoría.recuadro("Muestra", new ConfiguradorString() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalRecuadro.agregar(muestraRecuadro);

        Configurador desplazadorAltura = factoría.desplazador("Altura Texto", new ConfiguradorInt(factoría.getAltoTextoRecuadro(),20,150) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAltoTextoRecuadro(getValor());
            }
        });
        verticalRecuadro.agregar(desplazadorAltura);

        desplazadorAltura.agregarAcción(valor -> {
            ((Recuadro)muestraRecuadro.getComponenteGráfico()).setAltoTexto(((Number) valor).intValue());
        });

        String[] alineaciones = Alineación.getNombres();

        Configurador cAlineación = factoría.lista("Alineación Título", new ConfiguradorString(
                Utilidades.getÍndice(alineaciones, factoría.getAlineaciónTítuloRecuadro().toString()),alineaciones
        ) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAlineaciónTítuloRecuadro(Alineación.convertir((String) getValor()));
            }
        });
        verticalRecuadro.agregar(cAlineación);

        cAlineación.agregarAcción(valor -> {
            String alineación = (String) valor;
            ((Recuadro)muestraRecuadro.getComponenteGráfico()).setAlineaciónTítulo(
                    Alineación.convertir((String) valor)
            );
        });


        ////// Renglón //////

        ConfiguradorExtensible verticalRenglón = factoría.extensibleVertical("Renglón");
        árbolPaneles.agregar(verticalRenglón);

        Configurador muestraNormal = factoría.renglón("Normal", new ConfiguradorInt() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalRenglón.agregar(muestraNormal);

        Configurador muestraGrande = factoría.renglón("Grande", new ConfiguradorString() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalRenglón.agregar(muestraGrande);

        Configurador desplazadorNormal = factoría.desplazador("Ancho Normal", new ConfiguradorInt(factoría.getAnchoNormalRenglón(),50,150) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAnchoNormalRenglón(getValor());
            }
        });
        verticalRenglón.agregar(desplazadorNormal);

        desplazadorNormal.agregarAcción(valor -> {
            ((Renglón) muestraNormal.getComponenteGráfico()).setAnchoTextoNormal(((Number) valor).intValue());
        });

        Configurador desplazadorGrande = factoría.desplazador("Ancho Grande", new ConfiguradorInt(factoría.getAnchoGrandeRenglón(),50,250) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAnchoGrandeRenglón(getValor());
            }
        });
        verticalRenglón.agregar(desplazadorGrande);

        desplazadorGrande.agregarAcción(valor -> {
            ((Renglón) muestraGrande.getComponenteGráfico()).setAnchoTextoGrande(((Number) valor).intValue());
        });



        //////////////// Títulos ////////////////


        ConfiguradorExtensible árbolTítulos = factoría.extensibleÁrbol("Títulos");
        árbol.agregar(árbolTítulos);

        ConfiguradorExtensible verticalAspectoTítulos = factoría.extensibleVertical();
        verticalAspectoTítulos.setNombre("Aspecto");
        árbolTítulos.agregar(verticalAspectoTítulos);

        Configurador cAspectoTítulos = ConfiguradorAspecto.configurar(factoría.getAspectoTítulos());
        verticalAspectoTítulos.agregar(cAspectoTítulos);


        Configurador cConexiónAspectos = factoría.casilla("Aspecto de Paneles Simples", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {

            }
        });
        verticalAspectoTítulos.agregar(cConexiónAspectos);

        cConexiónAspectos.agregarAcción(valor -> {
            if((Boolean) valor) {
                cAspectoSimples.subscribir(cAspectoTítulos);
                cAspectoSimples.comunicar();
            }else {
                cAspectoSimples.eliminarSubscripción(cAspectoTítulos);
            }
        });


        cAspectoTítulos.agregarAcción(valor ->
                aplicarAspectoTítulos(árbol,factoría.getAspectoTítulos().getMuestra()));

        ConfiguradorExtensible verticalTítulos = factoría.extensibleVertical();
        verticalTítulos.setNombre("Dimensiones");
        árbolTítulos.agregar(verticalTítulos);

        PanelTítulo muestra = factoría.título("Dimensiones");
        verticalTítulos.agregar(muestra);

        Configurador cMargenTítulos = factoría.desplazador("Margen", new ConfiguradorInt(factoría.getMargenTítulos(),0,100) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setMargenTítulos(getValor());
            }
        });
        verticalTítulos.agregar(cMargenTítulos);

        cMargenTítulos.agregarAcción(valor -> muestra.setMargen(((Number)valor).intValue()));


        //////////////// Extensibles ////////////////

        ConfiguradorExtensible árbolExtensibles = factoría.extensibleÁrbol("Extensibles");
        árbol.agregar(árbolExtensibles);

        /// General ///
        ConfiguradorExtensible verticalExtensiblesGeneral = factoría.extensibleVertical("General");
        árbolExtensibles.agregar(verticalExtensiblesGeneral);

        Configurador cOpacidad = factoría.casilla("Opacidad", new ConfiguradorBoolean(factoría.getExtensiblesOpacos()) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setExtensiblesOpacos(getValor());
            }
        });
        verticalExtensiblesGeneral.agregar(cOpacidad);

        cOpacidad.agregarAcción(valor ->{
            verticalExtensiblesGeneral.getComponenteGráfico().setOpaque((Boolean)valor);
            verticalExtensiblesGeneral.getComponenteGráfico().repaint();
        });

        verticalExtensiblesGeneral.agregar(factoría.casilla("Verticales Titulados", new ConfiguradorBoolean(factoría.getVerticalTitulado()) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setVerticalTitulado(getValor());
            }
        }));


        /// Paneles Extensibles ///

        ConfiguradorExtensible árbolExtensiblesPaneles = factoría.extensibleÁrbol("Paneles");
        árbolExtensibles.agregar(árbolExtensiblesPaneles);


        /// Árbol ///

        ConfiguradorExtensible verticalÁrbol = factoría.extensibleVertical("Árbol");
        árbolExtensiblesPaneles.agregar(verticalÁrbol);

        Configurador cPorcentajeAparición = factoría.desplazador("Porcentaje Ancho Automático",
                new ConfiguradorFloat(factoría.getPorcentajeApariciónÁrbol(),0.1f,0.5f) {
                    @Override
                    public String validar() {
                        return null;
                    }

                    @Override
                    public void salvar() {
                        factoría.setPorcentajeMáximoAnchoÁrbol(getValor());
                    }
                });
        verticalÁrbol.agregar(cPorcentajeAparición);

        cPorcentajeAparición.agregarAcción(valor ->
                ((PanelÁrbol) árbol.getComponenteGráfico()).setPorcentajeApariciónÁrbol(((Number)valor).floatValue()));

        Configurador cPorcentajeMáximo = factoría.desplazador("Porcentaje Ancho Máximo",
                new ConfiguradorFloat(factoría.getPorcentajeMáximoAnchoÁrbol(),0.1f,0.5f) {
                    @Override
                    public String validar() {
                        return null;
                    }

                    @Override
                    public void salvar() {
                        factoría.setPorcentajeMáximoAnchoÁrbol(getValor());
                    }
                });
        verticalÁrbol.agregar(cPorcentajeMáximo);

        cPorcentajeMáximo.agregarAcción(valor ->
                ((PanelÁrbol) árbol.getComponenteGráfico()).setPorcentajeMáximoAnchoÁrbol(((Number)valor).floatValue()));

        cPorcentajeAparición.subscribir(cPorcentajeMáximo, valor ->
                Float.max(((Number) valor).floatValue(), ((ConfiguradorFloat) cPorcentajeMáximo).getValor()));

        cPorcentajeMáximo.subscribir(cPorcentajeAparición, valor ->
                Float.min(((Number) valor).floatValue(), ((ConfiguradorFloat) cPorcentajeAparición).getValor()));



        Configurador cAnchoMínimoÁrbol = factoría.desplazador("Ancho Mínimo", new ConfiguradorInt(factoría.getAnchoMínimoÁrbol(),5,100) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setAnchoMínimoÁrbol(getValor());
            }
        });
        verticalÁrbol.agregar(cAnchoMínimoÁrbol);

        cAnchoMínimoÁrbol.agregarAcción(valor ->
                ((PanelÁrbol) árbol.getComponenteGráfico()).setAnchoMínimoÁrbol(((Number)valor).intValue()));


        /// Barras ///

        ConfiguradorExtensible verticalBarras = factoría.extensibleVertical("Barras");
        árbolExtensiblesPaneles.agregar(verticalBarras);

        PanelBarras muestraBarras = new PanelBarras(new JTextArea(elegía));
        verticalBarras.agregar(muestraBarras);

        Configurador cAltoBarras = factoría.desplazador("Alto Mínimo", new ConfiguradorFloat(factoría.getPorcentajeAltoBarras(),
                0.05f, 1f) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                factoría.setPorcentajeAltoBarras(getValor());
            }
        });
        verticalBarras.agregar(cAltoBarras);

        cAltoBarras.agregarAcción(valor -> muestraBarras.setPorcentajeAltoPantalla(((Number) valor).floatValue()));



        //////////////// Marco ////////////////

        MarcoConfigurador marco = (MarcoConfigurador) factoría.marco("Configuración", árbol, terminar, cancelar);

        cMargen.agregarAcción(valor -> {
            ((PanelBotones)marco.getContentPane()).setMargen(((Number) valor).intValue());
        });

        cEspacioBotones.agregarAcción(valor -> {
            ((PanelBotones)marco.getContentPane()).setEspacioBotones(((Number) valor).intValue());
        });

        cAspectoMarco.agregarAcción(valor ->
                factoría.getAspectoMarco().getMuestra().aplicar((JComponent) marco.getContentPane()));

        //////////////// Archivo ////////////////

        ConfiguradorExtensible verticalArchivo = factoría.extensibleVertical("Archivo");
        árbol.agregar(verticalArchivo);

        PanelMatricial panelGuardar = new PanelMatricial();
        verticalArchivo.agregar(panelGuardar);


        JButton botónGuardar = new JButton("Guardar");
        panelGuardar.agregar(botónGuardar,0,Alineación.Centrada);

        botónGuardar.addActionListener(actionEvent -> {
            try{
                árbol.salvar();
                factoría.escribirEnDisco(marco);
            }catch (Exception ex) {
                factoría.mensaje("Error", "Ruta no válida.",null);
            }
        });

        factoría.getAspectoPanelesSimples().aplicar(panelGuardar);


        PanelMatricial panelCargar = new PanelMatricial();
        verticalArchivo.agregar(panelCargar);

        JButton botónCargar = new JButton("Cargar");
        panelCargar.agregar(botónCargar,0,Alineación.Centrada);

        botónCargar.addActionListener(actionEvent -> {
            try{
                if(factoría.leerDeDisco(marco)) {
                    marco.dispose();
                    ConfiguradorFactoría.configurar(factoría,terminar,cancelar);
                }
            }catch (Exception ex) {
                ex.printStackTrace();
                factoría.mensaje("Error", "Archivo inadecuado.\n\nEl archivo no contiene una factoría.",null);
            }
        });

        factoría.getAspectoPanelesSimples().aplicar(panelCargar);


        /// Visualización ///

        marco.setVisible(true);
    }


    private static final String elegía =
            "Yo quiero ser llorando el hortelano\n" +
                    "de la tierra que ocupas y estercolas,\n" +
                    "compañero del alma, tan temprano.\n" +
                    "\n" +
                    "Alimentando lluvias, caracolas\n" +
                    "y órganos mi dolor sin instrumento.\n" +
                    "a las desalentadas amapolas\n" +
                    "\n" +
                    "daré tu corazón por alimento.\n" +
                    "Tanto dolor se agrupa en mi costado,\n" +
                    "que por doler me duele hasta el aliento.\n" +
                    "\n" +
                    "Un manotazo duro, un golpe helado,\n" +
                    "un hachazo invisible y homicida,\n" +
                    "un empujón brutal te ha derribado.\n" +
                    "\n" +
                    "No hay extensión más grande que mi herida,\n" +
                    "lloro mi desventura y sus conjuntos\n" +
                    "y siento más tu muerte que mi vida.\n" +
                    "\n" +
                    "Ando sobre rastrojos de difuntos,\n" +
                    "y sin calor de nadie y sin consuelo\n" +
                    "voy de mi corazón a mis asuntos.\n" +
                    "\n" +
                    "Temprano levantó la muerte el vuelo,\n" +
                    "temprano madrugó la madrugada,\n" +
                    "temprano estás rodando por el suelo.\n" +
                    "\n" +
                    "No perdono a la muerte enamorada,\n" +
                    "no perdono a la vida desatenta,\n" +
                    "no perdono a la tierra ni a la nada.\n" +
                    "\n" +
                    "En mis manos levanto una tormenta\n" +
                    "de piedras, rayos y hachas estridentes\n" +
                    "sedienta de catástrofes y hambrienta.\n" +
                    "\n" +
                    "Quiero escarbar la tierra con los dientes,\n" +
                    "quiero apartar la tierra parte a parte\n" +
                    "a dentelladas secas y calientes.\n" +
                    "\n" +
                    "Quiero minar la tierra hasta encontrarte\n" +
                    "y besarte la noble calavera\n" +
                    "y desamordazarte y regresarte.\n" +
                    "\n" +
                    "Volverás a mi huerto y a mi higuera:\n" +
                    "por los altos andamios de las flores\n" +
                    "pajareará tu alma colmenera\n" +
                    "\n" +
                    "de angelicales ceras y labores.\n" +
                    "Volverás al arrullo de las rejas\n" +
                    "de los enamorados labradores.\n" +
                    "\n" +
                    "Alegrarás la sombra de mis cejas,\n" +
                    "y tu sangre se irán a cada lado\n" +
                    "disputando tu novia y las abejas.\n" +
                    "\n" +
                    "Tu corazón, ya terciopelo ajado,\n" +
                    "llama a un campo de almendras espumosas\n" +
                    "mi avariciosa voz de enamorado.\n" +
                    "\n" +
                    "A las aladas almas de las rosas\n" +
                    "del almendro de nata te requiero,\n" +
                    "que tenemos que hablar de muchas cosas,\n" +
                    "compañero del alma, compañero.";


}
