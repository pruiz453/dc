package dc;

/**
 * Configurador compatible con una {@link dc.gui.paneles.simples.Lista}.
 *
 * <p>Esta interfaz es usada por la factoría para comunicar
 * el {@link Configurador} con su componente gráfico.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface ConfiguradorLista extends Configurador {

	/**
	 * Dada la opción escogida de la {@link dc.gui.paneles.simples.Lista},
	 * devuelve el valor del configurador.
	 *
	 * <p>Por defecto devolverá el mismo valor, pero puede ser sobrescrito.</p>
	 *
	 * @param opción Opción de la {@link dc.gui.paneles.simples.Lista}.
	 * @return Valor del configurador.
	 */
	default Object convertirOpción(String opción) {
		return opción;
	}

	/**
	 * Devuelve la opción de la {@link dc.gui.paneles.simples.Lista}, en función
	 * del estado o valor del configurador.
	 *
	 * @return Opción de la {@link dc.gui.paneles.simples.Lista}.
	 */
	String getOpción();

	/**
	 * El conjunto de opciones posibles es necesario para inicializar la lista desplegable.
	 *
	 * @return Conjunto de opciones posibles para el configurador.
	 */
	String[] getOpciones();

}
