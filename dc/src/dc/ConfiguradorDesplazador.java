package dc;

/**
 * Configurador compatible con un {@link dc.gui.paneles.simples.Desplazador}.
 *
 * <p>Los desplazadores, en el proyecto, configuran valores numéricos.</p>
 *
 * <p>No se considerarán desplazamientos negativos, luego el desplazamiento
 * mínimo es constante e igual a <b>cero</b>.
 * El desplazamiento máximo es pasado como argumento.</p>
 *
 * <p>Esta interfaz es usada por la factoría para comunicar
 * el {@link Configurador} con su componente gráfico.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface ConfiguradorDesplazador extends Configurador {

	/**
	 * Dado un desplazamiento y el desplazamiento máximo, devuelve el
	 * valor del configurador. El desplazamiento mínimo es cero.
	 *
	 * @param desplazamiento {@link Integer} en el intervalo (0,desplazamientoMáximo).
	 * @param desplazamientoMáximo Máximo desplazamiento.
	 * @return Valor del configurador.
	 */
	Object convertirDesplazamiento(int desplazamiento, int desplazamientoMáximo);

	/**
	 * Dado el valor del configurador y un desplazamiento máximo, devuelve
	 * el desplazamiento que debe adquirir el desplazador, un entero en el
	 * intervalo (0,desplazamientoMáximo).
	 *
	 * @param valor Valor del configurador.
	 * @param desplazamientoMáximo Desplazamiento máximo del desplazador.
	 * @return {@link Integer} en el intervalo (0,desplazamientoMáximo).
	 */
	int getDesplazamiento(Object valor, int desplazamientoMáximo);

	/**
	 * Los paneles {@link dc.gui.paneles.simples.Desplazador} poseen un recuadro de
	 * texto donde informan el valor que acompaña al desplazamiento.
	 *
	 * <p>Lo normal es que sea un valor numérico, pero podría ser palabras como: "Frío",
	 * "Caliente"...</p>
	 *
	 * @return Texto del desplazador.
	 */
	String getTextoDesplazador();

}
