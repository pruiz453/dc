package dc;


import dc.comunicador.Reacción;

import javax.swing.*;

/**
 * Clase dedicada a la configuración de variables String.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorString extends ConfiguradorSimple implements ConfiguradorTextual, ConfiguradorLista  {

	private String valor;
	private String[] opciones;
	private int índice;

	/**
	 * Constructor por defecto inicializado en ""
	 *
	 */
	public ConfiguradorString(){
		inicializar("", new String[0]);
	}

	/**
	 * Constructor que permite la introducción del valor inicial.
	 * @param valorInicial Valor inicial del Configurador.
	 */
	public ConfiguradorString(String valorInicial){
		inicializar(valorInicial, new String[0]);
	}

	/**
	 * Constructor que permite dar una lista de opciones. No se aceptarán carácteres
	 * fuera de dicha lista.
	 *
	 * @param opciones Vector de carácteres posibles.
	 */
	public ConfiguradorString(String[] opciones) {
		inicializar(opciones[0],opciones);
		índice = 0;
	}

	/**
	 * Constructor que permite dar una lista de opciones y el índice inicial. No se aceptarán carácteres
	 * fuera de dicha lista.
	 *
	 * <p>El Configurador se inicializará con <s>opciones[índice]</s></p>
	 *
	 * <p>Si el índice está fuera de rango, se generará {@link RuntimeException}.</p>
	 *
	 * @param índice Índice del carácter inicial.
	 * @param opciones Vector de carácteres posibles.
	 */
	public ConfiguradorString(int índice, String[] opciones) {
		if(índice >= opciones.length)
			throw new RuntimeException("Índice fuera de rango en constructor de ConfiguradorString.");

		inicializar(opciones[índice],opciones);
		this.índice = índice;
	}

	/**
	 * Inicialización común a los constructores.
	 * @param valorInicial Valor inicial.
	 * @param opciones Vector de carácteres posibles.
	 */
	private void inicializar(String valorInicial, String[] opciones) {
		this.valor = valorInicial;
		this.opciones = opciones;
	}


	/// Métodos Sobrescritos ///


	@Override
	public Object convertirTexto(String texto) {
		return texto;
	}

	@Override
	public String getTexto() {
		return valor.toString();
	}

	@Override
	public String getOpción() {
		return valor;
	}

	@Override
	public String[] getOpciones() {
		return opciones;
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(!(mensaje instanceof String)) throw new RuntimeException("Argumento inválido en recibirValor de ConfiguradorString: " + mensaje);

		String valorNuevo = (String) mensaje;

		//Comprobamos si pertenece a opciones
		if(opciones != null && opciones.length != 0) {
			boolean valorEncontrado = false;

			for(int i = 0; i < opciones.length; i++) {
				if(valorNuevo == opciones[i]) {
					valorEncontrado = true;
					break;
				}
			}

			if(!valorEncontrado) {
				JOptionPane.showMessageDialog(null,"El valor '" + valorNuevo + "' no es una opción válida.");
				//Comunicamos para mantener la consistencia
				comunicar();
				return Reacción.Bloquear;
			}
		}

		if(setValor((String) mensaje)) {
			//Avisamos al contenedor padre
			actualizar();

			//Cambio efectivo
			return Reacción.Propagar;
		}else {
			return Reacción.No_Propagar;
		}
	}

	/// Selectores y Mutadores ///

	/**
	 * Mutador privado.
	 *
	 * @param valorNuevo Nuevo valor del Configurador.
	 * @return true si ha habido un cambio efectivo o si valorNuevo no figura en opciones.
	 */
	private boolean setValor(String valorNuevo) {

		if(!valorNuevo.equals(valor)) {
			valor = valorNuevo;
			return true;
		}
		return false;
	}

	/**
	 * Selector del valor del configurador.
	 *
	 * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
	 * y {@link Configurador#salvar()}.</p>
	 *
	 * @return El valor del configurador.
	 */
	public String getValor() {
		return valor;
	}
}