package dc;


/**
 * Configuración de valores byte.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorDouble extends ConfiguradorFlotante {

    /**
     * Constructor por defecto inicializado en 0.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     */
    public ConfiguradorDouble() {
        super((double)0,-Double.MAX_VALUE,Double.MAX_VALUE);
    }

    /**
     * Constructor que permite la inicialización.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     *
     * @param valorInicial Valor para la inicialización
     */
    public ConfiguradorDouble(double valorInicial) {
        super(valorInicial,-Double.MAX_VALUE,Double.MAX_VALUE);
    }

    /**
     * Constructor que permite la introducción de límites.
     * El valor inicial será el punto medio.
     *
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorDouble(double mínimo, double máximo) {
        super((mínimo + máximo)/2,mínimo,máximo);
    }

    /**
     * Constructor general. Permite la introducción de límites y
     * del valor de inicialización.
     *
     * <p>Si valorInicial no está en el intervalo permitido se terminará
     * la ejecución del programa.</p>
     *
     * @param valorInicial Valor para la inicialización
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorDouble(double valorInicial, double mínimo, double máximo) {
        super(valorInicial, mínimo, máximo);
    }

    /// Selectores y Mutadores ///

    /**
     * Selector del valor del configurador.
     *
     * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
     * y {@link Configurador#salvar()}.</p>
     *
     * @return El valor del configurador.
     */
    public double getValor()
    {
        return valor;
    }
}
