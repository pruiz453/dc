package dc;

/**
 * Configuración de valores byte.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorByte extends ConfiguradorEntero {

    /**
     * Constructor por defecto inicializado en 0.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     */
    public ConfiguradorByte() {
        super((byte)0,Byte.MIN_VALUE,Byte.MAX_VALUE);
    }

    /**
     * Constructor que permite la inicialización.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     *
     * @param valorInicial Valor para la inicialización
     */
    public ConfiguradorByte(byte valorInicial) {
        super(valorInicial,Byte.MIN_VALUE,Byte.MAX_VALUE);
    }

    /**
     * Constructor que permite la introducción de límites.
     * El valor inicial será el punto medio.
     *
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorByte(byte mínimo, byte máximo) {
        super((mínimo + máximo)/2,mínimo,máximo);
    }

    /**
     * Constructor general. Permite la introducción de límites y
     * del valor de inicialización.
     *
     * <p>Si valorInicial no está en el intervalo permitido se terminará
     * la ejecución del programa.</p>
     *
     * @param valorInicial Valor para la inicialización
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorByte(byte valorInicial, byte mínimo, byte máximo) {
        super(valorInicial, mínimo, máximo);
    }

    /// Selectores y Mutadores ///
   
    /**
     * Selector del valor del configurador.
     *
     * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
     * y {@link Configurador#salvar()}.</p>
     *
     * @return El valor del configurador.
     */
    public byte getValor()
    {
        return (byte) valor;
    }
}

