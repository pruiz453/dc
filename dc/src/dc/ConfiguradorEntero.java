package dc;

import dc.comunicador.Reacción;

/**
 * Configuración de números enteros: byte, short, int o long.
 *
 * <p>Las variables que almacena el configurador serán del mayor rango posible (long),
 * controlando que el valor no traspase los valores mínimo y máximo. Estos valores límites
 * deben estar dentro de los límites de cada tipo: si se configura un byte,
 * {@link ConfiguradorEntero#mínimo} >= {@link Byte#MIN_VALUE}. Esto es responsabilidad
 * de las clases hijas, que lo satisfacen al recibir los valores límite con el
 * mismo tipo del valor a configurar.</p>
 *
 * <p>El algoritmo de desplazamiento, como en {@link ConfiguradorFlotante} depende del valor
 * de {@link {@link ConfiguradorEntero#INCREMENTO_LÍMITE}}, debido a una mejor adecuación
 * de cada algoritmo según el rango del intervalo ({@link ConfiguradorEntero#mínimo},
 *  {@link ConfiguradorEntero#máximo}). Véase
 *  {@link ConfiguradorEntero#convertirDesplazamiento(int, int)} y
 *  {@link ConfiguradorEntero#getDesplazamiento(Object, int)}</p> para detalles.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorEntero extends ConfiguradorSimple implements ConfiguradorDesplazador, ConfiguradorTextual {

	/**
	 * Valor umbral que determina el algoritmo relacionado con desplazadores.
	 */
	public static final long INCREMENTO_LÍMITE = 10000;

	private long mínimo;
	private long máximo;
	private long puntoMedio;

	/**
	 * Valor del configurador
	 */
	protected long valor;

	/**
	 * Constructor que determina el valor inicial y el rango posible.
	 *
	 * <p>Si valorInicial no se encuentra en el rango, genera {@link RuntimeException}.</p>
	 *
	 * @param valorInicial Valor inicial del configurador.
	 * @param mínimo Mínimo valor del configurador.
	 * @param máximo Máximo valor del configurador.
	 */
	public ConfiguradorEntero(long valorInicial, long mínimo, long máximo) {

		if(valorInicial < mínimo || valorInicial > máximo)
			throw new RuntimeException("Valor inicial inválido en ConfiguradorEntero.");

		this.mínimo = mínimo;
		this.máximo = máximo;
		this.valor = valorInicial;
		puntoMedio = mínimo/2 + máximo/2;


	}

	/// Métodos sobrescritos ///

	/**
	 * <p>En este contexto, llamamos <i>incremento</i> a la proporción rangoConfigurador /
	 * rangoDesplazador. De tal forma que, multiplicando la proporción del desplazamiento
	 * argumento en su rango por el incremento se obtendrá la proporción en el rango del
	 * configurador.</p>
	 *
	 * <p>Si el incremento es menor que INCREMENTO_LÍMITE, se usará como punto de partida el valor mínimo más un valor
	 * proporcional al rango. Éste algoritmo es más preciso para rangos pequeños.</p>
	 *
	 * <p>Si el incremento es mayor o igual que INCREMENTO_LÍMITE, se partirá del punto medio y
	 * se sumará o restará un cierto número de incrementos. Éste algoritmo es más adecuado
	 * para rangos grandes, ya que evita posibles desbordamientos (para el cálculo del
	 * incremento utilizamos el punto medio con ese fin).</p>
	 *
	 * @param desplazamiento Entero entre 0 y desplazamientoMáximo que determinará el valor del configurador.
	 * @param desplazamientoMáximo Máximo desplazamiento.
	 * @return Valor del configurador.
	 */
	@Override
	public Object convertirDesplazamiento(int desplazamiento, int desplazamientoMáximo) {
		int desplazamientoMedio = desplazamientoMáximo/2;
		long incremento = (máximo - puntoMedio) / desplazamientoMedio;
		if(incremento < INCREMENTO_LÍMITE) {
			return ((long)((máximo - mínimo) * (double)(desplazamiento)/desplazamientoMáximo + mínimo));
		}else {
			return (puntoMedio + incremento * (desplazamiento - desplazamientoMedio));
		}
	}


	/**
	 * Realiza la operación inversa a {@link ConfiguradorEntero#convertirDesplazamiento(int, int)},
	 * también en función del valor del incremento.
	 *
	 * @param valor Valor del configurador.
	 * @param desplazamientoMáximo Desplazamiento máximo del desplazador.
	 * @return Desplazamiento del {@link dc.gui.paneles.simples.Desplazador}.
	 */
	@Override
	public int getDesplazamiento(Object valor, int desplazamientoMáximo) {
		int desplazamientoMedio = desplazamientoMáximo/2;
		long incremento = (máximo - puntoMedio) / desplazamientoMedio;

		if(incremento < INCREMENTO_LÍMITE) {
			return (int) ((desplazamientoMáximo * (((Number)valor).longValue() - mínimo)) / (máximo - mínimo));
		}else {
			return (int) ((((Number)valor).longValue() - puntoMedio)/incremento + desplazamientoMedio);
		}
	}

	@Override
	public String getTextoDesplazador() {
		return getTexto();
	}

	/**
	 * Elimina carácteres inválidos en un texto que debería contener un número entero, positivo o negativo.
	 * Tanto "-" como "" se considerarán válidos.
	 *
	 * @param texto Texto a limpiar.
	 * @return Texto limpio.
	 */
	@Override
	public String limpiarTexto(String texto) {
		if(texto.equals("") || texto.equals("-")) return texto;

		StringBuilder sb = new StringBuilder();

		//Primer carácter
		if(Character.isDigit(texto.charAt(0)) || texto.charAt(0) == '-') sb.append(texto.charAt(0));

		//Procesamos el resto.
		for(int i = 1; i < texto.length(); i++) {
			if(Character.isDigit(texto.charAt(i))) {
				sb.append(texto.charAt(i));
			}
		}

		return sb.toString();
	}


	/**
	 * En caso de "" o "-" el configurador tendrá valor cero.
	 *
	 * @param texto Texto introducido en el componente gráfico.
	 * @return Valor del configurador.
	 */
	@Override
	public Object convertirTexto(String texto) {
		if(texto.equals("") || texto.equals("-")) {
			return 0;
		}else {
			return Long.parseLong(texto);
		}
	}

	@Override
	public String getTexto() {
		return valor + "";
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(!(mensaje instanceof Number)) throw new RuntimeException("Argumento inválido en recibirValor de ConfiguradorEntero.");

		long nuevoValor = ((Number) mensaje).longValue();

		if(comprobaciónLímites(nuevoValor)) {
			if(setValor(nuevoValor)) {
				//Avisamos al contenedor padre
				actualizar();

				//Cambio efectivo
				return Reacción.Propagar;
			}else {
				return Reacción.No_Propagar;
			}
		}else {
			//Se han transgredido los límites.

			//Adoptamos el valor límite
			setValor(nuevoValor);

			//No propagamos el mensaje, sino que iniciamos una nueva comunicación.
			comunicar();

			//Avisamos al contenedor padre
			actualizar();

			//Bloqueamos la propagación
			return Reacción.Bloquear;
		}
	}

	public Object obtenerValor() {
		return valor;
	}

	/// Métodos Privados ///

	/**
	 * Comprueba que un determinado valor se encuentra entre los límites válidos.
	 *
	 * @param valor {@link Integer} a comprobar
	 * @return {@link Boolean}, true si es válido.
	 */
	boolean comprobaciónLímites(long valor) {
		return mínimo <= valor && valor <= máximo;
	}

	/**
	 * Método mutador privado. Si valorNuevo no se encuentra en el rango (mínimo, máximo), será modificado
	 * al límite más cercano.
	 *
	 * @param valorNuevo Valor nuevo del configurador.
	 * @return true si hay cambio efectivo.
	 */
	private boolean setValor(long valorNuevo) {
		if(valorNuevo < mínimo) valorNuevo = mínimo;
		if(valorNuevo > máximo) valorNuevo = máximo;

		if(valor != valorNuevo) {
			valor = valorNuevo;
			return true;
		}
		return false;
	}
}
