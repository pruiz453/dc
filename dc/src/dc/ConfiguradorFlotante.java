package dc;


import dc.comunicador.Reacción;

/**
 * Configuración de números flotantes: float o double.
 *
 * <p>Las variables que almacena el configurador serán del mayor rango posible (double),
 * controlando que el valor no traspase los valores mínimo y máximo. Como en el caso de
 * {@link ConfiguradorEntero}, es responsabilidad de los hijos que los valores mínimo
 * y máximo no traspasen los límites de su tipo. Sin embargo, el rango de {@link Float}
 * es ahora (-{@link Float#MAX_VALUE},{@link Float#MAX_VALUE}).
 *
 * <p>El algoritmo de desplazamiento, como en {@link ConfiguradorEntero} depende del valor
 * de {@link {@link ConfiguradorFlotante#INCREMENTO_LÍMITE}}, debido a una mejor adecuación
 * de cada algoritmo según el rango del intervalo ({@link ConfiguradorFlotante#mínimo},
 *  {@link ConfiguradorFlotante#máximo}). Véase
 *  {@link ConfiguradorFlotante#convertirDesplazamiento(int, int)} y
 *  {@link ConfiguradorFlotante#getDesplazamiento(Object, int)}</p> para detalles.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorFlotante extends ConfiguradorSimple implements ConfiguradorTextual, ConfiguradorDesplazador {

	/**
	 * Valor umbral que determina el algoritmo relacionado con desplazadores.
	 */
	public static final double INCREMENTO_LÍMITE = 10000;

	/**
	 * Valor umbral que determina el texto a mostrar en desplazadores.
	 */
	public static final double VALOR_LÍMITE = Integer.MAX_VALUE;

	private double mínimo;
	private double máximo;
	private double puntoMedio;

	/**
	 * Valor del configurador.
	 */
	protected double valor;

	/**
	 * Constructor que permite la introducción del valor inicial y el rango posible.
	 *
	 * <p>Si valorInicial no se encuentra en el rango, genera {@link RuntimeException}.</p>
	 *
	 * @param valorInicial Valor inicial del configurador.
	 * @param mínimo Mínimo valor del configurador.
	 * @param máximo Máximo valor del configurador.
	 */
	public ConfiguradorFlotante(double valorInicial, double mínimo, double máximo) {

		if(valorInicial < mínimo || valorInicial > máximo)
			throw new RuntimeException("Valor inicial inválido en ConfiguradorFlotante.");

		this.mínimo = mínimo;
		this.máximo = máximo;
		this.valor = valorInicial;
		puntoMedio = mínimo/2 + máximo/2;
	}

	/// Métodos sobrescritos ///

	/**
	 * <p>En este contexto, llamamos <i>incremento</i> a la proporción rangoConfigurador /
	 * rangoDesplazador. De tal forma que, multiplicando la proporción del desplazamiento
	 * argumento en su rango por el incremento se obtendrá la proporción en el rango del
	 * configurador.</p>
	 *
	 * <p>Si el incremento es menor que INCREMENTO_LÍMITE, se usará como punto de partida el valor mínimo más un valor
	 * proporcional al rango. Éste algoritmo es más preciso para rangos pequeños.</p>
	 *
	 * <p>Si el incremento es mayor o igual que INCREMENTO_LÍMITE, se partirá del punto medio y
	 * se sumará o restará un cierto número de incrementos. Éste algoritmo es más adecuado
	 * para rangos grandes, ya que evita posibles desbordamientos (para el cálculo del
	 * incremento utilizamos el punto medio con ese fin).</p>
	 *
	 * @param desplazamiento Entero entre 0 y desplazamientoMáximo que determinará el valor del configurador.
	 * @param desplazamientoMáximo Máximo desplazamiento.
	 * @return Valor del configurador.
	 */
	@Override
	public Object convertirDesplazamiento(int desplazamiento, int desplazamientoMáximo){
		int desplazamientoMedio = desplazamientoMáximo/2;
		double incremento = (máximo - puntoMedio) / desplazamientoMedio;
		if(incremento < INCREMENTO_LÍMITE) {
			return (máximo - mínimo) * (double)(desplazamiento)/desplazamientoMáximo + mínimo;
		}else {
			return puntoMedio + incremento * (desplazamiento - desplazamientoMedio);
		}
	}

	/**
	 * Realiza la operación inversa a {@link ConfiguradorFlotante#convertirDesplazamiento(int, int)},
	 * también en función del valor del incremento.
	 *
	 * @param valor Valor del configurador.
	 * @param desplazamientoMáximo Desplazamiento máximo del desplazador.
	 * @return Desplazamiento del {@link dc.gui.paneles.simples.Desplazador}.
	 */
	@Override
	public int getDesplazamiento(Object valor, int desplazamientoMáximo) {
		int desplazamientoMedio = desplazamientoMáximo/2;
		double incremento = (máximo - puntoMedio) / desplazamientoMedio;

		if(incremento < INCREMENTO_LÍMITE) {
			return (int) (desplazamientoMáximo * ((((Number) valor).doubleValue() - mínimo)) / (máximo - mínimo));
		}else {
			return (int) ((((Number) valor).doubleValue() - puntoMedio)/incremento + desplazamientoMedio);
		}
	}

	@Override
	public String getTextoDesplazador() {
		if(valor < -Integer.MAX_VALUE || Integer.MAX_VALUE < valor)
			return String.format("%6.3e",valor);
		
		return aTexto(2);
	}


	/**
	 * Elimina carácteres inválidos en un texto que debería contener un número flotante, positivo o negativo.
	 * Tanto "-" como "" se considerarán válidos.
	 *
	 * @param texto Texto a limpiar.
	 * @return Texto limpio.
	 */
	@Override
	public String limpiarTexto(String texto) {
		if(texto.equals("") || texto.equals("-")) return texto;
		StringBuilder sb = new StringBuilder();
		boolean punto = false;                      // Se ha encontrado el punto de la parte fraccionaria.

		//Primer elemento
		if(Character.isDigit(texto.charAt(0)) || texto.charAt(0) == '-') sb.append(texto.charAt(0));

		//Resto
		for(int i = 1; i < texto.length(); i++) {
			if(!punto && texto.charAt(i) == '.'){
				punto = true;
				sb.append(texto.charAt(i));
			}else if(Character.isDigit(texto.charAt(i))) {
				sb.append(texto.charAt(i));
			}
		}

		return  aTexto(2);
	}

	/**
	 * En caso de "" o "-" el configurador tendrá valor cero.
	 *
	 * @param texto Texto introducido en el componente gráfico.
	 * @return Valor del configurador.
	 */
	@Override
	public Object convertirTexto(String texto) {
		if(texto.equals("") || texto.equals("-")) {
			return 0;
		}else {
			return Double.parseDouble(texto);
		}
	}

	@Override
	public String getTexto() {
		return aTexto(20);
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(!(mensaje instanceof Number)) throw new RuntimeException("Argumento inválido en recibirValor de ConfiguradorFlotante: " + mensaje);

		double nuevoValor = ((Number) mensaje).doubleValue();

		if(comprobaciónLímites(nuevoValor)) {
			if(setValor(nuevoValor)) {
				//Avisamos al contenedor padre
				actualizar();

				//Cambio efectivo
				return Reacción.Propagar;
			}else {
				return Reacción.No_Propagar;
			}
		}else {
			//Se han transgredido los límites.

			//Adoptamos el valor límite
			setValor(nuevoValor);

			//No propagamos el mensaje, sino que iniciamos una nueva comunicación.
			comunicar();

			//Avisamos al contenedor padre
			actualizar();

			//Bloqueamos la propagación
			return Reacción.Bloquear;
		}
	}

	public Object obtenerValor() {
		return valor;
	}


	/// Métodos Privados ///

	/**
	 * Comprueba que un determinado valor se encuentra entre los límites válidos.
	 *
	 * @param valor {@link Integer} a comprobar
	 * @return {@link Boolean}, true si es válido.
	 */
	boolean comprobaciónLímites(double valor) {
		return mínimo <= valor && valor <= máximo;
	}

	/**
	 * Método mutador privado. Si valorNuevo no se encuentra en el rango (mínimo, máximo), será modificado
	 * al límite más cercano.
	 *
	 * @param valorNuevo Valor nuevo del configurador.
	 * @return true si hay cambio efectivo.
	 */
	private boolean setValor(double valorNuevo) {
		if(valorNuevo < mínimo) valorNuevo = mínimo;
		if(valorNuevo > máximo) valorNuevo = máximo;

		if(valor != valorNuevo) {
			valor = valorNuevo;
			return true;
		}
		return false;
	}

	/**
	 * Devuelve el texto asociado a número flotante, con un número limitado de decimales
	 *
	 * @param numMáximoDecimales Número de decimales de la representación textual.
	 * @return {@link String} representando a {@link ConfiguradorFlotante#valor}.
	 */
	private String aTexto(int numMáximoDecimales) {
		String s = String.format("%." + numMáximoDecimales + "f",valor).replace(',','.');
		int indice = s.length()-1;

		while(s.charAt(indice) == '0' && s.charAt(indice-1) != '.') indice--;

		return s.substring(0,indice+1);
	}

}
