package dc;

import dc.comunicador.Comunicador;

import javax.swing.*;

/**
 * Ancestro común de los configuradores de tipo básico.
 *
 * <p>Posee como atributos el componente gráfico, el {@link Comunicador} y el nombre,
 * proporcionando selectores y mutadores para los mismos.</p>
 *
 * <p>Además, provee a las clases hijas de un medio de comunicación con su
 * contenedor, para comunicar actualizaciones en su valor.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
abstract class ConfiguradorSimple implements Configurador {

	/**
	 * Canal por donde se transmitirán los mensajes de actualización
	 * al contenedor padre.
	 */
	protected static final String CANAL_ACTUALIZACIÓN = "Actualización";

	/**
	 * Mensaje enviado al contenedor padre para que sea consciente de
	 * una actualización.
	 */
	protected static class MensajeActualización {
		@Override
		public String toString() {
			return CANAL_ACTUALIZACIÓN;
		}
	}

	private final Comunicador comunicador = new Comunicador(this);

	private JComponent componenteGráfico;

	private String nombre;

	/**
	 * Genera un mensaje de actualización al contenedor padre.
	 */
	public void actualizar() {
		//Avisamos al contenedor padre de la actualización
		comunicar(CANAL_ACTUALIZACIÓN,new MensajeActualización());
	}

	@Override
	public void comunicar() {
		comunicar(obtenerValor());
	}


	@Override
	public Object obtenerValor() {
		if(this instanceof ConfiguradorBoolean) return ((ConfiguradorBoolean)this).getValor();
		if(this instanceof ConfiguradorChar) return ((ConfiguradorChar)this).getValor();
		if(this instanceof ConfiguradorString) return ((ConfiguradorString)this).getValor();

		if(this instanceof ConfiguradorFlotante) return ((ConfiguradorFlotante)this).obtenerValor();
		if(this instanceof ConfiguradorEntero) return ((ConfiguradorEntero)this).obtenerValor();

		throw new RuntimeException("Configurador inválido en getValor() de Configurador");
	}

	/**
	 * Útil para depuración.
	 *
	 * @return "Configurador " + nombre
	 */
	@Override
	public String toString() {
		if(nombre == null) nombre = "";
		return "Configurador " + nombre;
	}


	/// Selectores y Mutadores ///

	public JComponent getComponenteGráfico() {
		return componenteGráfico;
	}

	public void setComponenteGráfico(JComponent componenteGráfico) {
		this.componenteGráfico = componenteGráfico;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
		if(componenteGráfico != null) componenteGráfico.setName(nombre);
	}

	@Override
	public Comunicador getComunicador() {
		return comunicador;
	}
}
