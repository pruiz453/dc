package dc.gui;

/**
 * Métodos genéricos que han sido necesarios en diversas clases del proyecto.
 */
public class Utilidades {

    /**
     * Calcula el índice de un elemento {@link Object} en un array.
     *
     * <p>Si no se ha encontrado, devuelve -1.</p>
     *
     * @param vector {@link Object[]}. Array donde buscar.
     * @param elemento {@link Object}. Objeto a buscar.
     * @return
     */
    public static int getÍndice(Object [] vector, Object elemento) {
        for(int i = 0; i < vector.length; i++) {
            if (vector[i].equals(elemento)) return i;
        }

        return -1;
    }
}
