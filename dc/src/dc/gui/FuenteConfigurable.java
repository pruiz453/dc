package dc.gui;

import dc.*;
import dc.gui.paneles.Actualizable;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Permite la configuración de la fuente ({@link Font}) de un componente gráfico.
 *
 * <p>Puede establecerse el nombre de la fuente, su estilo (normal, negrita o cursiva),
 * su tamaño y su color.</p>
 *
 * <p>Las fuentes disponibles dependen del entorno de ejecución, aunque en todos hay
 * disponible al menos los tipos:</p>
 *
 * 1.- Dialog<br>
 * 2.- Monospaced<br>
 * 3.- Sans Serif<br>
 * 4.- Serif<br>
 *
 * <p>La aplicación de la {@link FuenteConfigurable} es recursiva sobre todos los
 * componentes y contenedores del componente gráfico.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class FuenteConfigurable  extends Clonable {

    public static String CURSIVA = "Cursiva";
    public static String NEGRITA = "Negrita";
    public static String NORMAL = "Normal";
    public final static String[] NOMBRES;

    //Obtenemos el nombre de las fuentes del sistema operativo
    static{
        Font[] fuentes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();

        NOMBRES = new String[fuentes.length + 4];

        NOMBRES[0] = Font.DIALOG;
        NOMBRES[1] = Font.MONOSPACED;
        NOMBRES[2] = Font.SANS_SERIF;
        NOMBRES[3] = Font.SERIF;

        for (int i = 0; i < fuentes.length; i++) {
            NOMBRES[i + 4] = fuentes[i].getName();
        }
    }

    private String nombre;
    private String estilo;
    private int tamaño;

    private ColorConfigurable color;

    /**
     * Elemento gráfico donde probar la muestra.
     */
    private JComponent muestrario;

    /**
     * Muestra donde aplicar una configuración de forma que la clase original
     * no sea modificada.
     */
    private FuenteConfigurable muestra;



    /**
     * Constructor por defecto, inicializado con {@link Font#SANS_SERIF}, estilo
     * {@link FuenteConfigurable#NORMAL}, en negro y de tamaño 14.
     */
    public FuenteConfigurable() {
        nombre = Font.SANS_SERIF;
        estilo = NORMAL;
        color = new ColorConfigurable();
        tamaño = 14;
    }



    /**
     * Constructor que permite la inicialización.
     *
     * @param nombre Nombre de la fuente.
     * @param estilo Estilo de la fuente.
     * @param tamaño Tamaño de letra.
     * @param color {@link ColorConfigurable} de letra.
     */
    public FuenteConfigurable(String nombre, String estilo, int tamaño, ColorConfigurable color) {
        this.nombre = nombre;
        this.estilo = estilo;
        this.tamaño = tamaño;
        this.color = color;
    }


    /**
     * Aplicación recursiva sobre los componentes de un contenedor
     * (ancestro de los componentes gráficos de java).
     *
     * <p>Para aplicar una {@link FuenteConfigurable} se usará
     * {@link Container#setFont(Font)} y {@link Container#setForeground(Color)},
     * que cambia el color de la letra.</p>
     *
     * <p>En el caso de que el contenedor implemente la interfaz {@link Actualizable},
     * actualizamos, lo cual reubica los componentes que pueden haber cambiado
     * de tamañao.</p>
     *
     * @param contenedor {@link Container} donde se aplicará la fuente.
     */
    public void aplicarFuente(Container contenedor) {
        contenedor.setFont(getFont());
        contenedor.setForeground(getColor());

        for(Component componente : contenedor.getComponents()) {
            if(componente instanceof Container) {
                aplicarFuente((Container) componente);
            }
        }

        if(contenedor instanceof Actualizable) {
            ((Actualizable) contenedor).actualizar();
        }
    }


    /**
     * Convierte el estilo en un entero, formato que aplica la librería {@link java.awt}.
     * @return Entero asociado al estilo en {@link java.awt}.
     */
    private int convertirEstilo() {
        if(estilo.equals(NORMAL))
            return Font.PLAIN;

        if(estilo.equals(CURSIVA))
            return Font.ITALIC;

        if(estilo.equals(NEGRITA))
            return Font.BOLD;

        throw new RuntimeException("Estilo inválido en FuenteConfigurable: " + estilo);
    }

    /**
     * Escribe los atributos de la instancia en un archivo asociado al
     * {@link ObjectOutputStream} pasado como argumento.
     *
     * @param oos {@link ObjectOutputStream}
     * @throws IOException
     */
    public void escribirEnDisco(ObjectOutputStream oos) throws IOException {
        oos.writeObject(nombre);
        oos.writeObject(estilo);
        oos.write(tamaño);

        color.escribirEnDisco(oos);
    }

    /**
     * Lee los atributos de la instancia en un archivo asociado al
     * {@link ObjectInputStream} pasado como argumento.
     *
     * @param ois {@link ObjectInputStream}
     * @throws IOException
     */
    public void leerDeDisco(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        nombre = (String) ois.readObject();
        estilo = (String) ois.readObject();
        tamaño = ois.read();

        color.leerDeDisco(ois);
    }

    /**
     * Crea la fuente (objeto {@link Font}) que debe aplicarse a los componentes.
     *
     * @return {@link Font} de {@link java.awt}
     */
    public Font getFont() {
        return new Font(nombre,convertirEstilo(),tamaño);
    }

    /**
     * {@link Color} asociado a {@link FuenteConfigurable#color}.
     * @return {@link Color} de {@link java.awt}
     */
    public Color getColor() {
        return color.getColor();
    }


    /// Selectores y Mutadores ///


    public int getTamaño() {
        return tamaño;
    }

    public JComponent getMuestrario() {
        return muestrario;
    }

    public ColorConfigurable getColorConfigurable() {
        return color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public void setColor(ColorConfigurable color) {
        this.color = color;
    }

    public void setMuestrario(JComponent muestrario) {
        this.muestrario = muestrario;
    }

    public FuenteConfigurable getMuestra() {
        return muestra;
    }

    public void setMuestra(FuenteConfigurable muestra) {
        this.muestra = muestra;
    }

    public static String getCURSIVA() {
        return CURSIVA;
    }

    public static void setCURSIVA(String CURSIVA) {
        FuenteConfigurable.CURSIVA = CURSIVA;
    }

    public static String getNEGRITA() {
        return NEGRITA;
    }

    public static void setNEGRITA(String NEGRITA) {
        FuenteConfigurable.NEGRITA = NEGRITA;
    }

    public static String getNORMAL() {
        return NORMAL;
    }

    public static void setNORMAL(String NORMAL) {
        FuenteConfigurable.NORMAL = NORMAL;
    }
}

