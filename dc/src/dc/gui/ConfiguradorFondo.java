package dc.gui;

import dc.*;

import java.util.List;

/**
 * Configura instancias {@link FondoConfigurable}.
 *
 * El método {@link ConfiguradorFondo#configurar(FondoConfigurable)} devuelve una
 * instancia {@link Configurador} para configurar {@link FondoConfigurable}.
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorFondo {

    /**
     * Crea un {@link Configurador} para la configuración de {@link FondoConfigurable}.
     *
     * <p>El configurador será un {@link ConfiguradorExtensible} asociado a
     * {@link dc.gui.paneles.extensibles.PanelPestañas}. La selección de la pestaña
     * configurará el estilo del fondo (color sólido o degradado). En cada pestaña
     * hay un {@link dc.gui.paneles.extensibles.PanelVertical} con los componentes gráficos
     * del {@link Configurador} de los {@link ColorConfigurable}.</p>
     *
     * <p>Los cambios que se produzcan en el {@link Configurador} se reproducirán en la
     * muestra de {@link FondoConfigurable} y se aplicarán a su muestrario.</p>
     *
     * @param fondo {@link FondoConfigurable} a configurar.
     * @return {@link Configurador}.
     */
    public static Configurador configurar(FondoConfigurable fondo) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        if(fondo.getMuestra() == null) fondo.setMuestra(new FondoConfigurable());
        FondoConfigurable muestra = fondo.getMuestra();

        String[] nombres = new String[]{fondo.COLOR_SÓLIDO,fondo.DEGRADADO};

        ConfiguradorLista cEstilo;

        ConfiguradorExtensible pestañas = factoría.extensiblePestañas("Fondo", cEstilo = new ConfiguradorString(
                Utilidades.getÍndice(nombres,fondo.getEstilo()),nombres) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                fondo.setEstilo(getValor());
            }
        });

        fondo.getColorSólido().setNombre(fondo.COLOR_SÓLIDO);
        Configurador cColorSólido = ConfiguradorColor.configurar(fondo.getColorSólido());
        pestañas.agregar(cColorSólido);

        ConfiguradorExtensible vDegradado = factoría.extensibleVertical();
        vDegradado.setNombre(fondo.DEGRADADO);

        fondo.getColorExterno().setNombre("Color Externo");
        Configurador cExterno = ConfiguradorColor.configurar(fondo.getColorExterno());
        vDegradado.agregar(cExterno);

        fondo.getColorInterno().setNombre("Color Interno");
        Configurador cInterno =ConfiguradorColor.configurar(fondo.getColorInterno());
        vDegradado.agregar(cInterno);

        factoría.agregarBarras(vDegradado);
        pestañas.agregar(vDegradado);


        cEstilo.agregarAcción(valor -> {
            muestra.setEstilo((String) valor);
            if(fondo.getMuestrario() != null)
                muestra.aplicarFondo(fondo.getMuestrario());
        });

        cEstilo.comunicar();

        cColorSólido.agregarAcción(valor -> {
            muestra.setColorSólido(ColorConfigurable.convertir((List<Object>) valor));
            if(fondo.getMuestrario() != null)
                muestra.aplicarFondo(fondo.getMuestrario());
        });
        cColorSólido.comunicar();

        cExterno.agregarAcción(valor -> {
            muestra.setColorExterno(ColorConfigurable.convertir((List<Object>) valor));

            if(fondo.getMuestrario() != null)
                muestra.aplicarFondo(fondo.getMuestrario());
        });
        cExterno.comunicar();

        cInterno.agregarAcción(valor -> {
            muestra.setColorInterno(ColorConfigurable.convertir((List<Object>) valor));

            if(fondo.getMuestrario() != null)
                muestra.aplicarFondo(fondo.getMuestrario());
        });
        cInterno.comunicar();

        return pestañas;
    }
}
