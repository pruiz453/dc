package dc.gui;

import dc.Clonable;
import dc.gui.paneles.PanelImagenFondo;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Configura el <i>Aspecto</i> de un {@link JComponent}.
 *
 * <br>
 * <p>El {@link AspectoConfigurable} se compone de:</p>
 *
 * 1.- {@link BordeConfigurable}<br>
 * 2.- {@link FondoConfigurable}<br>
 * 3.- {@link FuenteConfigurable}<br>
 *
 * @author Pablo Ruiz Sánchez
 */
public class AspectoConfigurable extends Clonable implements Serializable {

    private FondoConfigurable fondo;
    private BordeConfigurable borde;
    private FuenteConfigurable fuente;

    /**
     * Constructor
     *
     * @param borde {@link BordeConfigurable}
     * @param fondo {@link FondoConfigurable}
     * @param fuente {@link FuenteConfigurable}
     */
    public AspectoConfigurable(BordeConfigurable borde, FondoConfigurable fondo, FuenteConfigurable fuente) {
        this.fondo = fondo;
        this.borde = borde;
        this.fuente = fuente;
    }

    /**
     * Aplica el <i>Aspecto</i> a un {@link JComponent}.
     *
     * <p>Véase {@link AspectoConfigurable} para detalles.</p>
     *
     * @param componente {@link JComponent} cuyo aspecto se va a modificar.
     */
    public void aplicar(JComponent componente) {
        if(fondo != null && componente instanceof PanelImagenFondo)
            fondo.aplicarFondo((PanelImagenFondo) componente);

        if(borde != null)
            borde.aplicarBorde(componente);

        if(fuente != null)
            fuente.aplicarFuente(componente);
    }

    /**
     * Unifica todas las muestras de los atributos {@link BordeConfigurable},
     * {@link FondoConfigurable} y {@link FuenteConfigurable} en una muestra de
     * {@link AspectoConfigurable}.
     */
    public AspectoConfigurable getMuestra() {
        BordeConfigurable muestraBorde = null;
        FondoConfigurable muestraFondo = null;
        FuenteConfigurable muestraFuente = null;

        if(borde != null) muestraBorde = borde.getMuestra();

        if(fondo != null) muestraFondo = fondo.getMuestra();

        if(fuente != null) muestraFuente = fuente.getMuestra();

        return new AspectoConfigurable(muestraBorde,muestraFondo,muestraFuente);
    }

    @Override
    public String toString() {
        return "Borde:\n" + borde + "Fondo:\n" + fondo + "Fuente:\n" + fuente;
    }


    /**
     * Escribe los atributos de la instancia en un archivo asociado al
     * {@link ObjectOutputStream} pasado como argumento.
     *
     * @param oos {@link ObjectOutputStream}
     * @throws IOException
     */
    public void escribirEnDisco(ObjectOutputStream oos) throws IOException {
        if(borde != null) borde.escribirEnDisco(oos);
        if(fondo != null) fondo.escribirEnDisco(oos);
        if(fuente != null) fuente.escribirEnDisco(oos);
    }

    /**
     * Lee los atributos de la instancia en un archivo asociado al
     * {@link ObjectInputStream} pasado como argumento.
     *
     * <p>Obsérvese que la instancia que se escribió y la que se lee han de ser compatibles:
     * poseer los mismos atributos no nulos. En otro cacso, se generará {@link IOException}.</p>
     *
     * @param ois {@link ObjectInputStream}
     * @throws IOException
     */
    public void leerDeDisco(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        if(borde != null) borde.leerDeDisco(ois);
        if(fondo != null) fondo.leerDeDisco(ois);
        if(fuente != null) fuente.leerDeDisco(ois);
    }


    public FondoConfigurable getFondo() {
        return fondo;
    }

    public BordeConfigurable getBorde() {
        return borde;
    }

    public FuenteConfigurable getFuente() {
        return fuente;
    }

}
