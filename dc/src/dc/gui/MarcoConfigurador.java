package dc.gui;


import dc.*;
import dc.comunicador.Comunicativo;
import dc.gui.paneles.MensajeRedimensión;
import dc.gui.paneles.PanelBotones;

import javax.swing.*;
import java.awt.event.*;

/**
 * <p>Marco para los diálogos de configuración.</p>
 *
 * <p>Como argumento principal recibe un objeto {@link Configurador}, del que obtendrá
 * su componente gráfico. Con él construirá un {@link PanelBotones}, que será su
 * panel asociado.</p>
 *
 * <p>Centrado en la pantalla por defecto.</p>
 *
 * <br>
 * <p><b>Comportamiento Aceptar</b></p>
 * <p>Al aceptar, se invocará {@link Configurador#validar()}.
 * Si el retorno es una cadena con contenido, lo presenta en un diálogo usando
 * {@link FactoríaConfiguración#mensaje(String, String, FactoríaConfiguración.Acción)}.
 * En otro caso, llamará a {@link Configurador#salvar()} de
 * y por último {@link MarcoConfigurador#terminar()}.</p>
 *
 * <br>
 * <p><b>Comportamiento Cancalar</b></p>
 * Se invocará {@link MarcoConfigurador#cancelar()}</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class MarcoConfigurador extends Marco implements Comunicativo {

	/**
	 * Constructor.
	 * @param título Aparecerá en la barra de título.
	 * @param configurador Contiene el componente gráfico y determina el comportamiento
	 *                     al aceptar.
	 */
	public MarcoConfigurador(String título, Configurador configurador) {
		super(título);
		if(configurador == null) throw new NullPointerException("Configurador null en constructor MarcoConfigurador.");

		JComponent componente = configurador.getComponenteGráfico();
		if(componente == null) throw new NullPointerException("Configurador con componente gráfico null en constructor MarcoConfigurador.");

		PanelBotones panelBotones = new PanelBotones(componente) {
			@Override
			public void aceptar() {
				//Validamos
				String validación = configurador.validar();

				if(validación == null || validación.length() == 0) {
					//Salvamos
					configurador.salvar();
					dispose();
					terminar();
				}else {
					//Hay errores
					FactoríaConfiguración.getInstancia().mensaje("Error",validación, null);
				}
			}

			@Override
			public void cancelar() {
				dispose();
				MarcoConfigurador.this.cancelar();
			}
		};

		setContentPane(panelBotones);
		panelBotones.subscribir(MensajeRedimensión.CANAL,this);


		//Al cerrar la ventana, cancelamos.
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				super.windowClosing(windowEvent);
				cancelar();
			}
		});

		//Ajustamos el tamaño inicial mínimo y máximo
		procesarMensaje(new MensajeRedimensión(MensajeRedimensión.Tipo.Mínimo,
				panelBotones.getMinimumSize()));

		procesarMensaje(new MensajeRedimensión(MensajeRedimensión.Tipo.Máximo,
				panelBotones.getMaximumSize()));
	}


	/**
	 * Método ejecutado tras una configuración exitosa (con valores validados y salvados).
	 */
	public abstract void terminar();

	/**
	 * Método ejecutado si la configuración es cancelada.
	 */
	public abstract void cancelar();

}