package dc.gui.paneles;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * <p>Panel que permite situar una imagen de fondo.</p>
 *
 * <p>En caso de que fondo no se haya inicializado,
 * no consume recursos y permite mantener la opacidad,
 * pudiendo optar por un fondo monocromático.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelImagenFondo extends JPanel {

	private BufferedImage imagen;

	/**
	 * Constructor que mantiene el fondo opaco y sin
	 * inicializar el campo imagen.
	 *
	 */
	public PanelImagenFondo() {	}

	/**
	 * Constructor que inicializa fondo y elimina la opacidad
	 * en caso de que el argumento sea no nulo.
	 * @param fondo {@link BufferedImage} de fondo.
	 */
	public PanelImagenFondo(BufferedImage fondo) {
		setImagen(fondo);
	}

	@Override
	public void paint(Graphics g) {
		if(imagen != null) {

			Dimension tamaño = getSize();
			g.drawImage(imagen, 0, 0, tamaño.width, tamaño.height, null);
		}
		super.paint(g);
	}


	/// Selectores y Mutadores ///

	public BufferedImage getImagen() {
		return imagen;
	}

	/**
	 * Si el argumento es no nulo, elimina la opacidad.
	 *
	 * @param imagen {@link BufferedImage} de fondo.
	 */
	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;

		setOpaque(imagen == null);
	}

}