package dc.gui.paneles;

import dc.comunicador.Reacción;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * <p>Panel con componente centrado horizontalmente, pasado como argumento en el constructor,
 * y dos botones <i>Aceptar</i> y <i>Cancelar</i> debajo del mismo, cuyas acciones vienen
 * dadas por métodos abstractos.</p>
 *
 * <p>Es el panel de un {@link dc.gui.MarcoConfigurador}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class PanelBotones extends PanelEmisor {
    public static final int MARGEN_DEFECTO = 50;
    public static final int ESPACIO_BOTONES_DEFECTO = 16;

    private int margen;
    private int espacioBotones;

    private JButton botónAceptar;
    private JButton botónCancelar;
    private Component componente;

    /**
     * Constructor
     *
     * @param componente {@link Component} Componente gráfico que aparecerá
     *                                    sobre los botones {@link PanelBotones#botónAceptar}
     *                                    y {@link PanelBotones#botónCancelar}.
     */
    public PanelBotones(Component componente) {
        margen = MARGEN_DEFECTO;
        espacioBotones = ESPACIO_BOTONES_DEFECTO;
        this.componente = componente;

        //// Creamos botones ////
        botónAceptar = new JButton("Aceptar");
        botónAceptar.addActionListener(actionEvent -> aceptar());

        botónCancelar = new JButton("Cancelar");
        botónCancelar.addActionListener(actionEvent -> cancelar());


        //// Agregamos componentes ////
        add(botónAceptar);
        add(botónCancelar);
        add(componente);

        //Posicionamiento de componentes - Diseño - Layout
        setLayout(null);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent event) {
                posicionar();
            }
        });

        //Tamaño Inicial
        ajustarTamaños();
    }


    //Métodos Abstractos

    /**
     * Acción asociada a {@link PanelBotones#botónAceptar}.
     */
    public abstract void aceptar();

    /**
     * Acción asociada a {@link PanelBotones#botónCancelar}.
     */
    public abstract void cancelar();


    /**
     * Posicionamiento o distribución de los componentes gráficos del {@link PanelBotones}
     */
    private void posicionar() {
        Dimension dPanel = getSize();
        Dimension dAceptar = botónAceptar.getPreferredSize();
        Dimension dCancelar = botónCancelar.getPreferredSize();

        botónAceptar.setBounds(dPanel.width/2 - dAceptar.width - espacioBotones /2, dPanel.height - margen - dAceptar.height * 3/2,dAceptar.width,dAceptar.height);
        botónCancelar.setBounds(dPanel.width/2 + espacioBotones /2, dPanel.height - margen - dCancelar.height* 3/2,dCancelar.width,dCancelar.height);
        componente.setBounds(margen, margen,dPanel.width - margen *2, dPanel.height - dAceptar.height - margen *3);
    }

    /**
     * Cálculo del tamaño del {@link PanelBotones}.
     *
     * @param tamañoComponente Puede ser su tamaño mínimo, máximo o predilecto.
     *
     * @return Tamaño del {@link PanelBotones}.
     */
    private Dimension tamañoPanel(Dimension tamañoComponente) {
        Dimension dAceptar = botónAceptar.getPreferredSize();
        Dimension dCancelar = botónCancelar.getPreferredSize();
        Dimension tamaño = new Dimension();

        tamaño.width  = tamañoComponente.width  + margen * 2;
        tamaño.height = tamañoComponente.height + margen * 3 + dAceptar.height;

        //Ancho mínimo del marco sin considerar el componente.
        int anchoMínimo = dAceptar.width + dCancelar.width + espacioBotones + margen *2;
        if(tamaño.width < anchoMínimo)
            tamaño.width = anchoMínimo;

        return tamaño;
    }

    /**
     * Ajusta los tamaño mínimo, máximo y predilecto.
     */
    private void ajustarTamaños() {
        setMinimumSize(tamañoPanel(componente.getMinimumSize()));
        setMaximumSize(tamañoPanel(componente.getMaximumSize()));
        setPreferredSize(tamañoPanel(componente.getPreferredSize()));
    }

    @Override
    public Reacción procesarMensaje(Object mensaje) {
        if(mensaje instanceof MensajeRedimensión) {
            ajustarTamaños();
        }
        return Reacción.No_Propagar;
    }

    public int getMargen() {
        return margen;
    }

    public void setMargen(int margen) {
        this.margen = margen;
        posicionar();
    }

    public int getEspacioBotones() {
        return espacioBotones;
    }

    public void setEspacioBotones(int espacioBotones) {
        this.espacioBotones = espacioBotones;
        posicionar();
    }
}
