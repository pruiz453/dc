package dc.gui.paneles.simples;

import dc.gui.distribuciones.Alineación;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Panel con un recuadro de texto {@link JTextArea} de varias líneas situado bajo
 * una etiqueta
 * ({@link JLabel}) que
 * muestra el nombre del panel.</p>
 *
 * <p><IMG SRC="../../../../Imágenes/recuadro.png"/></p>
 *
 * <p>Véase {@link dc.gui.paneles.simples.PanelTextual} para detalles sobre el comportamiento.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Recuadro extends PanelTextual {
    public static final int ALTO_TEXTO_DEFECTO = 40;
    public static final Alineación ALINEACIÓN_TÍTULO_DEFECTO = Alineación.Centrada;

    private Alineación alineaciónTítulo = ALINEACIÓN_TÍTULO_DEFECTO;

    /**
     * Constructor
     */
    public Recuadro() {
        super(new JTextArea());

        setAltoTexto(ALTO_TEXTO_DEFECTO);

        agregar(componenteNombre, 0, alineaciónTítulo);
        agregar(entradaTexto, 1, Alineación.Expansible);

    }


    public int getAltoTexto() {
        return entradaTexto.getPreferredSize().height;
    }

    public void setAltoTexto(int altoTexto) {

        Dimension d = entradaTexto.getPreferredSize();

        d.height = altoTexto;

        entradaTexto.setMinimumSize(d);
        entradaTexto.setMaximumSize(new Dimension(Integer.MAX_VALUE,altoTexto));
        entradaTexto.setPreferredSize(d);

        actualizar();
    }

    public Alineación getAlineaciónTítulo() {
        return alineaciónTítulo;
    }

    public void setAlineaciónTítulo(Alineación alineaciónTítulo) {
        this.alineaciónTítulo = alineaciónTítulo;
        eliminar(componenteNombre,0);
        agregar(componenteNombre,0,alineaciónTítulo);
        actualizar();
    }
}
