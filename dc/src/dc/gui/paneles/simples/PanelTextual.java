package dc.gui.paneles.simples;

import dc.comunicador.Reacción;

import javax.swing.text.JTextComponent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * <p>Panel con una entrada de texto ({@link JTextComponent}) que puede poseer una o varias
 * filas,
 * y una etiqueta {@link javax.swing.JLabel} que indica el nombre del panel.</p>
 *
 * <p>Permite el procesado del texto. Para ello se debe incorporar un
 * {@link PanelTextual.ProcesadorTexto} con
 * el método {@link PanelTextual#setProcesador(ProcesadorTexto procesador)}.
 * Ante cambios en la entrada de
 * texto, el panel procesa el contenido, modificándolo potencialmente, si se ha establecido
 * procesador, previo a comunicaciones.</p>
 *
 * <p>La creación y la distribución del componente textual se delega en las clases hijas.</p>
 *
 * <p>Comunica las variaciones en el componente textual por el sistema de comunicación por
 * paso de mensajes de la aplicación.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelTextual extends PanelSimple{

    /**
     * Permite procesar una cadena de caracteres.
     */
    public interface ProcesadorTexto {
        String procesar(String texto);
    }

    protected JTextComponent entradaTexto;

    private ProcesadorTexto procesador;

    /**
     * Constructor
     *
     * @param entradaTexto Componente que permite la entrada de texto.
     */
    public PanelTextual(JTextComponent entradaTexto) {
        this.entradaTexto = entradaTexto;

        //Comunicación
        entradaTexto.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                //Procesamos
                if(procesador != null) {
                    String nuevoTexto = procesador.procesar(entradaTexto.getText());

                    //Solo modificamos en caso de que el procesador provoque un cambio
                    //efectivo, ya que las modificaciones mueven el cursor.
                    if(! nuevoTexto.equals(entradaTexto.getText())) {
                        entradaTexto.setText(nuevoTexto);
                    }
                }

                comunicar(entradaTexto.getText());
            }
        });
    }

    @Override
    public Reacción procesarMensaje(Object mensaje) {
        if(mensaje instanceof String) {
            String textoAnterior = entradaTexto.getText();
            boolean cambio = !textoAnterior.equals(mensaje);

            //Solo modificamos en caso de que el haya un cambio
            //efectivo, ya que las modificaciones mueven el cursor.
            if(cambio) entradaTexto.setText((String) mensaje);

            if(cambio) {
                return Reacción.Propagar;
            }else {
                return Reacción.No_Propagar;
            }
        }else {
            throw new RuntimeException("Valor inválido en recibirValor de PanelTextual: " + mensaje);
        }
    }

    public String getValor() {
        return entradaTexto.getText();
    }

    public ProcesadorTexto getProcesador() {
        return procesador;
    }

    public void setProcesador(ProcesadorTexto procesador) {
        this.procesador = procesador;
    }

    public JTextComponent getComponenteTextual() {
        return entradaTexto;
    }
}
