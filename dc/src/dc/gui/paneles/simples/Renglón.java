package dc.gui.paneles.simples;

import dc.gui.distribuciones.Alineación;

import javax.swing.*;
import java.awt.*;

/**
 * Incorpora a {@link JTextField} cambios en su tamaño mínimo y predilecto ante
 * cambios de fuente.
 *
 * @author Pablo Ruiz Sánchez
 */
class CampoTexto extends JTextField {
    @Override
    public void setFont(Font f) {
        super.setFont(f);

        JLabel etiqueta = new JLabel("Á");
        etiqueta.setFont(f);

        Dimension d = getMinimumSize();
        d.height = etiqueta.getPreferredSize().height;

        setMinimumSize(d);
        setPreferredSize(d);
    }
}

/**
 * <p>Panel con un recuadro de texto de una única línea situada a la derecha de una etiqueta
 * ({@link JLabel}) que muestra el nombre del panel.</p>
 *
 * <p>El ancho es ajustable numéricamente o eligiendo dos tamaños predefinidos, cuyos valores
 * son también ajustables.</p>
 *
 * <p><IMG SRC="../../../../Imágenes/renglón.png"/></p>
 *
 * <p>Véase {@link dc.gui.paneles.simples.PanelTextual} para detalles sobre el comportamiento.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Renglón extends PanelTextual {

    /**
     * Indicará el ancho del {@link JTextField}.
     */
    public enum AnchoTexto {
        Normal,
        Grande
    }

    public static final int ANCHO_NORMAL_DEFECTO = 80;
    public static final int ANCHO_GRANDE_DEFECTO = 100;

    private int anchoTextoNormal = ANCHO_NORMAL_DEFECTO;
    private int anchoTextoGrande = ANCHO_GRANDE_DEFECTO;

    private AnchoTexto ancho;

    /**
     * Constructor.
     */
    public Renglón() {
        super(new CampoTexto());

        agregar(componenteNombre, 0, Alineación.Izquierda);
        agregar(entradaTexto, 0, Alineación.Derecha);

        ancho = AnchoTexto.Normal;
        setAnchoTexto(anchoTextoNormal);
        ((JTextField)entradaTexto).setHorizontalAlignment(SwingConstants.RIGHT);
    }


    private void setAnchoTexto(int ancho) {
        Dimension d = entradaTexto.getPreferredSize();

        d.width = ancho;

        entradaTexto.setMinimumSize(d);
        entradaTexto.setMaximumSize(d);
        entradaTexto.setPreferredSize(d);

        actualizar();
    }

    public void setAnchoTexto(AnchoTexto ancho) {
        this.ancho = ancho;

        switch (ancho) {
            case Normal:
                setAnchoTexto(anchoTextoNormal);
                break;
            case Grande:
                setAnchoTexto(anchoTextoGrande);
                break;
        }
    }

    public int getAnchoTextoNormal() {
        return anchoTextoNormal;
    }

    public void setAnchoTextoNormal(int anchoTextoNormal) {
        this.anchoTextoNormal = anchoTextoNormal;

        if(ancho == AnchoTexto.Normal) setAnchoTexto(anchoTextoNormal);
    }

    public int getAnchoTextoGrande() {
        return anchoTextoGrande;
    }

    public void setAnchoTextoGrande(int anchoTextoGrande) {
        this.anchoTextoGrande = anchoTextoGrande;

        if(ancho == AnchoTexto.Grande) setAnchoTexto(anchoTextoGrande);
    }

    public AnchoTexto getAncho() {
        return ancho;
    }

    public void setAncho(AnchoTexto ancho) {
        this.ancho = ancho;
    }
}
