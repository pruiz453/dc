package dc.gui.paneles.simples;

import dc.comunicador.Reacción;
import dc.gui.distribuciones.Alineación;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Lista desplegable, que agrega a {@link JComboBox} cambios en su tamaño predilecto
 * ante cambios de fuente.</p>
 *
 * <p>Para ello crea un modelo, al que le aplica la fuente y obtiene su tamaño predilecto.</p>
 *
 * <p>Además controla el ancho mínimo.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
class ListaDesplegable extends JComboBox<String> {

    private int anchoMínimo;

    @Override
    public void setFont(Font font) {
        super.setFont(font);

        if(isShowing()) {
            JComboBox<String> modelo = new JComboBox<>();

            modelo.setFont(font);

            for(int i = 0; i < getItemCount(); i++) {
                modelo.addItem(getItemAt(i));
            }

            setPreferredSize(modelo.getPreferredSize());

            aplicarAnchoMínimo();
        }
    }

    public int getAnchoMínimo() {
        return anchoMínimo;
    }

    public void aplicarAnchoMínimo() {
        setAnchoMínimo(anchoMínimo);
    }

    public void setAnchoMínimo(int anchoMínimo) {
        this.anchoMínimo = anchoMínimo;

        Dimension dSelector = getPreferredSize();
        dSelector.width = Integer.max(dSelector.width,anchoMínimo);
        setPreferredSize(dSelector);
    }

    public void setAncho(int ancho) {
        Dimension dSelector = getPreferredSize();
        dSelector.width = ancho;
        setPreferredSize(dSelector);
    }
}

/**
 * <p>Panel que posee una lista desplegable y una etiqueta ({@link JLabel}) con su nombre.</p>
 *
 * <p>Comunica los cambios en el elemento seleccionado por el sistema de comunicación
 * por paso de mensajes de la aplicación.</p>
 *
 * <p>El ancho mínimo es configurable.</p>
 *
 * <p><IMG SRC="../../../../Imágenes/lista.png"/></p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Lista extends PanelSimple {

    public static int ANCHO_MÍNIMO_LISTA_DEFECTO = 80;

    private final ListaDesplegable selector;

    /**
     * Constructor
     */
    public Lista() {
        //Creamos e inicializamos
        selector = new ListaDesplegable();
        selector.setAnchoMínimo(ANCHO_MÍNIMO_LISTA_DEFECTO);

        agregar(componenteNombre,0, Alineación.Izquierda);
        agregar(selector,0, Alineación.Derecha);

        //Comunicación
        selector.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                comunicar((String) selector.getSelectedItem());
            }
        });
    }

    @Override
    public Reacción procesarMensaje(Object mensaje) {
        if(mensaje instanceof String) {
            Object anterior = selector.getSelectedItem();

            selector.setSelectedItem(mensaje);

            if(! anterior.equals(mensaje)) {
                return Reacción.Propagar;
            }else {
                return Reacción.No_Propagar;
            }
        }
        throw new RuntimeException("Valor inválido en recibirValor de Lista: " + mensaje);
    }

    public void setOpciones(String[] opciones) {
        for(String opción : opciones) {
            selector.addItem(opción);
        }

        selector.aplicarAnchoMínimo();
    }

    public void setAnchoLista(int ancho) {

        selector.setAncho(ancho);

        actualizar();
    }

    public void setAnchoMínimoLista(int ancho) {

        selector.setAnchoMínimo(ancho);

        actualizar();
    }

    public int getAnchoMínimoLista() {
        return selector.getAnchoMínimo();
    }

    public String getValor() {
        return selector.getSelectedItem().toString();
    }
}
