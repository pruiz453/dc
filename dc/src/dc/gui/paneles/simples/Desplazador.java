package dc.gui.paneles.simples;

import dc.ConfiguradorDesplazador;
import dc.comunicador.Reacción;
import dc.gui.distribuciones.Alineación;

import javax.swing.*;

/**
 * <p>Panel con un desplazador {@link JSlider} que permite configurar valores numéricos.
 * Posee además una etiqueta ({@link JLabel}) con el nombre del panel y un recuadro de
 * texto creado para indicar
 * el valor numérico asociado al desplazamiento del desplazador. El {@link Desplazador} no
 * establece dicho texto sino que delega la responsabilidad en {@link dc.FactoríaConfiguración}
 * y ésta acude a su
 * vez a {@link ConfiguradorDesplazador#getTextoDesplazador()}, ya que el texto depende de
 * de si están configurando valores enteros, flotantes, del rango... El {@link Desplazador}
 * puede usarse
 * en otro contexto, pero en dicho caso, si no se regula, no se mostrará el valor del
 * desplazamiento.</p>
 *
 * <p>Comunica los desplazamientos por el sistema de comunicación por paso de mensajes
 * de la aplicación.</p>
 *
 * <p>El listener del desplazador ({@link javax.swing.event.ChangeListener}) se activa
 * ante cualquier cambio, incluso
 * aquellos provocados por la mismo panel {@link Desplazador} debido a algún mensaje recibido.
 * Para evitar comportamientos anómalos, la variable
 * {@link Desplazador#comunicarDesplazamiento}, impide las comunicaciones
 * en dicho caso.</p>
 *
 * <p><IMG SRC="../../../../Imágenes/desplazador.png"/></p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Desplazador extends PanelSimple {

    private JLabel campoTexto;
    private JSlider desplazador;

    /**
     * Regula la comunicación de desplazamientos.
     */
    private boolean comunicarDesplazamiento;

    /**
     * Constructor.
     */
    public Desplazador() {
        //Inicialización
        comunicarDesplazamiento = true;

        //Creamos los componentes
        campoTexto = new JLabel(" ");
        desplazador = new JSlider();

        //Configuramos los componentes
        campoTexto.setHorizontalAlignment(SwingConstants.RIGHT);
        desplazador.setOpaque(false);

        //Disposición
        agregar(componenteNombre,0, Alineación.Izquierda);
        agregar(campoTexto,0, Alineación.Derecha);
        agregar(desplazador,1,Alineación.Expansible);

        //Comunicación
        desplazador.addChangeListener(changeEvent -> {

            if (comunicarDesplazamiento) {
                comunicar(desplazador.getValue());
            }
        });
    }

    @Override
    public Reacción procesarMensaje(Object mensaje) {

        if(mensaje instanceof Number) {
            int valorNuevo = ((Number) mensaje).intValue();
            int valorAnterior = desplazador.getValue();

            //Restringimos las comunicaciones y ajustamos el desplazamiento
            comunicarDesplazamiento = false;

            desplazador.setValue(valorNuevo);
            comunicarDesplazamiento = true;

            if(valorAnterior != (Integer) mensaje) {
                return Reacción.Propagar;
            }else {
                return Reacción.No_Propagar;
            }

        }else{
            throw new RuntimeException("Valor inválido en recibirValor de Desplazador: " + mensaje);
        }
    }

    public int desplazamientoMáximo() {
        return desplazador.getMaximum();
    }

    public void setTexto(String texto) {
        campoTexto.setText(texto);
        actualizar();
    }

    public int getValor() {
        return desplazador.getValue();
    }

}
