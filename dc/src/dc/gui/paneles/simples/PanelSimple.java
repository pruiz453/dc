package dc.gui.paneles.simples;

import dc.comunicador.Comunicativo;
import dc.gui.paneles.PanelMatricial;

import javax.swing.*;

/**
 * <p>Ancestro común de todos los paneles que configuran tipos básicos.</p>
 *
 * <p>Mantiene un componente que debe tener
 * todo {@link PanelSimple} que indica el nombre del argumento a configurar. Por defecto
 * dicho componente es un {@link JLabel}, aunque las clases hijas pueden cambiarlo. Las
 * llamadas al método {@link java.awt.Component#setName(String)} modificarán el texto de
 * dicho componente.</p>
 *
 * <p>Obsérvese que la carencia de un ancestro común en librería java para las clases
 * {@link JLabel} y {@link JToggleButton} con el método <i>setText(String texto)</i>, obliga
 * a hacer distinciones.
 * Esta visicitud provoca que si en una ampliación se agrega una clase hija que sustituya
 * {@link PanelSimple#componenteNombre} por otra clase distinta a las citadas, se deberá
 * agregar una distinción
 * extra al código de {@link PanelSimple#setName(String nombre)}.
 * A pesar de ello, sigue siendo producente este
 * diseño, al evitar duplicación de código.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class PanelSimple extends PanelMatricial implements Comunicativo {

    protected JComponent componenteNombre;

    /**
     * Constructor
     */
    public PanelSimple() {
        componenteNombre = new JLabel();
    }

    @Override
    public void setName(String name) {
        super.setName(name);

        if (componenteNombre instanceof JLabel) {
            ((JLabel) componenteNombre).setText(name);
        }

        if (componenteNombre instanceof JToggleButton) {
            ((JToggleButton) componenteNombre).setText(name);
        }
    }


    @Override
    public String toString() {
        return "Panel " + getName();
    }

    // Selectores y Mutadores


    public JComponent getComponenteNombre() {
        return componenteNombre;
    }

    public void setComponenteNombre(JComponent componenteNombre) {
        this.componenteNombre = componenteNombre;
        componenteNombre.setOpaque(false);
    }
}