package dc.gui.paneles.simples;

import dc.comunicador.Reacción;
import dc.gui.distribuciones.Alineación;

import javax.swing.*;

/**
 * <p>Panel con una casilla de verificación.</p>
 *
 * <p>El texto de la casilla es el nombre del panel. Comunica los cambios en la casilla
 * por el sistema de comunicación por paso de mensajes de la aplicación.</p>
 *
 * <p>El estilo de la casilla es configurable: {@link Casilla.Estilo}.</p>
 *
 * <p><IMG SRC="../../../../Imágenes/casilla1.png"/></p>
 *
 * <p><IMG SRC="../../../../Imágenes/casilla2.png"/></p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Casilla extends PanelSimple {
    public enum Estilo {
        Cuadrada,
        Redonda;

        /**
         * Convierte una cadena de caracteres en un {@link Casilla.Estilo}.
         */
        public static Estilo convertir(String estilo) {
            if(estilo.equals(Cuadrada.toString())) return Cuadrada;
            if(estilo.equals(Redonda.toString())) return Redonda;

            throw new RuntimeException("Cadena inválida en convertir de Casilla: " + estilo);
        }

        /**
         * Array de los nombres de los estilos.
         */
        public static String[] getNombres() {
            return new String[] {Cuadrada.toString(),Redonda.toString()};
        }
    }

    public static final Estilo ESTILO_DEFECTO = Estilo.Cuadrada;

    private Estilo estilo = ESTILO_DEFECTO;

    private JToggleButton casilla;

    /**
     * Constructor
     */
    public Casilla() {
        inicializar();
    }

    /**
     * Llamado en el constructor o ante cambios de estilo.
     */
    private void inicializar(){
        //Inicializamos casilla
        switch (estilo) {
            case Cuadrada:
                casilla = new JCheckBox(getName());
                break;
            case Redonda:
                casilla = new JRadioButton(getName());
                break;
        }

        casilla.setFont(getFont());
        casilla.setForeground(getForeground());

        agregar(casilla,0, Alineación.Izquierda);

        //Sustituye el componente nombre de PanelSimple, por defecto un JLabel.
        setComponenteNombre(casilla);

        //Comunicación
        casilla.addActionListener(actionEvent -> comunicar(casilla.isSelected()));
    }

    @Override
    public Reacción procesarMensaje(Object mensaje) {

        if(mensaje instanceof Boolean) {
            Boolean marcada = (Boolean) mensaje;
            Boolean marcaAnterior = casilla.isSelected();

            casilla.setSelected(marcada);

            if(! marcaAnterior.equals(marcada)) {
                return Reacción.Propagar;
            }else {
                return Reacción.No_Propagar;
            }
        }else {
            throw new RuntimeException("Valor inválido en recibirValor de Casilla: " + mensaje);
        }
    }

    public boolean getValor() {
        return casilla.isSelected();
    }

    public Estilo getEstilo() {
        return estilo;
    }

    /**
     * Modifica el estilo de la casilla. Para ello, elimina la anterior y crea una
     * nueva, configurándola y estableciendo el mismo estado (seleccionada o no) de
     * la anterior.
     */
    public void setEstilo(Estilo estilo) {

        if(this.estilo != estilo) {
            boolean marcada = casilla.isSelected();

            this.estilo = estilo;

            eliminar(casilla,0);
            inicializar();
            actualizar();

            casilla.setSelected(marcada);
        }
    }
}
