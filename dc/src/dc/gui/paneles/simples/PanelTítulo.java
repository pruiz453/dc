package dc.gui.paneles.simples;

import dc.gui.distribuciones.Alineación;
import dc.gui.paneles.PanelMatricial;

import javax.swing.*;

/**
 * Panel con un título, {@link JLabel} centrado horizontal y verticalmente.
 * Los cambios en el nombre
 * afectan al texto de la etiqueta.
 */
public class PanelTítulo extends PanelMatricial {

    public static final int MARGEN = 10;
    public static final int GANANCIA = 0;

    private JLabel etiqueta;

    /**
     * Constructor
     * @param texto {@link String} del título y nombre del {@link PanelTítulo}.
     */
    public PanelTítulo(String texto) {
        setName(texto);

        etiqueta = new JLabel(texto);
        agregar(etiqueta,0, Alineación.Centrada);

        getDistribución().setMargen(MARGEN);
        getDistribución().setGananciaAltura(GANANCIA);
    }

    @Override
    public void setName(String nombre) {
        super.setName(nombre);

        if(etiqueta != null) {
            etiqueta.setText(nombre);
            actualizar();
        }
    }

    public void setMargen(int margen) {
        getDistribución().setMargen(margen);
        actualizar();
    }
}
