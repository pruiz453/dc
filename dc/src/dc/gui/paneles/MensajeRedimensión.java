package dc.gui.paneles;

import java.awt.*;

/**
 * <p>Mensaje pueden transmitir los elementos gráficos {@link dc.comunicador.Comunicativo}
 * ante cambios en su tamaño predilecto o sus límites mínimo o máximo.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class MensajeRedimensión {

    public static final String CANAL = "Redimensión";

    /**
     * Indica si el tamaño modificado es el mínimo, máximo o predilecto.
     */
    public enum Tipo {
        Mínimo,
        Máximo,
        Predilecto
    }

    private Tipo tipo;
    private Dimension tamaño;


    /**
     * Constructor
     *
     * @param tipo {@link Tipo}
     * @param tamaño {@link Dimension}
     */
    public MensajeRedimensión(Tipo tipo, Dimension tamaño) {
        this.tipo = tipo;
        this.tamaño = tamaño;
    }

    @Override
    public String toString() {
        return "Redimensión " + tipo + "("+tamaño.width+","+tamaño.height+")";
    }

    public Tipo getTipo() {
        return tipo;
    }

    public Dimension getTamaño() {
        return tamaño;
    }
}
