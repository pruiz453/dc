package dc.gui.paneles.extensibles;


import dc.comunicador.Reacción;
import dc.gui.paneles.MensajeRedimensión;
import dc.gui.paneles.PanelEmisor;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Nodo del árbol {@link JTree}. Tendrán asociado un componente gráfico.
 *
 * @author Pablo Ruiz Sánchez
 */
class Nodo extends DefaultMutableTreeNode {
	private Component componente;

	/**
	 * Constructor
	 * 
	 * @param componente {@link Component} que aparecerá en {@link PanelÁrbol} al
	 *                                    ser seleccionado el {@link Nodo}.   
	 */
	public Nodo(Component componente) {
		super(componente.getName());
		this.componente = componente;
	}

	//Selectores y Mutadores

	public Component getComponente() {
		return componente;
	}
}

/**
 * <p>Contenedor gráfico que posee un panel izquierdo con una estructura de árbol ({@link JTree})
 * cuyos nodos ({@link Nodo}) poseen asociados componentes gráficos. Al seleccionar un {@link Nodo},
 * su componente se visualiza a la derecha.</p>
 *
 * <p>El ancho del {@link JTree} se establece automáticamente, pero también es ajustable
 * interactivamente. En el caso de que haya un reajusto, el ancho automático se desactiva.
 * Ajustar el ancho también provoca cambios en el tamaño mínimo del panel, que serán
 * comunicados por el mecanismo de paso de mensajes global de la aplicación. Los reajustes
 * máximo y automático se basan en porcentaje del tamaño del {@link PanelÁrbol}, para que se
 * mantengan coherentes ante redimensiones. El tamaño mínimo no es proporcional.
 * Este enfoque proporciona un comportamiento más natural al componente.</p>
 *
 * <p>Concretamente el panel posee a lo sumo tres elementos gráficos simultáneamente: el
 * {@link JTree}, envuelto en un {@link PanelBarras}, el ajustador de su ancho y,
 * si hubiera un {@link Nodo} seleccionado, su componente gráfico.</p>
 *
 * <p>La altura mínima inicial es dependiente del tamaño de la pantalla y se ha establecido a
 * la mitad de esta. El ancho mínimo inicial depende del nombre del panel, pues éste aparecerá
 * como raíz del árbol. Dicho nombre cambiará ante modificaciones en el nombre del panel con
 * {@link JComponent#setName(String nombre)}.</p>
 *
 * <p>Al agregar un componente al panel, se creará un {@link Nodo} que lo contenga y
 * aparecerá en el {@link JTree}. Si el componente es otro {@link PanelÁrbol}, se agregará su
 * raíz como un nodo contenedor.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelÁrbol extends PanelEmisor {

	private static final int ANCHO_AJUSTADOR = 10;

	public static final int ANCHO_MÍNIMO_ÁRBOL_DEFECTO = 50;
	public static final float PORCENTAJE_APARICIÓN_ÁRBOL_DEFECTO = 0.25f;
	public static final float PORCENTAJE_MÁXIMO_ANCHO_ÁRBOL_DEFECTO = 0.30f;

	public static final float PORCENTAJE_ALTURA_MÍNIMA_INICIAL = 0.5f;

	/**
	 * Agregado al ancho por defecto de {@link JTree} (decisión estética)
	 */
	public static final int INCREMENTO_ANCHO = 20;

	private float porcentajeMáximoAnchoÁrbol = PORCENTAJE_MÁXIMO_ANCHO_ÁRBOL_DEFECTO;
	private float porcentajeApariciónÁrbol = PORCENTAJE_APARICIÓN_ÁRBOL_DEFECTO;
	private int anchoMínimoÁrbol = ANCHO_MÍNIMO_ÁRBOL_DEFECTO;


	private PanelBarras panelDesplazamiento;
	private JPanel ajustador;
	private JTree árbol;

	private DefaultMutableTreeNode raíz;

	private Component componenteActivo;

	private boolean ajusteProporcional;

	/**
	 * Ancho del {@link JTree}
	 */
	private int anchoÁrbol;

	/**
	 * Constructor defecto.
	 */
	public PanelÁrbol() {
		inicializar("");
	}

	/**
	 * Constructor
	 *
	 * @param nombre Aparecerá en la raíz del {@link JTree}.
	 */
	public PanelÁrbol(String nombre){
		inicializar(nombre);
		setName(nombre);
	}

	/**
	 * Código común a ambos constructores.
	 */
	private void inicializar(String nombre){
		//Creamos y agregamos el {@link JTree}
		raíz = new DefaultMutableTreeNode(nombre);
		árbol = new JTree(raíz);
		panelDesplazamiento = new PanelBarras(árbol);

		super.add(panelDesplazamiento);

		árbol.getSelectionModel().setSelectionMode
				(TreeSelectionModel.SINGLE_TREE_SELECTION);

		//Creamos y agregamos el ajustador
		super.add(ajustador = new JPanel());
		ajustador.setOpaque(false);

		//Ajustes iniciales
		setOpaque(false);
		ajusteProporcional = true;

		//Comportamiento al seleccionar un nodo
		árbol.addTreeSelectionListener(treeSelectionEvent -> {
			if(árbol.getLastSelectedPathComponent() instanceof Nodo){
				Nodo nodo = (Nodo) árbol.getLastSelectedPathComponent();

				if(componenteActivo != null) componenteActivo.setVisible(false);
super.add(nodo.getComponente());
				nodo.getComponente().setVisible(true);
				componenteActivo = nodo.getComponente();
				posicionar();
				ajustarTamaños();
			}
		});

		//Al entrar en el ajustador cambiamos el puntero
		ajustador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(Cursor.getDefaultCursor());
			}
		});

		//Comportamiento ajustador
		ajustador.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				ajusteProporcional = false;
				ajustarAnchoÁrbol(e.getXOnScreen());
				posicionar();
			}
		});

		//Posicionamiento de componentes
		setLayout(null);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				super.componentResized(e);

				ajustarAnchoÁrbol();
				posicionar();
			}
		});

		//Tamaño inicial
		ajustarTamaños();
	}

	/**
	 * Ajusta el ancho del {@link JTree} sin interacción de ratón
	 */
	private void ajustarAnchoÁrbol() {
		ajustarAnchoÁrbol(0);
	}

	/**
	 * Ajusta el ancho del {@link JTree} cuando el ratón está redimensionando.
	 * Para el ajuste interactivo se exige que la coordenada del ratón no sea 0,
	 * o negativa, ya que en tal caso, el panel desaparecería.
	 */
	private void ajustarAnchoÁrbol(int xRatón) {
		if (ajusteProporcional){					//Ajuste automático
			anchoÁrbol =  (int) (getSize().width * porcentajeApariciónÁrbol);;
		}else if(xRatón > 0 && isShowing()) {		//Ajuste interactivo o manual
			int anchoMáximoÁrbol = (int) (getSize().width * porcentajeMáximoAnchoÁrbol);

			anchoÁrbol = xRatón - getLocationOnScreen().x;

			if(anchoÁrbol > anchoMáximoÁrbol) anchoÁrbol = anchoMáximoÁrbol;
			if(anchoÁrbol < anchoMínimoÁrbol) anchoÁrbol = anchoMínimoÁrbol;
		}

		ajustarTamaños();
	}

	/**
	 * Posicionamiento de componentes.
	 */
	public void posicionar() {
		int anchoPanel = getSize().width - anchoÁrbol;

		panelDesplazamiento.setBounds(0,0,anchoÁrbol ,getSize().height);

		if(componenteActivo != null)  componenteActivo.setBounds(anchoÁrbol,0,anchoPanel,getSize().height);

		ajustador.setBounds(anchoÁrbol - ANCHO_AJUSTADOR/2, 0, ANCHO_AJUSTADOR, getSize().height);
	}

	@Override
	public Component add(Component comp) {

		//Si el componente es un PanelÁrbol, agregamos su raíz al JTree
		if(comp instanceof PanelÁrbol) {
			PanelÁrbol árbol = (PanelÁrbol) comp;
			raíz.add(árbol.getRaíz());
		}else {
			//En otro caso, agregamos un nuevo nodo asociado al componente.

			raíz.add(new Nodo(comp));
			comp.setVisible(false);
		}

		return null;
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(mensaje instanceof MensajeRedimensión) {
			ajustarTamaños();
		}
		return Reacción.No_Propagar;
	}

	/**
	 * Obtiene el tamaño del {@link PanelÁrbol} dado el tamaño de
	 * {@link PanelÁrbol#componenteActivo}.
	 */
	private Dimension tamañoÁrbol(Dimension tamañoComponente) {
		Dimension tamaño = new Dimension();

		tamaño.width = tamañoComponente.width + anchoÁrbol;
		tamaño.height = tamañoComponente.height;

		return tamaño;
	}

	/**
	 * Tamaño inicial del árbol, dependiente de su nombre y del alto de la pantalla.
	 */
	private Dimension tamañoÁrbol() {
		Dimension tamañoÁrbol = árbol.getPreferredSize();

		Dimension tamaño = new Dimension();

		tamaño.width = (int) (tamañoÁrbol.width / porcentajeApariciónÁrbol) + INCREMENTO_ANCHO;
		tamaño.height = (int) (PORCENTAJE_ALTURA_MÍNIMA_INICIAL *
				Toolkit.getDefaultToolkit().getScreenSize().height);

		return tamaño;
	}

	/**
	 * Ajusta los tamaños mínimo, máximo y predilecto.
	 */
	private void ajustarTamaños() {
		if(componenteActivo != null) {
			setMinimumSize(tamañoÁrbol(componenteActivo.getMinimumSize()));
			setPreferredSize(tamañoÁrbol(componenteActivo.getPreferredSize()));
			setMaximumSize(tamañoÁrbol(componenteActivo.getMaximumSize()));
		}else {
			Dimension tamañoÁrbol = tamañoÁrbol();
			setMinimumSize(tamañoÁrbol);
			setPreferredSize(tamañoÁrbol);
		}
	}

	@Override
	public void setName(String name) {
		super.setName(name);

		//Al cambiar el nombre cambiamos el nombre del nodo raíz.
		raíz.setUserObject(name);
	}

	@Override
	public Dimension getPreferredSize() {
		//El tamaño predilecto será el mínimo.
		return getMinimumSize();
	}

	private DefaultMutableTreeNode getRaíz() {
		return raíz;
	}

	public float getPorcentajeMáximoAnchoÁrbol() {
		return porcentajeMáximoAnchoÁrbol;
	}

	public void setPorcentajeMáximoAnchoÁrbol(float porcentajeMáximoAnchoÁrbol) {
		this.porcentajeMáximoAnchoÁrbol = porcentajeMáximoAnchoÁrbol;
		ajustarAnchoÁrbol();
		posicionar();
	}

	public float getPorcentajeApariciónÁrbol() {
		return porcentajeApariciónÁrbol;
	}

	public void setPorcentajeApariciónÁrbol(float porcentajeApariciónÁrbol) {
		this.porcentajeApariciónÁrbol = porcentajeApariciónÁrbol;
		ajustarAnchoÁrbol();
		posicionar();
	}

	public int getAnchoMínimoÁrbol() {
		return anchoMínimoÁrbol;
	}

	public void setAnchoMínimoÁrbol(int anchoMínimoÁrbol) {
		this.anchoMínimoÁrbol = anchoMínimoÁrbol;
		ajustarAnchoÁrbol();
		posicionar();
	}
}