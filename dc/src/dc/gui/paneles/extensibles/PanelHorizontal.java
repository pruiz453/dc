package dc.gui.paneles.extensibles;

import dc.gui.distribuciones.DistribuciónHorizontal;

import java.awt.*;

/**
 * Este panel constituye la aplicación de una {@link DistribuciónHorizontal} a un {@link javax.swing.JPanel}.
 * Limita el número de componentes a 3.
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelHorizontal extends PanelDimensional {

	public static final int NÚMERO_MÁXIMO_COMPONENTES = 3;

	/**
	 * Constructor
	 */
	public PanelHorizontal() {
		distribución = new DistribuciónHorizontal(this);
	}

	@Override
	public Component add(Component componente) {

		//Comprobamos el límite de componentes
		if(getComponentCount() >= NÚMERO_MÁXIMO_COMPONENTES) {
			throw new RuntimeException("Superado el número máximo de componentes en PanelHorizontal.");
		}

		return super.add(componente);
	}

}
