package dc.gui.paneles.extensibles;

import dc.ConfiguradorLista;
import dc.comunicador.Comunicador;
import dc.comunicador.Comunicativo;
import dc.comunicador.Reacción;
import dc.gui.paneles.MensajeRedimensión;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Extensión de {@link JTabbedPane} (contenedor con pestañas),
 * incorporando el sistema de comunicación que permite el control sobre
 * los tamaños mínimo, máximo y predilecto.</p>
 *
 * <p>Además, el panel puede ser utilizarse para interactuar con un {@link dc.ConfiguradorLista},
 * del mismo modo que una lista desplegable, usando para ello el método
 * {@link dc.FactoríaConfiguración#extensiblePestañas(String, ConfiguradorLista)}</p>
 *
 * <p>Los tamaños límite mínimo, máximo y predilecto dependerán de la pestaña activa.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelPestañas extends JTabbedPane implements Comunicativo {

	/**
	 * Altura de las pestañas.
	 */
	public static final int ALTURA_ADICIONAL = 25;

	private final Comunicador comunicador = new Comunicador(this);

	/**
	 * Constructor
	 */
	public PanelPestañas() {

		//Al cambiar de pestaña, lo comunciamos y ajustamos los tamaños conforme
		//al nuevo componente visualizado.
		addChangeListener(changeEvent -> {
			if(getComponentCount() != 1)
				comunicar(getSelectedComponent().getName());

			ajustarTamaños();
		});
	}

	@Override
	public Component add(Component comp) {

		//Nos subscribirmos a las redimensiones del componente.
		if(comp instanceof Comunicativo) {
			((Comunicativo) comp).subscribir(MensajeRedimensión.CANAL,this);
		}

		return super.add(comp);
	}

	@Override
	public void setMinimumSize(Dimension tamañoMínimo) {
		Dimension tamañoAnterior = getMinimumSize();

		if(! tamañoAnterior.equals(tamañoMínimo)) {
			super.setMinimumSize(tamañoMínimo);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Mínimo, tamañoMínimo));
		}
	}

	@Override
	public void setPreferredSize(Dimension tamañoPredilecto) {
		Dimension tamañoAnterior = getPreferredSize();

		if(! tamañoAnterior.equals(tamañoPredilecto)) {
			super.setPreferredSize(tamañoPredilecto);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Predilecto, tamañoPredilecto));
		}
	}

	@Override
	public void setMaximumSize(Dimension tamañoMáximo) {
		Dimension tamañoAnterior = getMaximumSize();

		if(! tamañoAnterior.equals(tamañoMáximo)) {
			super.setMaximumSize(tamañoMáximo);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Máximo, tamañoMáximo));
		}
	}

	/**
	 * Dado el tamaño del componente activo, devuelve el tamaño del {@link PanelPestañas},
	 * sumando la altura de la barra de pestañas.
	 */
	private Dimension tamañoPanelPestañas(Dimension tamañoComponenteActivo) {
		Dimension tamaño = new Dimension();
		tamaño.width = tamañoComponenteActivo.width;
		tamaño.height = tamañoComponenteActivo.height + ALTURA_ADICIONAL;

		return tamaño;
	}

	/**
	 * Ajusta los tamaños mínimo, máximo y predilecto en función del componente activo.
	 */
	private void ajustarTamaños(){
		setMinimumSize(tamañoPanelPestañas(getSelectedComponent().getMinimumSize()));
		setPreferredSize(tamañoPanelPestañas(getSelectedComponent().getPreferredSize()));
		setMaximumSize(tamañoPanelPestañas(getSelectedComponent().getMaximumSize()));
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {

		//Debe de ser una redimensión o un cambio de pestaña.

		if(mensaje instanceof MensajeRedimensión) {

			ajustarTamaños();

			return Reacción.No_Propagar;

		}else if(mensaje instanceof String) {
			//Obtenemos la cadena de la pestaña seleccionada (de haber)
			String seleccionada = null;
			if(getSelectedComponent() != null)
				seleccionada = getSelectedComponent().getName();

			//Cambiamos de pestaña
			for(Component componente : getComponents()) {
				if(componente.getName().equals(mensaje)) setSelectedComponent(componente);
			}

			//Ajustamos los tamaño, al haber cambiado potencialmente el componente activo
			ajustarTamaños();

			if(!mensaje.equals(seleccionada)) {
				return Reacción.Propagar;
			}else {
				return Reacción.No_Propagar;
			}
		}else {
			throw new RuntimeException("Valor inválido en recibirValor de PanelPestañas: " + mensaje);
		}
	}

	@Override
	public Comunicador getComunicador() {
		return comunicador;
	}
}
