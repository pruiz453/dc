package dc.gui.paneles.extensibles;

import dc.gui.distribuciones.DistribuciónVertical;

/**
 * Este panel constituye la aplicación de una {@link DistribuciónVertical} a
 * un {@link javax.swing.JPanel}.
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelVertical extends PanelDimensional {

    /**
     * Constructor
     */
    public PanelVertical() {
        distribución = new DistribuciónVertical(this);
    }

}
