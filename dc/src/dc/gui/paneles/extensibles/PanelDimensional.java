package dc.gui.paneles.extensibles;

import dc.comunicador.Reacción;
import dc.gui.distribuciones.DistribuciónDimensional;
import dc.gui.paneles.MensajeRedimensión;
import dc.gui.paneles.PanelEmisor;

/**
 * <p>Aplicación de una {@link DistribuciónDimensional} a un {@link javax.swing.JPanel}.
 * Reúne el código común de {@link PanelVertical} y {@link PanelHorizontal}.</p>
 *
 * <p>Cuando se recibe un {@link MensajeRedimensión}, se reposicionan los componentes, lo que
 * además conlleva un cambio en los tamaños mínimo, máximo y/o predilecto del {@link PanelDimensional}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelDimensional extends PanelEmisor {

    protected DistribuciónDimensional distribución;

    /**
     * Procesamiento de mensajes {@link MensajeRedimensión}.
     *
     * <p>Si recibimos un mensaje de un cambio en los límites del tamaño de
     * un componente, reposicionamos el panel. La reposición conlleva
     * cambios en los tamaños límite del {@link PanelDimensional}.</p>
     *
     * @param mensaje Véase {@link dc.comunicador.Comunicativo}
     * @return No se reenviarán mensajes de redimensión, ya que al posicionar, redimensionamos,
     *          provocando el inicio de comunicaciones.
     */
    @Override
    public Reacción procesarMensaje(Object mensaje) {
        if(mensaje instanceof MensajeRedimensión) {

            distribución.posicionarComponentes();
        }
        return Reacción.No_Propagar;
    }

    public DistribuciónDimensional getDistribución() {
        return distribución;
    }
}
