package dc.gui.paneles.extensibles;

import dc.comunicador.Comunicador;
import dc.comunicador.Comunicativo;
import dc.comunicador.Reacción;
import dc.gui.paneles.MensajeRedimensión;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * <p>Extensión de {@link JScrollPane} (barras de desplazamiento),
 * incorporando el sistema de comunicación que permite el control sobre
 * los tamaños mínimo, máximo y predilecto.</p>
 *
 * <p>En el proyecto no se considerarán barras horizontales, de manera que el
 * ancho mínimo depende del componente a envolver. El alto mínimo depende
 * del tamaño de la pantalla.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelBarras extends JScrollPane implements Comunicativo {

    /**
     * Al multiplicar por el alto de la pantalla se obtiene
     * el tamaño mínimo del panel.
     */
    public static float PORCENTAJE_ALTO_PANTALLA_DEFECTO = 0.3f;

    /**
     * Cota superior del ancho de las barras verticales.
     */
    private static final int ANCHO_ADICIONAL = 25;

    private float porcentajeAltoPantalla = PORCENTAJE_ALTO_PANTALLA_DEFECTO;

    private final Comunicador comunicador = new Comunicador(this);

    private Component componente;

    /**
     * Constructor
     *
     * @param componente {@link Component} al que se añadirán las barras.
     */
    public PanelBarras(Component componente) {
        super(componente);

        this.componente = componente;

        //Nos subscribimos a redimensiones del componente
        if(componente instanceof Comunicativo) {
            ((Comunicativo) componente).subscribir(MensajeRedimensión.CANAL,this);
        }

        //Solución a los problemas de visualización
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                paintAll(getGraphics());
            }
        });

        //Tamaño mínimo inicial
        ajustarTamañoMínimo(componente.getPreferredSize().width + ANCHO_ADICIONAL);
    }

    /**
     * Dado un tamaño, le suma el tamaño de las barras de desplazamiento.
     */
    private void aumentarTamaño(Dimension tamaño) {
        tamaño.width += ANCHO_ADICIONAL;
    }

    /**
     * Esteblece el tamaño mínimo dado un ancho que depende del componente.
     */
    private void ajustarTamañoMínimo(int ancho) {
        int altoMínimo = (int ) (porcentajeAltoPantalla * Toolkit.getDefaultToolkit().getScreenSize().height);

        setMinimumSize(new Dimension(ancho ,altoMínimo));
    }

    @Override
    public void setMinimumSize(Dimension tamañoMínimo) {
        Dimension tamañoAnterior = getMinimumSize();

        if(! tamañoAnterior.equals(tamañoMínimo)) {
            super.setMinimumSize(tamañoMínimo);
            comunicar(MensajeRedimensión.CANAL,
                    new MensajeRedimensión(MensajeRedimensión.Tipo.Mínimo, tamañoMínimo));
        }
    }

    @Override
    public void setPreferredSize(Dimension tamañoPredilecto) {
        Dimension tamañoAnterior = getPreferredSize();

        if(! tamañoAnterior.equals(tamañoPredilecto)) {
            super.setPreferredSize(tamañoPredilecto);
            comunicar(MensajeRedimensión.CANAL,
                    new MensajeRedimensión(MensajeRedimensión.Tipo.Predilecto, tamañoPredilecto));
        }
    }

    @Override
    public void setMaximumSize(Dimension tamañoMáximo) {
        Dimension tamañoAnterior = getMaximumSize();

        if(! tamañoAnterior.equals(tamañoMáximo)) {
            super.setMaximumSize(tamañoMáximo);
            comunicar(MensajeRedimensión.CANAL,
                    new MensajeRedimensión(MensajeRedimensión.Tipo.Máximo, tamañoMáximo));
        }
    }


    @Override
    public Reacción procesarMensaje(Object cuerpo) {
        if(cuerpo instanceof MensajeRedimensión) {
            MensajeRedimensión mensaje = (MensajeRedimensión) cuerpo;

            aumentarTamaño(mensaje.getTamaño());

            switch (mensaje.getTipo()) {
                case Mínimo:
                    ajustarTamañoMínimo(mensaje.getTamaño().width);
                    break;
                case Máximo:
                    setMaximumSize(mensaje.getTamaño());
                    break;
                case Predilecto:
                    setPreferredSize(mensaje.getTamaño());
                    break;
            }
        }
        return Reacción.No_Propagar;
    }

    @Override
    public Comunicador getComunicador() {
        return comunicador;
    }

    public float getPorcentajeAltoPantalla() {
        return porcentajeAltoPantalla;
    }

    public void setPorcentajeAltoPantalla(float porcentajeAltoPantalla) {
        this.porcentajeAltoPantalla = porcentajeAltoPantalla;

        ajustarTamañoMínimo(getMinimumSize().width);
    }

    public Component getComponente() {
        return componente;
    }
}
