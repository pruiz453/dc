package dc.gui.paneles;

import dc.comunicador.Comunicador;
import dc.comunicador.Comunicativo;
import dc.comunicador.Reacción;

import java.awt.*;

public class PanelEmisor extends PanelImagenFondo implements Comunicativo {

	private final Comunicador comunicador = new Comunicador(this);

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		return Reacción.No_Propagar;
	}

	@Override
	public Component add(Component comp) {
		if(comp instanceof Comunicativo) {
			((Comunicativo) comp).subscribir(MensajeRedimensión.CANAL,this);
		}

		return super.add(comp);
	}

	@Override
	public void setMinimumSize(Dimension tamañoMínimo) {
		Dimension tamañoAnterior = getMinimumSize();

		if(! tamañoAnterior.equals(tamañoMínimo)) {
			super.setMinimumSize(tamañoMínimo);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Mínimo, tamañoMínimo));
		}
	}

	@Override
	public void setPreferredSize(Dimension tamañoPredilecto) {
		Dimension tamañoAnterior = getPreferredSize();

		if(! tamañoAnterior.equals(tamañoPredilecto)) {
			super.setPreferredSize(tamañoPredilecto);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Predilecto, tamañoPredilecto));
		}
	}

	@Override
	public void setMaximumSize(Dimension tamañoMáximo) {
		Dimension tamañoAnterior = getMaximumSize();

		if(! tamañoAnterior.equals(tamañoMáximo)) {
			super.setMaximumSize(tamañoMáximo);
			comunicar(MensajeRedimensión.CANAL,
					new MensajeRedimensión(MensajeRedimensión.Tipo.Máximo, tamañoMáximo));
		}
	}


	@Override
	public Comunicador getComunicador() {
		return comunicador;
	}
}
