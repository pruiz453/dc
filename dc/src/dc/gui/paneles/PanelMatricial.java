package dc.gui.paneles;

import dc.gui.distribuciones.Alineación;
import dc.gui.distribuciones.DistribuciónMatricial;

import java.awt.*;

/**
 * <p>Aplicación de una {@link DistribuciónMatricial} a un {@link javax.swing.JPanel}.</p>
 *
 * <p>Los componentes se situarán por filas con una {@link Alineación}. Véase
 * {@link DistribuciónMatricial} para consultar las restricciones establecidas.</p>
 *
 * <p>Implementa la interfaz {@link Actualizable}. El panel debe ser actualizado si
 * se realiza un cambio de fuente en sus componentes.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class PanelMatricial extends PanelEmisor implements Actualizable {

	private DistribuciónMatricial distribución;

	/**
	 * Constructor
	 */
	public PanelMatricial() {
		distribución = new DistribuciónMatricial(this);
	}

	/**
	 * Agrega un componente a una fila.
	 *
	 * @param componente {@link Component}
	 * @param fila Índice de la fila.
	 * @param alineación {@link Alineación} del componente.
	 */
	public void agregar(Component componente, int fila, Alineación alineación) {
		add(componente);

		distribución.agregar(componente,fila,alineación);
	}

	/**
	 * <p>Elimina un componente del contenedor.</p>
	 *
	 * @param componente {@link Component} a eliminar.
	 * @param fila Índice de la fila donde se halla.
	 */
	public void eliminar(Component componente, int fila) {
		remove(componente);

		distribución.eliminar(componente,fila);
	}

	/**
	 * Reposiciona los componentes del panel.
	 */
	public void actualizar() {
		distribución.posicionarComponentes();
	}

	public DistribuciónMatricial getDistribución() {
		return distribución;
	}
}
