package dc.gui.paneles;

/**
 * Interfaz que implementan los panales que permiten actualizaciones externas.
 *
 * <p>Una actualización repinta los componentes reubicándolos si es necesario.</p>
 */
public interface Actualizable {
    /**
     * Repinta los componentes reubicándolos si es necesario.
     */
    void actualizar();
}
