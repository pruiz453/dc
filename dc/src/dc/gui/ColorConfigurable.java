package dc.gui;

import dc.Clonable;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Clase que guarda los tonos de un color. Permitiendo modificaciones.
 *
 * <p>Es necesaria debido al carácter inmutable de {@link Color}.</p>
 *
 * <p>La clase mantiene un atributo ({@link ColorConfigurable#modificado}), de forma que
 * indica si ha sufrido cambios desde la última consulta (o llamada) a
 * {@link ColorConfigurable#modificado()}, que reestablecerá el valor de
 * {@link ColorConfigurable#modificado}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class ColorConfigurable extends Clonable {

    private String nombre;

    private int r;
    private int g;
    private int b;

    private boolean modificado;

    /**
     * Constructor inicializado en Negro: todos los tonos con valor 0.
     */
    public ColorConfigurable() {
        modificado = false;
    }

    /**
     * Constructor que permite la inicialización de los tonos.
     *
     * @param r Rojo.
     * @param g Verde.
     * @param b Azul.
     */
    public ColorConfigurable(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        modificado = false;
    }

    @Override
    public String toString() {
        return nombre + " (" + r+ ", " + g + ", " + b + ")";
    }

    /**
     * Convierte una lista con tres valores numéricos (las tonalidades)
     * en un {@link ColorConfigurable}.
     *
     * <p>Necesario para la aplicación en tiempo real de un color a una muestra
     * durante la configuración de un {@link ColorConfigurable}</p>
     *
     * @param lista Lista de los tonos del {@link ColorConfigurable}
     * @return {@link ColorConfigurable} cuyos tonos se encontraban en la lista.
     */
    public static ColorConfigurable convertir(List<Object> lista) {
        if (!(lista.get(0) instanceof Number) ||
                !(lista.get(1) instanceof Number) ||
                !(lista.get(2) instanceof Number) )
            throw new RuntimeException("Lista inválida en convertir de ColorConfigurable");

        return new ColorConfigurable(
                ((Number) lista.get(0)).intValue(),
                ((Number) lista.get(1)).intValue(),
                ((Number) lista.get(2)).intValue());
    }

    /**
     * Conversión a {@link java.awt.Color}.
     *
     * @return {@link Color} asociado (de tonos coincidentes) al {@link ColorConfigurable}
     */
    public Color getColor() {
        return new Color(r,g,b);
    }

    /**
     * Escribe los tonos del {@link ColorConfigurable} en un archivo asociado al
     * {@link ObjectOutputStream} pasado como argumento.
     *
     * @param oos {@link ObjectOutputStream}
     * @throws IOException
     */
    public void escribirEnDisco(ObjectOutputStream oos) throws IOException {
        oos.write(r);
        oos.write(g);
        oos.write(b);
    }

    /**
     * Lee los tonos del {@link ColorConfigurable} en un archivo asociado al
     * {@link ObjectInputStream} pasado como argumento.
     *
     * @param ois {@link ObjectInputStream}
     * @throws IOException
     */
    public void leerDeDisco(ObjectInputStream ois) throws IOException {
        r = ois.read();
        g = ois.read();
        b = ois.read();
    }

    /// Selectores y Mutadores ///

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        modificado = modificado || this.r != r;
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        modificado = modificado || this.g != g;
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        modificado = modificado || this.b != b;
        this.b = b;
    }

    public boolean modificado() {
        boolean anteriorModificado = modificado;
        modificado = false;
        return anteriorModificado;
    }
}