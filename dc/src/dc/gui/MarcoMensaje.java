package dc.gui;

import dc.gui.distribuciones.Alineación;
import dc.gui.paneles.MensajeRedimensión;
import dc.gui.paneles.PanelMatricial;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Marco con un mensaje informativo y un botón Aceptar.
 *
 * <p>El mensaje aparecerá en una área no editable con barras de
 * desplazamiento, y el botón bajo ésta.</p>
 *
 * <p>La clase fue ideada para mostrar mensajes de error
 * potencialmente largos, de todos los objetos {@link dc.Configurador}
 * contenidos en {@link dc.ConfiguradorExtensible}</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class MarcoMensaje extends Marco {

    private static final int ANCHO_MÍNIMO_ÁREA = 300;
    private static final int ALTO_ÁREA = 200;
    private static final int GANANCIA = 0;

    private JButton botónAceptar;

    /**
     * Constructor
     *
     * @param título Aparecerá en la barra de título.
     * @param texto Aparecerá en el área de texto.
     */
    public MarcoMensaje(String título, String texto) {
        super(título);

        PanelMatricial panel = new PanelMatricial();
        panel.getDistribución().setGananciaAltura(GANANCIA);
        setContentPane(panel);
        panel.subscribir(MensajeRedimensión.CANAL,this);

        JTextArea areaTexto = new JTextArea(texto);
        areaTexto.setEditable(false);

        JScrollPane panelBarras = new JScrollPane(areaTexto);

        panelBarras.setMinimumSize(new Dimension(ANCHO_MÍNIMO_ÁREA, ALTO_ÁREA));
        panelBarras.setPreferredSize(new Dimension(ANCHO_MÍNIMO_ÁREA, ALTO_ÁREA));

        panel.agregar(panelBarras, 0 , Alineación.Expansible);

        botónAceptar = new JButton("Aceptar");

        panel.agregar(botónAceptar,1,Alineación.Centrada);

        botónAceptar.addActionListener(actionEvent -> {
            dispose();
            acción();
        });

        setLocationRelativeTo(null);
        //Al cerrar la ventana, aceptamos.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                super.windowClosing(windowEvent);
                botónAceptar.doClick();
            }
        });

    }

    protected abstract void acción();


    public JButton getBotónAceptar() {
        return botónAceptar;
    }

}
