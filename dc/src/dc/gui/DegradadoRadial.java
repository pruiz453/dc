package dc.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 * <p>Degradado radial con dos colores. La distancia con el centro de un pixel
 * determinará su color, linealmente, entre los colores interno y externo,
 * fijando así circunferencias isocromáticas.</p
 *
 * <p><b>Ejemplo 1:</b></p>
 * <IMG SRC="../../Imágenes/degradado1.png"/>
 * <p><b>Ejemplo 2:</b></p>
 * <IMG SRC="../../Imágenes/degradado2.png"/>
 * <p><b>Ejemplo 3:</b></p>
 * <IMG SRC="../../Imágenes/degradado3.png"/>
 *
 * @author Pablo Ruiz Sánchez
 */
public class DegradadoRadial extends BufferedImage implements Serializable {
	static final int ANCHO_DEFECTO = 600;
	static final int ALTO_DEFECTO = 600;

	/**
	 * Radio máximo.
	 */
	private double maxRadio;

	private final Color colorExterno;
	private final Color colorInterno;

	/**
	 * Constructor que determina los valores de los colores
	 * externo e interno y usa la dimensión por defecto.
	 * @param colorExterno {@link Color}
	 * @param colorInterno {@link Color}
	 */
	public DegradadoRadial(Color colorExterno, Color colorInterno) {
		super(ANCHO_DEFECTO, ALTO_DEFECTO, BufferedImage.TYPE_INT_RGB);
		this.colorExterno = colorExterno;
		this.colorInterno = colorInterno;

		inicializar();
	}

	/**
	 * Constructor que recibe la dimensión de la imagen y los colores externo e interno.
	 *
	 * @param ancho {@link Integer}
	 * @param alto {@link Integer}
	 * @param colorExterno {@link Color}
	 * @param colorInterno {@link Color}
	 */
	public DegradadoRadial(int ancho, int alto, Color colorExterno, Color colorInterno) {
		super(ancho, alto, BufferedImage.TYPE_INT_RGB);
		this.colorExterno = colorExterno;
		this.colorInterno = colorInterno;

		inicializar();
	}

	/**
	 * Acciones comunes a ambos constructores (crea la imagen)
	 */
	private void inicializar() {
		maxRadio = radio(0,0);

		for(int x = 0; x < getWidth(); x++) {
			for(int y = 0; y < getHeight(); y++) {
				setRGB(x,y, color(radio(x,y)).getRGB());
			}
		}
	}

	/**
	 * Determina la distancia con el centro de un punto de coordenadas (x,y).
	 * @param x
	 * @param y
	 * @return Radio. Distancia al centro.
	 */
	private double radio(int x, int y) {
		return Math.sqrt(Math.pow(x-getWidth()/2,2) + Math.pow(y-getHeight()/2,2));
	}

	/**
	 * Determina la insidad de una componente de un color (roja, verde o azul) dadas las componentes
	 * externas y un radio. Una posible subclase podría sobrescribir este método rompiendo quizás la linealidad.
	 * @param componenteExterna
	 * @param componenteInterna
	 * @param radio
	 * @return Término intermedio entre componenteInterna y componenteExterna en función del radio.
	 */
	protected int componenteColor(int componenteExterna, int componenteInterna, double radio) {
		double proporción = radio / maxRadio;
		return (int) (componenteInterna + proporción * (componenteExterna - componenteInterna));
	}

	/**
	 * Determina el color de un pixel dado un radio.
	 * @param radio
	 * @return Color del pixel.
	 */
	private Color color(double radio) {
		return new Color(
				componenteColor(colorExterno.getRed(),colorInterno.getRed(),radio),
				componenteColor(colorExterno.getGreen(),colorInterno.getGreen(),radio),
				componenteColor(colorExterno.getBlue(),colorInterno.getBlue(),radio)
		);
	}
}