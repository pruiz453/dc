package dc.gui;

import dc.comunicador.Comunicador;
import dc.comunicador.Comunicativo;
import dc.comunicador.Reacción;
import dc.gui.paneles.MensajeRedimensión;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Extensión de {@link JFrame} que integra el sistema de comunicación del sistema y
 * responde ante posibles envíos de {@link MensajeRedimensión} de su panel contenedor.
 *
 * <p>El tamaño recibido en los mensajes es aumentado en los márgenes del marco y
 * la barra de título.</p>
 *
 * <p>Cuando un mensaje cambia el tamaño del marco, su localización es modificada, de forma
 * que si estaba centrado, continúe estándolo.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Marco extends JFrame implements Comunicativo {

    private final Comunicador comunicador = new Comunicador(this);

    private static final int INCREMENTO_ANCHO;
    private static final int INCREMENTO_ALTO;

    /**
     * Cálculo de los márgenes del marco y barra de título.
     *
     * <p>Crea un {@link JFrame} vacío, de tamaño mínimo, y obtiene su dimensión.</p>
     */
    static {
        JFrame marco = new JFrame();
        marco.pack();
        INCREMENTO_ANCHO = marco.getMinimumSize().width;
        INCREMENTO_ALTO = marco.getMinimumSize().height;
    }

    /**
     * Constructor
     * @param título Aparecerá en la barra de título.
     * @throws HeadlessException
     */
    public Marco(String título) throws HeadlessException {
        super(título);

        //Aparición central por defecto
        setLocationRelativeTo(null);

        //Si estamos en windows nos aseguramos que se cumpla el tamaño mínimo
        if(System.getProperty("os.name").startsWith("Windows")) {
            addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    Dimension d = getSize();
                    Dimension minD = getMinimumSize();
                    if (d.width < minD.width)
                        d.width = minD.width;
                    if (d.height < minD.height)
                        d.height = minD.height;
                    setSize(d);
                }
            });
        }
    }


    @Override
    public Reacción procesarMensaje(Object cuerpo) {
        if(cuerpo instanceof MensajeRedimensión) {
            MensajeRedimensión mensaje = (MensajeRedimensión) cuerpo;

            //Aumentamos el tamaño con los márgenes del marco
            mensaje.getTamaño().width += INCREMENTO_ANCHO;
            mensaje.getTamaño().height += INCREMENTO_ALTO;

            switch (mensaje.getTipo()) {
                case Máximo:
                    setMaximumSize(mensaje.getTamaño());
                    break;
                case Mínimo:
                    setMinimumSize(mensaje.getTamaño());
                    break;
                case Predilecto:
                    setPreferredSize(mensaje.getTamaño());
                    break;
            }
        }
        return Reacción.No_Propagar;
    }

    @Override
    public void setSize(int ancho, int alto) {

        //Modificamos la localización
        if(System.getProperty("os.name").startsWith("Windows")) {

            int x = Integer.max(getX() - (ancho - getSize().width) / 2, 0);
            int y = Integer.max(getY() - (alto - getSize().height) / 2, 0);

            setLocation(x, y);
        }

        super.setSize(ancho, alto);
    }

    @Override
    public Comunicador getComunicador() {
        return comunicador;
    }
}
