package dc.gui;

import dc.Configurador;
import dc.ConfiguradorExtensible;
import dc.ConfiguradorInt;
import dc.FactoríaConfiguración;
import dc.comunicador.Reacción;
import dc.gui.distribuciones.Alineación;
import dc.gui.paneles.simples.PanelSimple;

import javax.swing.*;
import java.awt.*;

/**
 * Panel con un cuadrado opaco cuyo color de fondo puede modificarse mediante mensajes
 * enviados con el sistema de paso de mensajes del sistema: {@link dc.comunicador.Comunicador}.
 *
 * <p>Tiene además una etiqueta que muestra su nombre. Extiende a {@link PanelSimple} y, por
 * tanto, posee {@link dc.gui.distribuciones.DistribuciónMatricial}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
class PanelColor extends PanelSimple {

    public final static int LADO_CUADRADO = 25;

    private JPanel cuadrado;

    private ColorConfigurable muestra;

    /**
     * Constructor. Crea, dimensiona y ubica el cuadrado. También posiciona la etiqueta
     * con el nombre.
     */
    public PanelColor() {
        muestra = new ColorConfigurable();

        cuadrado = new JPanel();
        Dimension dCuadrado = new Dimension(LADO_CUADRADO, LADO_CUADRADO);

        cuadrado.setMinimumSize(dCuadrado);
        cuadrado.setMaximumSize(dCuadrado);
        cuadrado.setPreferredSize(dCuadrado);

        agregar(componenteNombre,0, Alineación.Izquierda);
        agregar(cuadrado,0,Alineación.Derecha);
    }

    //Modificadores del color del cuadrado

    public void setR(int r) {
        muestra.setR(r);
        cuadrado.setBackground(muestra.getColor());
    }
    public void setG(int g) {
        muestra.setG(g);
        cuadrado.setBackground(muestra.getColor());
    }
    public void setB(int b) {
        muestra.setB(b);
        cuadrado.setBackground(muestra.getColor());
    }

    @Override
    public String toString() {
        return "Panel Muestra Color";
    }

}

/**
 * Crea una instancia {@link Configurador} para la configuración de {@link ColorConfigurable}.
 *
 * <p>El {@link Configurador} será un {@link ConfiguradorExtensible} vertical compuesto por
 * tres {@link Configurador} (asociados a {@link dc.gui.paneles.simples.Desplazador}), uno
 * por cada tonalidad.</p>
 *
 * <p>El {@link ConfiguradorExtensible} vertical no poseerá título. En su lugar, se encuentra
 * un {@link PanelColor}. Los configuradores de las tonalidades interactuarán mediante acciones
 * con {@link PanelColor} para que mute el color del cuadrado cuando el <i>Usuario</i> interactúe
 * con un desplazador.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorColor {
    private static final int MARGEN = 5;

    /**
     * Para detalles sobre el {@link Configurador}, vea {@link ConfiguradorColor}.
     *
     * @param color {@link ColorConfigurable} a configurar.
     * @return {@link Configurador} para {@link ColorConfigurable}.
     */
    public static Configurador configurar(ColorConfigurable color) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        ConfiguradorExtensible vertical = factoría.extensibleVertical(MARGEN);
        vertical.setNombre(color.getNombre());

        //Inicializamos PanelColor, su nombre y le aplicamos
        // el aspecto de los paneles simples.
        PanelColor panelColor = new PanelColor();
        if (color.getNombre() == null) color.setNombre("Color");
        panelColor.setName(color.getNombre());

        factoría.getAspectoPanelesSimples().aplicar(panelColor);

        vertical.agregar(panelColor);

        //Agregamos los desplazadores y los comunicamos.
        Configurador rojo = factoría.desplazador("Rojo " + color.getNombre(), new ConfiguradorInt(color.getR(),0,255) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                color.setR(getValor());
            }
        });
        rojo.agregarAcción(valor -> panelColor.setR(((Number)valor).intValue()));
        rojo.comunicar();
        vertical.agregar(rojo);

        Configurador verde = factoría.desplazador("Verde " + color.getNombre(), new ConfiguradorInt(color.getG(),0,255) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                color.setG(getValor());
            }
        });

        verde.agregarAcción(valor -> panelColor.setG(((Number)valor).intValue()));
        verde.comunicar();
        vertical.agregar(verde);

        Configurador azul = factoría.desplazador("Azul", new ConfiguradorInt(color.getB(),0,255) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                color.setB(getValor());
            }
        });

        azul.agregarAcción(valor -> panelColor.setB(((Number)valor).intValue()));
        azul.comunicar();
        vertical.agregar(azul);

        return vertical;
    }
}
