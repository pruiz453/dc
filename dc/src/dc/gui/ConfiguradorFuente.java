package dc.gui;

import dc.*;

import java.util.List;


/**
 * Configura instancias {@link FuenteConfigurable}.
 *
 * El método {@link ConfiguradorFuente#configurar(FuenteConfigurable)} devuelve una
 * instancia {@link Configurador} para configurar {@link FuenteConfigurable}.
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorFuente {

    private static final String textoMuestra = "Texto de Muestra";

    /**
     * Crea un {@link Configurador} para la configuración de {@link FuenteConfigurable},
     * incluyendo el color de letra.
     *
     * <p>El configurador será un {@link ConfiguradorExtensible} asociado a
     * {@link dc.gui.paneles.extensibles.PanelBarras} conteniendo un
     * {@link dc.gui.paneles.extensibles.PanelVertical}, que a su vez posee una
     * {@link dc.gui.paneles.simples.Lista} para elegir el nombre,  otra para el estilo,
     * un {@link dc.gui.paneles.simples.Desplazador} para el tamaño y el {@link Configurador}
     * del {@link ColorConfigurable}.</p>
     *
     * <p>Los cambios que se produzcan en el {@link Configurador} se reproducirán en la
     * muestra de {@link FuenteConfigurable} y se aplicarán a su muestrario.</p>
     *
     * @param fuente {@link FuenteConfigurable} a configurar
     * @return {@link Configurador}
     */
    public static Configurador configurar(FuenteConfigurable fuente) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        if(fuente.getMuestra() == null) fuente.setMuestra(new FuenteConfigurable());
        FuenteConfigurable muestra = fuente.getMuestra();

        ConfiguradorExtensible vertical = factoría.extensibleVertical();
        vertical.setNombre("Fuente");

        Configurador nombre = factoría.lista("Nombre",
                new ConfiguradorString(Utilidades.getÍndice(FuenteConfigurable.NOMBRES,fuente.getNombre()),FuenteConfigurable.NOMBRES) {
                    @Override
                    public String validar() {
                        return null;
                    }

                    @Override
                    public void salvar() {
                        fuente.setNombre(getValor());
                    }
                });
        vertical.agregar(nombre);

        nombre.agregarAcción(valor -> {
            muestra.setNombre((String) valor);
            if(fuente.getMuestrario() != null) muestra.aplicarFuente(fuente.getMuestrario());
        });
        nombre.comunicar();


        String[] estilos = new String[] {FuenteConfigurable.NORMAL,FuenteConfigurable.CURSIVA,FuenteConfigurable.NEGRITA};

        Configurador cEstilo = factoría.lista("Estilo", new ConfiguradorString(
                Utilidades.getÍndice(estilos,fuente.getEstilo()), estilos
        ) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                fuente.setEstilo(getValor());
            }
        });
        vertical.agregar(cEstilo);

        cEstilo.agregarAcción(valor -> {
            muestra.setEstilo((String) valor);
            if(fuente.getMuestrario() != null) muestra.aplicarFuente(fuente.getMuestrario());
        });
        cEstilo.comunicar();


        Configurador tamaño = factoría.desplazador("Tamaño", new ConfiguradorInt(fuente.getTamaño(),1,45) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                fuente.setTamaño(getValor());
            }
        });
        vertical.agregar(tamaño);

        tamaño.agregarAcción(valor -> {
            muestra.setTamaño(((Number) valor).intValue());
            if(fuente.getMuestrario() != null) muestra.aplicarFuente(fuente.getMuestrario());
        });
        tamaño.comunicar();

        Configurador color = ConfiguradorColor.configurar(fuente.getColorConfigurable());

        vertical.agregar(color);

        color.agregarAcción(valor -> {
            muestra.setColor(ColorConfigurable.convertir((List<Object>) valor));
            if(fuente.getMuestrario() != null) muestra.aplicarFuente(fuente.getMuestrario());
        });
        color.comunicar();

        factoría.agregarBarras(vertical);

        return vertical;

    }
}
