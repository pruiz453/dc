package dc.gui;

import dc.Clonable;
import dc.gui.paneles.PanelImagenFondo;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Permite la configuración del fondo de un elemento gráfico.
 *
 * <p>El fondo puede ser un color sólido o un degradado, usando {@link DegradadoRadial}.
 * La generación del degradado se realizará únicamente al ser modificados
 * {@link FondoConfigurable#colorExterno} o {@link FondoConfigurable#colorInterno}. </p>
 *
 * <p>El fondo se aplicará exclusivamente sobre {@link PanelImagenFondo}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class FondoConfigurable extends Clonable {
    static String COLOR_SÓLIDO = "Color Sólido";
    static String DEGRADADO = "Degradado";

    private String estilo;

    private ColorConfigurable colorExterno;
    private ColorConfigurable colorInterno;

    private ColorConfigurable colorSólido;

    private BufferedImage imagen;

    /**
     * Elemento gráfico donde probar la muestra.
     */
    private PanelImagenFondo muestrario;

    /**
     * Muestra donde aplicar una configuración de forma que la clase original
     * no sea modificada.
     */
    private FondoConfigurable muestra;

    /**
     * Constructor con color sóligo blanco.
     */
    public FondoConfigurable() {
        estilo = COLOR_SÓLIDO;
        colorExterno = new ColorConfigurable(255,255,255);
        colorInterno = new ColorConfigurable(100,100,100);
        colorSólido = new ColorConfigurable(255,255,255);
    }

    /**
     * Constructor con fondo degradado.
     *
     * @param colorExterno {@link ColorConfigurable} externo del degradado.
     * @param colorInterno {@link ColorConfigurable} interno del degradado.
     */
    public FondoConfigurable(ColorConfigurable colorExterno, ColorConfigurable colorInterno) {
        estilo = DEGRADADO;
        this.colorExterno = colorExterno;
        this.colorInterno = colorInterno;
        colorSólido = new ColorConfigurable(255,255,255);
    }

    /**
     * Constructor con un color sólido introducido.
     *
     * @param colorSólido {@link ColorConfigurable}
     */
    public FondoConfigurable(ColorConfigurable colorSólido) {
        estilo = COLOR_SÓLIDO;
        this.colorSólido = colorSólido;
        colorExterno = new ColorConfigurable(255,255,255);
        colorInterno = new ColorConfigurable(255,255,255);
    }

    /**
     * Aplica el fondo a un {@link PanelImagenFondo}.
     * @param panel {@link PanelImagenFondo} cuyo fondo se modificará.
     */
    public void aplicarFondo(PanelImagenFondo panel) {
        if(estilo.equals(COLOR_SÓLIDO)) {
            panel.setOpaque(true);
            panel.setBackground(colorSólido.getColor());
        }else {
            panel.setOpaque(false);
            panel.setBackground(null);
            panel.setImagen(getImagen());
        }
    }

    /**
     * Genera la imagen de fondo.
     */
    private void regenerar() {
        imagen = new DegradadoRadial(colorExterno.getColor(),colorInterno.getColor());
    }

    /**
     * Imagen de fondo.
     * @return {@link DegradadoRadial}.
     */
    public BufferedImage getImagen() {
        if(imagen == null || colorExterno.modificado() || colorInterno.modificado())
            regenerar();
        return imagen;
    }


    @Override
    public String toString() {
        return "Estilo: " + estilo + "\n" + colorExterno + "\n" + colorInterno
                + "\n" + colorSólido;
    }

    /**
     * Escribe los atributos de la instancia en un archivo asociado al
     * {@link ObjectOutputStream} pasado como argumento.
     *
     * @param oos {@link ObjectOutputStream}
     * @throws IOException
     */
    public void escribirEnDisco(ObjectOutputStream oos) throws IOException {
        oos.writeObject(estilo);

        colorExterno.escribirEnDisco(oos);
        colorInterno.escribirEnDisco(oos);
        colorSólido.escribirEnDisco(oos);
    }

    /**
     * Lee los atributos de la instancia en un archivo asociado al
     * {@link ObjectInputStream} pasado como argumento.
     *
     * @param ois {@link ObjectInputStream}
     * @throws IOException
     */
    public void leerDeDisco(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        estilo = (String) ois.readObject();

        colorExterno.leerDeDisco(ois);
        colorInterno.leerDeDisco(ois);
        colorSólido.leerDeDisco(ois);

        regenerar();
    }

    /// Selectores y Mutadores ///

    public String getEstilo() {
        return estilo;
    }

    public ColorConfigurable getColorExterno() {
        return colorExterno;
    }

    public ColorConfigurable getColorInterno() {
        return colorInterno;
    }

    public ColorConfigurable getColorSólido() {
        return colorSólido;
    }

    public void setEstilo(String estilo) {
        if(! estilo.equals(COLOR_SÓLIDO) && !estilo.equals(DEGRADADO))
            throw new RuntimeException("Estilo inválido en setEstilo de FondoConfigurable");

        this.estilo = estilo;
    }

    public void setColorExterno(ColorConfigurable colorExterno) {
        this.colorExterno = colorExterno;
        regenerar();
    }

    public void setColorInterno(ColorConfigurable colorInterno) {
        this.colorInterno = colorInterno;
        regenerar();
    }

    public void setColorSólido(ColorConfigurable colorSólido) {
        this.colorSólido = colorSólido;
    }

    public PanelImagenFondo getMuestrario() {
        return muestrario;
    }

    public void setMuestrario(PanelImagenFondo muestrario) {
        this.muestrario = muestrario;
    }

    public FondoConfigurable getMuestra() {
        return muestra;
    }

    public void setMuestra(FondoConfigurable muestra) {
        this.muestra = muestra;
    }
}
