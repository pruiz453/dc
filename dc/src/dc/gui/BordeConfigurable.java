package dc.gui;

import dc.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Permite la configuración de un borde ({@link Border}) de un componente gráfico.
 *
 * <p>Puede establecerse el tipo, el subtipo y los {@link ColorConfigurable}
 * principal y secundario.
 * Además posee dos subbordes configurables ({@link BordeConfigurable#bordeExterno} y
 * {@link BordeConfigurable#bordeInterno}) que serán empleados en caso de que
 * el tipo sea {@link BordeConfigurable#COMPUESTO}. Estos subbordes no pueden ser
 * bordes compuestos.</p>
 *
 * <p>Se produce una reutilización de atributos. Así, {@link BordeConfigurable#colorPrincipal}
 * se utiliza para el color principal de un {@link BevelBorder} como para el único
 * color de un {@link javax.swing.border.LineBorder}</p>
 *
 * <p>La siguiente tabla muestra los posibles bordes a configurar y
 * la aplicación de los atributos sobre ellos:</p>
 *
 * <IMG SRC="../../Imágenes/tablaBordes.png"/>
 *
 * <p>El estar activo, atributo {@link BordeConfigurable#coloresDefecto} provoca que
 * se ignoren {@link BordeConfigurable#colorPrincipal} y {@link BordeConfigurable#colorSecundario},
 * y que {@link java} utilice colores en función del color de fondo.</p>
 *
 * @see {@link BorderFactory}
 *
 * @author Pablo Ruiz Sánchez
 */
public class BordeConfigurable extends Clonable {

    public static final String BISELADO = "Biselado";
    public static final String COMPUESTO = "Compuesto";
    public static final String GRABADO = "Grabado";
    public static final String LINEAL = "Lineal";
    public static final String SIN_BORDE = "Sin Borde";

    public static final String PROFUNDIDAD = "Profundidad";
    public static final String RESALTE = "Resalte";

    private String tipo;
    private String subtipo;

    private ColorConfigurable colorPrincipal;
    private ColorConfigurable colorSecundario;
    private boolean coloresDefecto = true;

    private int grosor;

    private BordeConfigurable bordeExterno;
    private BordeConfigurable bordeInterno;
    private boolean subBorde = false;

    private BordeConfigurable muestra;
    private JComponent muestrario;

    private String nombre;

    /**
     * Constructor por defecto: {@link BordeConfigurable#LINEAL}, negro y de grosor 1.
     */
    public BordeConfigurable() {
        inicializar(LINEAL,PROFUNDIDAD,new ColorConfigurable(),new ColorConfigurable(),1,
                subBordeDefecto(),subBordeDefecto());
    }

    /**
     * Constructor que permite la introducción de tipo y subtipo.
     *
     * @param tipo Véase {@link BordeConfigurable} para las opciones.
     * @param subtipo Véase {@link BordeConfigurable} para las opciones.
     */
    public BordeConfigurable(String tipo, String subtipo) {
        inicializar(tipo,subtipo,new ColorConfigurable(),new ColorConfigurable(),1,
                subBordeDefecto(),subBordeDefecto());
    }

    /**
     * Constructo inicializado con un {@link javax.swing.border.LineBorder}.
     *
     * @param colorPrincipal Color del {@link javax.swing.border.LineBorder}.
     * @param grosor Grosor del {@link javax.swing.border.LineBorder}.
     */
    public BordeConfigurable(ColorConfigurable colorPrincipal, int grosor) {
        inicializar(LINEAL,PROFUNDIDAD,colorPrincipal,new ColorConfigurable(),grosor,
                subBordeDefecto(),subBordeDefecto());
    }

    /**
     * Constructor que permite la inicialización de un borde no compuesto
     * @param tipo Véase {@link BordeConfigurable} para las opciones.
     * @param subtipo Véase {@link BordeConfigurable} para las opciones.
     * @param colorPrincipal {@link ColorConfigurable} principal del borde.
     * @param colorSecundario {@link ColorConfigurable} secundario del borde.
     */
    public BordeConfigurable(String tipo, String subtipo, ColorConfigurable colorPrincipal, ColorConfigurable colorSecundario) {

        inicializar(tipo,subtipo,colorPrincipal,colorSecundario,1,null,null);
    }

    /**
     * Constructor genérico. Permite la introducción de todos los argumentos.
     *
     * @param tipo Véase {@link BordeConfigurable} para las opciones.
     * @param subtipo Véase {@link BordeConfigurable} para las opciones.
     * @param colorPrincipal {@link ColorConfigurable} principal del borde.
     * @param colorSecundario {@link ColorConfigurable} secundario del borde.
     * @param grosor Grosor de {@link javax.swing.border.LineBorder}.
     * @param bordeExterno Borde externo de {@link javax.swing.border.CompoundBorder}.
     *                      Puede ser <i>null</i>.
     * @param bordeInterno Borde interno de {@link javax.swing.border.CompoundBorder}.
     *                      Puede ser <i>null</i>.
     */
    public BordeConfigurable(String tipo, String subtipo, ColorConfigurable colorPrincipal, ColorConfigurable colorSecundario,
                             int grosor, BordeConfigurable bordeExterno, BordeConfigurable bordeInterno) {
        inicializar(tipo,subtipo,colorPrincipal,colorSecundario,grosor,bordeExterno,bordeInterno);
        bordeExterno.setBordeInterno(null);
        bordeInterno.setBordeInterno(null);
        bordeExterno.setBordeExterno(null);
        bordeInterno.setBordeExterno(null);
    }


    /**
     * Código común de los constructores.
     */
    private void inicializar(String tipo, String subtipo, ColorConfigurable colorPrincipal, ColorConfigurable colorSecundario,
                             int grosor, BordeConfigurable bordeExterno, BordeConfigurable bordeInterno) {
        setTipo(tipo);
        setSubtipo(subtipo);
        this.colorPrincipal = colorPrincipal;
        this.colorSecundario = colorSecundario;
        this.grosor = grosor;
        this.bordeExterno = bordeExterno;
        this.bordeInterno = bordeInterno;

        if(bordeExterno != null)
            bordeExterno.setSubBorde(true);

        if(bordeInterno != null)
            bordeInterno.setSubBorde(true);
    }

    /**
     * Crea un suborde (cuyos subordes serán <i>null.</i>
     *
     * @return {@link BordeConfigurable}
     */
    private BordeConfigurable subBordeDefecto() {
        return new BordeConfigurable(LINEAL,PROFUNDIDAD, new ColorConfigurable(), new ColorConfigurable());
    }

    /**
     * Aplica el borde configurado a un {@link JComponent}.
     *
     *
     * @param componente {@link JComponent} al que se aplicará el borde.
     */
    public void aplicarBorde(JComponent componente) {
        componente.setBorder(getBorder());
    }

    /**
     * Construye el borde configurado.
     *
     * @return {@link Border}. Véase {@link BordeConfigurable} para las opciones.
     */
    public Border getBorder() {
        if(tipo.equals(BISELADO)) {

            if(subtipo.equals(PROFUNDIDAD)) {
                if(coloresDefecto) {
                    return BorderFactory.createBevelBorder(BevelBorder.LOWERED);
                }else {
                    return BorderFactory.createBevelBorder(BevelBorder.LOWERED,colorPrincipal.getColor(),colorSecundario.getColor());
                }

            }else {
                if(coloresDefecto) {
                    return BorderFactory.createBevelBorder(BevelBorder.RAISED);
                }else {
                    return BorderFactory.createBevelBorder(BevelBorder.RAISED,colorPrincipal.getColor(),colorSecundario.getColor());
                }

            }

        }else if(tipo.equals(GRABADO)) {

            if(subtipo.equals(PROFUNDIDAD)) {
                if(coloresDefecto) {
                    return BorderFactory.createEtchedBorder(BevelBorder.LOWERED);
                }else {
                    return BorderFactory.createEtchedBorder(BevelBorder.LOWERED,colorPrincipal.getColor(),colorSecundario.getColor());
                }

            }else {
                if(coloresDefecto) {
                    return BorderFactory.createEtchedBorder(BevelBorder.RAISED);
                }else {
                    return BorderFactory.createEtchedBorder(BevelBorder.RAISED,colorPrincipal.getColor(),colorSecundario.getColor());
                }

            }

        }else if(tipo.equals(LINEAL)) {

            return BorderFactory.createLineBorder(colorPrincipal.getColor(),grosor);

        }else if(tipo.equals(COMPUESTO)) {

            return BorderFactory.createCompoundBorder(bordeExterno.getBorder(), bordeInterno.getBorder());

        }else {
            return BorderFactory.createEmptyBorder();
        }
    }

    /**
     * Comprueba si un {@link String} es un tipo válido de Borde.
     * @param tipo {@link String} a comprobar.
     * @return {@link Boolean#TRUE} si es un tipo válido.
     */
    private boolean esTipo(String tipo) {
        return tipo.equals(BISELADO) ||
                tipo.equals(COMPUESTO) ||
                tipo.equals(GRABADO) ||
                tipo.equals(LINEAL) ||
                tipo.equals(SIN_BORDE);
    }

    /**
     * Comprueba si un {@link String} es un subtipo válido de Borde.
     * @param subtipo {@link String} a comprobar.
     * @return {@link Boolean#TRUE} si es un subtipo válido.
     */
    private boolean esSubtipo(String subtipo) {
        return subtipo.equals(PROFUNDIDAD) ||
                subtipo.equals(RESALTE);
    }

    @Override
    public String toString() {
        return tipo + " " + subtipo + "\n" + colorPrincipal + "\n" + colorSecundario;
    }

    /**
     * Escribe los atributos de la instancia en un archivo asociado al
     * {@link ObjectOutputStream} pasado como argumento.
     *
     * @param oos {@link ObjectOutputStream}
     * @throws IOException
     */
    public void escribirEnDisco(ObjectOutputStream oos) throws IOException {
        oos.writeObject(tipo);
        oos.writeObject(subtipo);
        oos.write(grosor);

        colorPrincipal.escribirEnDisco(oos);
        colorSecundario.escribirEnDisco(oos);

        oos.writeBoolean(coloresDefecto);

        if(tipo.equals(COMPUESTO)) {
            bordeExterno.escribirEnDisco(oos);
            bordeInterno.escribirEnDisco(oos);
        }
    }

    /**
     * Lee los atributos de la instancia en un archivo asociado al
     * {@link ObjectInputStream} pasado como argumento.
     *
     * @param ois {@link ObjectInputStream}
     * @throws IOException
     */
    public void leerDeDisco(ObjectInputStream ois) throws IOException, ClassNotFoundException {

        tipo = (String) ois.readObject();
        subtipo = (String) ois.readObject();
        grosor = ois.read();

        colorPrincipal.leerDeDisco(ois);
        colorSecundario.leerDeDisco(ois);

        coloresDefecto = ois.readBoolean();

        if(tipo.equals(COMPUESTO)) {
            bordeExterno.leerDeDisco(ois);
            bordeInterno.leerDeDisco(ois);
        }
    }


    /// Selectores y Mutadores ///


    public boolean esSubBorde() {
        return subBorde;
    }

    public void setSubBorde(boolean subBorde) {
        this.subBorde = subBorde;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        if(! esTipo(tipo))
            throw new RuntimeException("Tipo inválido en setTipo de BordeConfigurable");
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        if(! esSubtipo(subtipo))
            throw new RuntimeException("Subtipo inválido en setSubtipo de BordeConfigurable");

        this.subtipo = subtipo;
    }

    public boolean isColoresDefecto() {
        return coloresDefecto;
    }

    public void setColoresDefecto(boolean coloresDefecto) {
        this.coloresDefecto = coloresDefecto;
        if(bordeExterno != null) bordeExterno.setColoresDefecto(coloresDefecto);
        if(bordeInterno != null) bordeInterno.setColoresDefecto(coloresDefecto);
    }

    public BordeConfigurable getMuestra() {
        return muestra;
    }

    public void setMuestra(BordeConfigurable muestra) {
        this.muestra = muestra;
    }

    public JComponent getMuestrario() {
        return muestrario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ColorConfigurable getColorPrincipal() {
        return colorPrincipal;
    }

    public void setColorPrincipal(ColorConfigurable colorPrincipal) {
        this.colorPrincipal = colorPrincipal;
    }

    public ColorConfigurable getColorSecundario() {
        return colorSecundario;
    }

    public void setColorSecundario(ColorConfigurable colorSecundario) {
        this.colorSecundario = colorSecundario;
    }

    public int getGrosor() {
        return grosor;
    }

    public void setGrosor(int grosor) {
        this.grosor = grosor;
    }

    public BordeConfigurable getBordeExterno() {
        return bordeExterno;
    }

    public void setBordeExterno(BordeConfigurable bordeExterno) {
        this.bordeExterno = bordeExterno;
    }

    public BordeConfigurable getBordeInterno() {
        return bordeInterno;
    }

    public void setBordeInterno(BordeConfigurable bordeInterno) {
        this.bordeInterno = bordeInterno;
    }

    public void setMuestrario(JComponent muestrario) {
        this.muestrario = muestrario;
    }
}
