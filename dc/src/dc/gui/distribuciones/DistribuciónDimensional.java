package dc.gui.distribuciones;

import java.awt.*;

/**
 * <p>Clase abstracta que distribuye los componentes horizontal
 * o verticalmente. Reúne los aspectos comunes de ambos casos.</p>
 *
 * <IMG SRC="../../../Imágenes/distribuciónDimensional.png"/>
 *
 * <p>Se entenderá como <i>dimensión</i> aquella componente de la <i>dimensión</i>
 * (ancho en caso horizontal, alto en vertical) a distribuir los
 * componentes gráficos.</p>
 *
 * <p>La otra componente de la <i>dimensión</i> será igual a la del contenedor (salvo margen).</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class DistribuciónDimensional extends Distribución {

    /**
     * Por defecto, no se considerará margen ni vertical ni horizontal.
     */
    public static final int MARGEN_DEFECTO = 0;

    private int margenHorizontal;
    private int margenVertical;

    /**
     * Constructor. Inicializa los márgenes.
     */
    public DistribuciónDimensional(Container contenedor) {
        super(contenedor);
        margenHorizontal = MARGEN_DEFECTO;
        margenVertical = MARGEN_DEFECTO;
    }


    ////// Métodos abstractos dependientes de si la disposición
    ////// es vertical u horizontal.

    /**
     * Selector coordenada principal
     * @param punto {@link Point} del que extraer la coordenada.
     * @return {@link Point#x} o {@link Point#y}
     */
    protected abstract int getCoordenada(Point punto);

    /**
     * Mutador coordenada principal
     * @param punto {@link Point} al que modificar la coordenada.
     * @param valor {@link Integer}
     */
    protected abstract void setCoordenada(Point punto, int valor);

    /**
     * Selector dimensión principal (la que es repartida entre componentes).
     * @param d {@link Dimension} del que extraer la dimensión.
     * @return {@link Dimension#width} o {@link Dimension#height}
     */
    protected abstract int getDimensión(Dimension d);

    /**
     * Mutador dimensión principal (la que es repartida entre componentes).
     * @param d {@link Dimension} al que modificar la dimensión.
     * @param valor {@link Integer} al que se establecerá
     *      {@link Dimension#width} o {@link Dimension#height}.
     */
    protected abstract void setDimensión(Dimension d, int valor);

    /**
     * Selector dimensión secundaria (la que es máxima en todos los componentes).
     * @param d {@link Dimension} del que extraer la dimensión.
     * @return {@link Dimension#width} o {@link Dimension#height}
     */
    protected abstract int getOtraDimensión(Dimension d);

    /**
     * Mutador dimensión secundaria (la que es máxima en todos los componentes).
     * @param d {@link Dimension} al que modificar la dimensión.
     * @param valor {@link Integer} al que se establecerá
     *      {@link Dimension#width} o {@link Dimension#height}.
     */
    protected abstract void setOtraDimensión(Dimension d, int valor);

    /**
     * Margen en la dimensión secundaria (la que es máxima en todos los componentes).
     * @return {@link Integer}.
     */
    protected abstract int getMargenSecundario();


    ////// Métodos públicos //////

    /**
     * Posiciona los componentes gráficos y reajusta los tamaños mínimo, máximo
     * y predilecto debido a que pueden haber cambiado.
     */
    @Override
    public void posicionarComponentes() {
        Dimension tamaño = contenedor.getSize();
        int dimenxiónMínima;
        int dimensiónMáxima;
        int dimensión;
        int dPunto;

        ajustarTamaños();

        Point punto = new Point(margenHorizontal,margenVertical);
        tamaño.width -= 2*margenHorizontal;
        tamaño.height -= 2*margenVertical;

        //Calculamos la parte de dimensión que le corresponde a cada
        //componente. Si ésta supera el máximo de un componente dado,
        //el componente obtendrá dicho máximo en su lugar.
        int dimensiónSobrante = dimensiónSobrante();

        for(Component componente : contenedor.getComponents()) {
            dimenxiónMínima = getDimensión(componente.getMinimumSize());
            dimensiónMáxima = getDimensión(componente.getMaximumSize());

            //Dimensión correspondiente al componente
            dimensión = Integer.min(suma(dimenxiónMínima, dimensiónSobrante), dimensiónMáxima);

            //Ajustamos el tamaño
            setDimensión(tamaño ,dimensión);

            //Equivalente a setBounds
            componente.setLocation(punto);
            componente.setSize(tamaño);

            //Calculamos siguiente punto.
            setCoordenada(punto, getCoordenada(punto) + dimensión);
        }
    }


    /**
     * <p>Calcula la cantidad de <i>dimensión</i> a agregar a la mínima para hallar el tamaño
     * de cada componete.</p>
     *
     * <p>A medida que un contenedor se expande, llamamos <i>dimensión sobrante</i> a la diferencia
     * entre su tamaño con respecto a su tamaño mínimo. Esa diferencia será repartida entre
     * los distintos componentes, respetando su dimensión máxima.</p>
     *
     * <p>Para cada componente, calcula el valor de dimensión que le corresponde y, si es mayor
     * que su dimensión máxima, el componente debe tener tamaño máximo y el exceso repartirse
     * entre los demás (que puedan seguir expandiéndose).</p>
     *
     * <p>Si un componente alcanza su máximo, quitamos a la dimensión sobrante la parte de dimensión
     * que le corresponde, establecemos el booleano de su índice en componentesAjustados a true, restamos
     * uno a componentesNoAjustados y volvemos a iniciar el reparto.</p>
     *
     * <p>La dimensión correspondiente a cada componente será:</p>
     *
     * <p>     dimensiónSobranteTotal/componentesNoAjustados + dimensiónMínima</p>
     *
     *
     * @param dimensiónSobranteTotal Diferencia entre el valor de la dimensión actual del contenedor y su mínimo.
     * @param componentesAjustados Array de {@link Boolean} que indican si en el reparto de dimensión sobrante los
     *                            componentes han alcanzado su máximo.
     * @param componentesNoAjustados Número de componentes que no han alcanzado su máximo y se repartirán la
     *                               dimensión sobrante.
     *
     * @return Dimensión correspondiente a cada componente o {@link Integer#MAX_VALUE} si todos han alcanzado su máximo.
     */
    private int dimensiónSobranteRecursivo(int dimensiónSobranteTotal,boolean componentesAjustados[], int componentesNoAjustados) {
        int dimensiónMínima;
        int dimensiónMáxima;
        int dimensión;
        
        if(componentesNoAjustados == 0) {
            return  Integer.MAX_VALUE;
        }

        int dimensiónSobrante = dimensiónSobranteTotal/componentesNoAjustados;

        for(int i = 0; i < contenedor.getComponentCount(); i++) {
            dimensiónMínima = getDimensión(contenedor.getComponent(i).getMinimumSize());
            dimensiónMáxima = getDimensión(contenedor.getComponent(i).getMaximumSize());
            dimensión = dimensiónMínima + dimensiónSobrante;

            if(!componentesAjustados[i] && dimensión > dimensiónMáxima) {
                componentesAjustados[i] = true;
                dimensiónSobranteTotal -= dimensiónMáxima - dimensiónMínima;
                componentesNoAjustados--;
                return dimensiónSobranteRecursivo(dimensiónSobranteTotal,componentesAjustados,componentesNoAjustados);
            }
        }

        return dimensiónSobrante;

    }

    /**
     * <p>Calcula la cantidad de <i>dimensión</i> a agregar a la mínima para hallar el tamaño
     * de cada componete.</p>
     *
     * <p>1.- Halla la diferencia entre la dimensión actual y la mínima.</p>
     * <p>2.- Llama a dimensiónSobranteRecursivo</p>
     *
     * @return Dimensión correspondiente a cada componente o {@link Integer#MAX_VALUE} si todos han alcanzado su máximo.
     */
    private int dimensiónSobrante() {
        int dimensiónSobranteTotal = getDimensión(contenedor.getSize()) - getDimensión(contenedor.getMinimumSize());

        return dimensiónSobranteRecursivo(dimensiónSobranteTotal,new boolean[contenedor.getComponentCount()], contenedor.getComponentCount());
    }


    /**
     *
     *  Suma de números positivos sin desbordamiento.
     *
     * @param a {@link Integer}
     * @param b {@link Integer}
     * @return a + b
     */
    private int suma(int a, int b) {
        return Integer.max(b,Integer.max(a+b , a));
    }

    /**
     * Estable tamaños mínimo y predilecto.
     */
    public void ajustarTamaños() {
        Dimension tamañoMínimo = new Dimension();
        Dimension tamañoComponente;

        tamañoMínimo.width = getMargenHorizontal()*2;
        tamañoMínimo.height = getMargenVertical()*2;
        int otraDimensión;

        for(Component componente : contenedor.getComponents()) {
            tamañoComponente = componente.getMinimumSize();

            //La dimensión principal se suma
            setDimensión(tamañoMínimo, suma(getDimensión(tamañoMínimo), getDimensión(tamañoComponente)) );

            //La dimensión secundaria es el máximo de las dimensiones secundarias (más el margen secundario)
            otraDimensión = suma(getOtraDimensión(tamañoComponente),getMargenSecundario()*2);
            setOtraDimensión(tamañoMínimo, Integer.max(getOtraDimensión(tamañoMínimo) , otraDimensión));
        }

        contenedor.setMinimumSize(tamañoMínimo);
        contenedor.setPreferredSize(tamañoMínimo);
    }

    /// Selectores y Mutadores ///

    public int getMargenHorizontal() {
        return margenHorizontal;
    }

    public void setMargenHorizontal(int margenHorizontal) {
        this.margenHorizontal = margenHorizontal;
    }

    public int getMargenVertical() {
        return margenVertical;
    }

    public void setMargenVertical(int margenVertical) {
        this.margenVertical = margenVertical;
    }
}
