package dc.gui.distribuciones;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


/**
 * <p>Clase abstracta para la distribución espacial de componetes de un objeto {@link Container}.</p>
 *
 * <p>Se trata de un {@link LayoutManager}, pero en lugar de implementar dicha interfaz,
 * agrega un {@link ComponentAdapter}  que sobrescribe {@link ComponentAdapter#componentResized(ComponentEvent)}.
 * Esta solución funciona mejor en ciertos entornos, como el de desarollo del proyecto:
 * <a href="https://i3wm.org">i3wm</a>.</p>
 *
 * <p>Al contrario que un {@link LayoutManager}, está vinculado al contenedor y lo visualiza
 * como atributo.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class Distribución {

    /**
     * Contenedor a distribuir.
     */
    protected Container contenedor;

    /**
     * Constructor
     *
     * @param contenedor {@link Container}
     */
    public Distribución(Container contenedor) {
        this.contenedor = contenedor;

        contenedor.setLayout(null);
        contenedor.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                posicionarComponentes();
            }
        });
    }

    /**
     * Método que distribuye los componentes del contenedor.
     */
    public abstract void posicionarComponentes();
}
