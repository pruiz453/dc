package dc.gui.distribuciones;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * <p>Distribución matricial de componentes de un contenedor gráfico.</p>
 *
 * <p>Cada componente será asociado a una {@link Fila} y una {@link Alineación}. El conjunto
 * de filas formarán una {@link Matriz} que es centrada verticalmente.</p>
 *
 * <p>La {@link Alineación} puede ser {@link Alineación#Izquierda}, {@link Alineación#Centrada},
 * {@link Alineación#Derecha} o {@link Alineación#Expansible}.</p>
 *
 * <p>Una {@link Fila} podrá contener un {@link Elemento} con cualquier {@link Alineación},
 * o dos {@link Elemento}, uno con {@link Alineación#Izquierda}
 * y otro con {@link Alineación#Derecha}. A pesar de que solo
 * puede tener dos elementos, por comodidad y con vistas a futuras expansiones
 * de la funcionalidad de la clase, {@link Fila} extiende a {@link LinkedList<Elemento>}. Obsérvese que
 * el cálculo de las dimensiones de una fila es de orden constante.</p>
 *
 * <p>Los componentes tendrán tamaño fijo e igual a su tamaño predilecto:
 * {@link Component#getPreferredSize()}.
 * La expansión aumentará el espacio horizontal entre éstos
 * y los márgenes verticales de la {@link Matriz}. Los contenedores serán expansibles
 * infinitamente horizontalmente y la altura máxima vendrá dada por la suma del atributo
 * {@link DistribuciónMatricial#gananciaAltura} a la altura mínima.</p>
 *
 * <p>Otro atributo: {@link DistribuciónMatricial#margen} determinará el margen horizontal, los márgenes mínimos
 * verticales, el espacio horizontal mínimo entre componentes y el espacio vertical
 * fijo entre filas.</p>
 *
 * <p>En el algoritmo de posicionamiento, se calcula previamente el tamaño de la {@link Matriz}.
 * Ésto es debido a que los componentes podrían haber cambiado de tamaño entre una
 * disposición y otra. Podría haberse establecido un sistema de paso de mensajes
 * análogo al de PanelEmisor, sin embargo, la clase fue pensada para matrices de
 * poco tamaño (dos filas a lo sumo), luego el coste de cálculo es mínimo y de esta
 * forma aumentamos la reutilización de la clase sin vernos obligados a admitir
 * únicamente componentes que envíen mensajes cuando cambia su tamaño preferente.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class DistribuciónMatricial extends Distribución {

    /**
     * <p>Asocia un componente con su {@link Alineación}.</p>
     *
     * <p>Los objectos {@link Elemento} serán guardados en objetos {@link Fila}.</p>
     *
     * <p>Se considerará que dos elementos son iguales si poseen igual
     * componente. Quedan sobrescritos los métodos {@link Elemento#equals(Object)} y
     * {@link Elemento#hashCode()} de esta forma.</p>
     *
     * @author Pablo Ruiz Sánchez
     */
    class Elemento {
        private Component componente;
        private Alineación alineación;


        /**
         *
         * @param componente {@link Component}
         * @param alineación {@link Alineación}
         */
        public Elemento(Component componente, Alineación alineación) {
            this.componente = componente;
            this.alineación = alineación;
        }

        @Override
        public boolean equals(Object obj) {

            if(obj instanceof Elemento) {
                return componente.equals(((Elemento)obj).componente);
            }
            if(obj instanceof Component) {
                return componente.equals(obj);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return componente.hashCode();
        }

        public Component getComponente() {
            return componente;
        }

        public Alineación getAlineación() {
            return alineación;
        }

    }

    /**
     * <p>Clase que recoge los {@link Elemento} de una fila de la {@link Matriz}.</p>
     *
     * <p>Podrá contener un {@link Elemento} con cualquier {@link Alineación},
     * o dos {@link Elemento}, uno con {@link Alineación#Izquierda}
     * y otro con {@link Alineación#Derecha}. A pesar de que solo
     * puede tener dos elementos, por comodidad y con vistas a futuras expansiones
     * de la funcionalidad de la clase, {@link Fila} extiende a {@link LinkedList<Elemento>}. Obsérvese que
     * el cálculo de las dimensiones de una fila es de orden constante.</p>
     *
     * <p>Vinculada a una instancia de {@link DistribuciónMatricial}.</p>
     *
     * @author Pablo Ruiz Sánchez
     */
    class Fila extends LinkedList<Elemento> {

        /**
         * <p>Incluye la comprobación de condiciones asociada a una {@link Fila}
         * al método add de LinkedList.</p>
         *
         * <p>Obsérvese que en futuras ampliaciones estas condiciones podrían
         * variar. Por ejemplo, podría considerarse aceptar un elemento
         * central y uno a la izquierda, o tres elementos: izquierda, central
         * y derecha...</p>
         *
         * @param elemento {@link Elemento} a agregar.
         *
         * @return Véase {@link LinkedList#add(Object)}
         */
        @Override
        public boolean add(Elemento elemento) {
            if((elemento.getAlineación() == Alineación.Centrada ||
                    elemento.getAlineación() == Alineación.Expansible) &&
                    size() > 0) {
                throw new RuntimeException("Una fila con un elemento centrado o expandible solo puede contener un elemento.");
            }

            if(elemento.getAlineación() == Alineación.Izquierda  && size() > 0 &&
                    get(0).getAlineación() != Alineación.Derecha) {
                throw new RuntimeException("Una fila con un elemento alineado a la izquierda solo puede combinarse con otro elemento alineado a la derecha.");
            }

            if(elemento.getAlineación() == Alineación.Derecha  && size() > 0 &&
                    get(0).getAlineación() != Alineación.Izquierda) {
                throw new RuntimeException("Una fila con un elemento alineado a la derecha solo puede combinarse con otro elemento alineado a la izquierda.");
            }

            return super.add(elemento);
        }

        /**
         * <p>Tamaño de una fila.</p>
         *
         * <p>Obsérvese que, de cambiarse las condiciones este método
         * también debería ser modificado.</p>
         *
         * @return
         */
        public Dimension tamaño() {
            int ancho = 0;
            int alto = 0;

            for(Elemento elemento : this) {
                Dimension dElemento = elemento.getComponente().getPreferredSize();
                if(dElemento.height > alto) alto = dElemento.height;
                ancho += dElemento.width;
            }

            ancho += getMargen() * (size() - 1);

            return new Dimension(ancho,alto);
        }
    }

    /**
     * <p>Lista de objetos {@link Fila}.</p>
     *
     * <p>En lugar del método {@link LinkedList#add(Object)}, debe usarse
     * {@link Matriz#agregar(Elemento, int)},
     * más adecuado para el uso de matrices.</p>
     */
    class Matriz extends LinkedList<Fila> {

        /**
         * <p>Agrega un {@link Elemento} a una {@link Fila}.</p>
         *
         * <p>Si el índice excede el tamaño de la matriz,
         * agrega las nuevas {@link Fila} necesarias para evitar
         * {@link NullPointerException}.</p>
         *
         * @param elemento {@link Elemento} a agregar.
         * @param índiceFila
         */
        public void agregar(Elemento elemento, int índiceFila) {
            for(int i = size(); i <= índiceFila; i++ ){
                add(new Fila());
            }

            Fila fila = get(índiceFila);
            fila.add(elemento);
        }

        /**
         * Tamaño de la matriz. Este método es independiente
         * de las condiciones que imponga {@link Fila}.
         *
         */
        public Dimension tamaño() {
            int ancho = 0;
            int alto = 0;

            for(Fila fila : this) {
                Dimension dFila = fila.tamaño();

                if(dFila.width > ancho) ancho = dFila.width;
                alto += dFila.height;
            }

            alto += getMargen() * (size() - 1);

            return new Dimension(ancho,alto);
        }
    }


    public static final int MARGEN_DEFECTO = 10;
    public static final int GANANCIA_ALTURA_DEFECTO = 50;

    /**
     * Margen horizontal, margen mínimo vertical, espacio mínimo horizontal
     * entre componentes y espacio fijo vertical entre los mismos.
     */
    private int margen;

    /**
     * Sumado a la altura mínima, se obtiene la altura máxima.
     */
    private int gananciaAltura;

    /**
     * Contiene los componentes con sus respectivas {@link Alineación} y ordenados en filas.
     */
    private Matriz matriz;

    /**
     * Constructor que inicia los atributos por defecto.
     * @param contenedor {@link Container} a distribuir.
     */
    public DistribuciónMatricial(Container contenedor) {
        super(contenedor);
        matriz = new Matriz();

        margen = MARGEN_DEFECTO;
        gananciaAltura = GANANCIA_ALTURA_DEFECTO;

    }

    /**
     * <p>Agrega un {@link Component} a una {@link Fila} con una {@link Alineación} pasados
     * por argumento. Tras ello, ajusta los tamaños mínimo, máximo y predilecto.</p>
     *
     * <p>Para que el componente aparezca en pantalla, se ha de ejecutar adicionalmente
     * el método {@link Container#add(Component)}. Se distribuyen componentes que pertenezcan
     * al contenedor y es responsabilidad de éste administrarlos.</p>
     *
     * @param componente {@link Component} a distribuir (posicionar)
     * @param índiceFila índice de la {@link Fila} donde se situará.
     * @param alineación {@link Alineación} del componente.
     */
    public void agregar(Component componente, int índiceFila, Alineación alineación) {
        matriz.agregar(new Elemento(componente,alineación),índiceFila);

        ajustarTamaños(matriz.tamaño());
    }

    /**
     * <p>Elimina un componente de una fila. El componente ya no será posicionado
     * en el contenedor.</p>
     *
     * <p>Es necesario la introducción del índice de la fila en
     * el que se halla. En la versión actual no ha sido necesario
     * implementar una versión en la que se buscara el componente
     * a eliminar en las filas. </p>
     *
     * @param componente {@link Component} a eliminar.
     * @param índiceFila índice de la {@link Fila} del componente.
     */
    public void eliminar(Component componente, int índiceFila) {
        Fila fila =  matriz.get(índiceFila);

        for(int i = 0; i <  fila.size(); i++) {
            if( fila.get(i).getComponente().equals(componente)) {
                fila.remove(i);
            }
        }

        ajustarTamaños(matriz.tamaño());
    }


    /**
     * <p>En primer lugar se calcula el tamaño de la matriz, para centrarla verticalmente.
     * Luego, por cada {@link Fila}, se vuelve a recalcular su tamaño (con un gasto computacional
     * mínimo, al poseer a lo sumo dos elementos) y se calcula la altura que le corresponde
     * a la fila. Los {@link Elemento} de una {@link Fila} se centrarán en ella verticalmente según
     * su altura. Finalmente, se situan los elementos según su alineación. </p>
     *
     * <p>La comprobación de inconsistencias se delega al método {@link Fila#add(Elemento)}.</p>
     *
     * <p>Además, se reajustan los tamaños mínimo, máximo y predilecto.</p>
     *
     */
    @Override
    public void posicionarComponentes() {
        Dimension tamaño = contenedor.getSize();
        Dimension tamañoMatriz = matriz.tamaño();

        //Altura de la primera fila (coincide con el inicio de la matriz)
        int alturaFila = (tamaño.height - tamañoMatriz.height)/2;

        for (Fila fila : matriz) {
            Dimension tamañoFila = fila.tamaño();

            for(Elemento elemento : fila) {
                Component componente = elemento.getComponente();
                Dimension tamañoComponente = componente.getPreferredSize();

                //A esta altura se posicionará el componente de la fila
                //Si el componente es el de mayor altura de la misma, y = alturaFila
                //Sino, será centrado verticalmente.
                int y = alturaFila + (tamañoFila.height - tamañoComponente.height)/2;

                switch (elemento.getAlineación()){
                    case Izquierda:
                        componente.setBounds(margen,y,tamañoComponente.width,tamañoComponente.height);
                        break;
                    case Derecha:
                        componente.setBounds(tamaño.width - tamañoComponente.width - margen,y,tamañoComponente.width,tamañoComponente.height);
                        break;
                    case Centrada:
                        componente.setBounds((tamaño.width - tamañoComponente.width)/2,y,tamañoComponente.width,tamañoComponente.height);
                        break;
                    case Expansible:
                        componente.setBounds(margen,y,tamaño.width - margen *2,tamañoComponente.height);
                        break;
                }
            }

            //Sumamos la altura de la fila para obtener la alutra de la siguiente.
            alturaFila += tamañoFila.height + margen;
        }
        
        ajustarTamaños(tamañoMatriz);
    }

    /**
     * <p>Ajuste de los tamaños mínimo, máximo y predilecto.</p>
     *
     * <p>El tamaño mínimo se obtendrá sumando dos veces el margen en cada dimensión
     * al tamaño de la matriz. El predilecto coincidirá con el tamaño mínimo.
     * La altura máxima vendrá dada por la suma de gananciaAltura con la altura
     * mínima y no habrá límite máximo horizontal.</p>
     *
     * @param tamañoMatriz {@link Dimension} de {@link DistribuciónMatricial#matriz}
     */
    private void ajustarTamaños(Dimension tamañoMatriz) {
        tamañoMatriz.width += margen *2;
        tamañoMatriz.height += margen *2;

        contenedor.setMinimumSize(tamañoMatriz);

        contenedor.setPreferredSize(tamañoMatriz);

        contenedor.setMaximumSize(new Dimension(Integer.MAX_VALUE,
                tamañoMatriz.height + gananciaAltura));

    }


    /// Selectores y Mutadores ///

    public int getMargen() {
        return margen;
    }

    public void setMargen(int margen) {
        this.margen = margen;
    }

    public int getGananciaAltura() {
        return gananciaAltura;
    }

    public void setGananciaAltura(int gananciaAltura) {
        this.gananciaAltura = gananciaAltura;
    }
}