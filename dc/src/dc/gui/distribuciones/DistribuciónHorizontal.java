package dc.gui.distribuciones;
import java.awt.*;

/**
 * <p>Clase para la distribución de componentes Horizontal.</p>
 *
 * <p>{@link DistribuciónDimensional#getDimensión(Dimension)} y
 * {@link DistribuciónDimensional#setDimensión(Dimension, int)} interactuarán con el ancho
 * y {@link DistribuciónDimensional#getCoordenada(Point)} y
 * {@link DistribuciónDimensional#setCoordenada(Point, int)} con la coordenada x.</p>
 *
 * @see dc.gui.distribuciones.DistribuciónDimensional
 *
 * @author Pablo Ruiz Sánchez
 */
public class DistribuciónHorizontal extends DistribuciónDimensional {

    /**
     * Constructor
     *
     * @param contenedor {@link Container}
     */
    public DistribuciónHorizontal(Container contenedor) {
        super(contenedor);
    }

    @Override
    protected int getCoordenada(Point punto) {
        return punto.x;
    }

    @Override
    protected void setCoordenada(Point punto, int valor) {
        punto.x = valor;
    }

    @Override
    protected int getDimensión(Dimension d) {
        return d.width;
    }

    @Override
    protected void setDimensión(Dimension d, int valor) {
        d.width = valor;
    }

    @Override
    protected int getOtraDimensión(Dimension d) {
        return d.height;
    }

    @Override
    protected void setOtraDimensión(Dimension d, int valor) {
        d.height = valor;
    }

    @Override
    protected int getMargenSecundario() {
        return getMargenVertical();
    }
}