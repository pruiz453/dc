package dc.gui.distribuciones;


/**
 * <p>Enumeración que indica la alineación de un componente en ciertas distribuciones
 * como {@link DistribuciónMatricial}.</p>
 *
 * <p>Posee métodos estáticos para extraer una Alineación de un {@link String} y obtener
 * un array de {@link String} con todas las alineaciones.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public enum Alineación {
    Izquierda,
    Centrada,
    Derecha,
    Expansible;

    /**
     * Convierte un {@link String} en una {@link Alineación}
     *
     * @param cadena {@link String} a convertir
     * @return Cadena convertida en {@link Alineación}.
     */
    public static Alineación convertir(String cadena) {
        if(cadena.equals(Izquierda.toString())) return Izquierda;
        if(cadena.equals(Centrada.toString())) return Centrada;
        if(cadena.equals(Derecha.toString())) return Derecha;
        if(cadena.equals(Expansible.toString())) return Expansible;

        throw new RuntimeException("Cadena inválida en convertir de Alineación: " + cadena);
    }

    /**
     * Array con los nombres de las posibles Alineaciones.
     *
     * @return Array de {@link String}
     */
    public static String[] getNombres() {
        return new String[] {Izquierda.toString(),Centrada.toString(),Derecha.toString(),Expansible.toString()};
    }
}