package dc.gui.distribuciones;

import java.awt.*;

/**
 * <p>Clase para la distribución de componentes Vertical.</p>
 *
 * <p>{@link DistribuciónDimensional#getDimensión(Dimension)} y
 * {@link DistribuciónDimensional#setDimensión(Dimension, int)} interactuarán con el alto
 * y {@link DistribuciónDimensional#getCoordenada(Point)} y
 * {@link DistribuciónDimensional#setCoordenada(Point, int)} con la coordenada y.</p>
 *
 * @see dc.gui.distribuciones.DistribuciónDimensional
 *
 * @author Pablo Ruiz Sánchez
 */
public class DistribuciónVertical extends DistribuciónDimensional {

    /**
     * Constructor
     *
     * @param contenedor {@link Container}
     */
    public DistribuciónVertical(Container contenedor) {
        super(contenedor);
    }

    @Override
    protected int getCoordenada(Point punto) {
        return punto.y;
    }

    @Override
    protected void setCoordenada(Point punto, int valor) {
        punto.y = valor;
    }

    @Override
    protected int getDimensión(Dimension d) {
        return d.height;
    }

    @Override
    protected void setDimensión(Dimension d, int valor) {
        d.height = valor;
    }

    @Override
    protected int getOtraDimensión(Dimension d) {
        return d.width;
    }

    @Override
    protected void setOtraDimensión(Dimension d, int valor) {
        d.width = valor;
    }

    @Override
    protected int getMargenSecundario() {
        return getMargenHorizontal();
    }
}
