package dc.gui;

import dc.Configurador;
import dc.ConfiguradorExtensible;
import dc.FactoríaConfiguración;
import dc.gui.paneles.PanelImagenFondo;
import dc.gui.paneles.simples.PanelTítulo;

/**
 * Configura instancias {@link AspectoConfigurable}.
 *
 * El método {@link ConfiguradorAspecto#configurar(AspectoConfigurable)} devuelve una
 * instancia {@link Configurador} para configurar {@link AspectoConfigurable}.
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorAspecto {
    private static final String textoMuestrario = "Aspecto";


    /**
     * Crea un {@link Configurador} para la configuración de {@link AspectoConfigurable}.
     *
     * <p>El configurador será un {@link ConfiguradorExtensible} asociado a
     * {@link dc.gui.paneles.extensibles.PanelVertical}, que poseerá un título
     * que será a su vez un <i>muestrario</i> (elemento donde se aplicarán las muestras).
     * Tras ello, posee un {@link dc.gui.paneles.extensibles.PanelPestañas} con los
     * elementos gráficos de los objetos {@link Configurador} de los componentes de
     * {@link AspectoConfigurable}.</p>
     *
     * <p>Los cambios que se produzcan en el {@link Configurador} se reproducirán en la
     * muestra de {@link AspectoConfigurable} y se aplicarán a su muestrario creado
     * en este método.</p>
     *
     * @param aspecto {@link AspectoConfigurable}
     * @return {@link Configurador}
     */
    public static Configurador configurar(AspectoConfigurable aspecto) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        ConfiguradorExtensible vertical = factoría.extensibleVertical();
        vertical.setNombre("Aspecto");

        PanelTítulo muestrario = factoría.título(textoMuestrario);
        vertical.agregar(muestrario);

        ConfiguradorExtensible pestañas = factoría.extensiblePestañas();
        vertical.agregar(pestañas);

        if(aspecto.getBorde() != null){
            aspecto.getBorde().setMuestrario(muestrario);
            pestañas.agregar(ConfiguradorBorde.configurar(aspecto.getBorde()));
        }

        if(aspecto.getFondo() != null){
            aspecto.getFondo().setMuestrario(muestrario);
            pestañas.agregar(ConfiguradorFondo.configurar(aspecto.getFondo()));
        }

        if(aspecto.getFuente() != null){
            aspecto.getFuente().setMuestrario(muestrario);
            pestañas.agregar(ConfiguradorFuente.configurar(aspecto.getFuente()));
        }

        return vertical;
    }
}
