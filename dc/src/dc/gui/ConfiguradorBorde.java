package dc.gui;

import dc.*;

import java.util.List;

/**
 * Configura instancias {@link BordeConfigurable}.
 *
 * El método {@link ConfiguradorBorde#configurar(BordeConfigurable)} devuelve una
 * instancia {@link Configurador} para configurar {@link BordeConfigurable}.
 *
 * @author Pablo Ruiz Sánchez
 */
public class ConfiguradorBorde {

    /**
     * Crea un {@link Configurador} para la configuración de {@link BordeConfigurable}.
     *
     * <p>El configurador será un {@link ConfiguradorExtensible} asociado a un
     * {@link dc.gui.paneles.extensibles.PanelVertical} que poseerá un
     * {@link dc.gui.paneles.extensibles.PanelPestañas} y una
     * {@link dc.gui.paneles.simples.Lista}, que indicará si se han de aplicar
     * los colores por defecto o los introducidos por el usuario.
     *
     * <p>La selección de la pestaña
     * configurará el tipo de borde (véase {@link BordeConfigurable}). En cada pestaña
     * se encuentran los componentes para configurar el tipo concreto, como una
     * {@link dc.gui.paneles.simples.Lista} para el subtipo o los colores principal y
     * secundario. El {@link BordeConfigurable#LINEAL} tiene un
     * {@link dc.gui.paneles.simples.Desplazador}
     * que configura su grosor.</p>
     *
     * <p>El en el caso de {@link BordeConfigurable#COMPUESTO}, se agregan los {@link Configurador}
     * de sus subbordes, generados recursivamente, a un {@link dc.gui.paneles.extensibles.PanelPestañas}.
     * Si los subbordes pudieran ser compuestos, en este punto se generaría una
     * recursión no controlada y un {@link StackOverflowError}.</p>
     *
     * <p>Los cambios que se produzcan en el {@link Configurador} se reproducirán en la
     * muestra de {@link BordeConfigurable} y se aplicarán a su muestrario.</p>
     *
     * @param borde {@link BordeConfigurable} a configurador.
     * @return {@link Configurador}.
     */
    public static Configurador configurar(BordeConfigurable borde) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        //Coordinamos las muestras
        if (borde.getMuestra() == null) borde.setMuestra(new BordeConfigurable());

        if(borde.getBordeExterno() != null) {
            borde.getMuestra().setBordeExterno(new BordeConfigurable());
            borde.getBordeExterno().setMuestra(borde.getMuestra().getBordeExterno());
        }

        if(borde.getBordeInterno() != null) {
            borde.getMuestra().setBordeInterno(new BordeConfigurable());
            borde.getBordeInterno().setMuestra(borde.getMuestra().getBordeInterno());
        }


        //Nombres de los tipos
        String nombres[] = null;

        if(borde.getBordeInterno() == null || borde.getBordeExterno() == null) {
            nombres = new String[] {BordeConfigurable.BISELADO,BordeConfigurable.GRABADO,BordeConfigurable.LINEAL
                    ,BordeConfigurable.SIN_BORDE};
        }else {
            nombres = new String[] {BordeConfigurable.BISELADO,BordeConfigurable.COMPUESTO,BordeConfigurable.GRABADO,
                    BordeConfigurable.LINEAL, BordeConfigurable.SIN_BORDE};
        }

        //Nombre de los bordes
        if(borde.getNombre() == null) borde.setNombre("Borde");
        if(borde.getBordeExterno() != null) borde.getBordeExterno().setNombre("Externo");
        if(borde.getBordeInterno() != null) borde.getBordeInterno().setNombre("Interno");

        //Nombre de los colores
        borde.getColorPrincipal().setNombre("Color Principal");
        borde.getColorSecundario().setNombre("Color Secundario");


        ConfiguradorLista cTipo;
        ConfiguradorExtensible pestañas = factoría.extensiblePestañas(borde.getNombre(),cTipo = new ConfiguradorString
                (Utilidades.getÍndice(nombres,borde.getTipo().toString()),nombres) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                borde.setTipo(getValor());
            }
        });


        //Biselado
        ConfiguradorExtensible vBiselado = factoría.extensibleVertical();
        vBiselado.setNombre(BordeConfigurable.BISELADO);

        Configurador cSubtipo1 = configuradorSubtipo(borde);
        vBiselado.agregar(cSubtipo1);

        Configurador cPrincipal1 = configuradorColorPrincipal(borde);
        vBiselado.agregar(cPrincipal1);

        Configurador cSecundario1 = configuradorColorSecundario(borde);
        vBiselado.agregar(cSecundario1);

        factoría.agregarBarras(vBiselado);
        pestañas.agregar(vBiselado);

        //Compuesto
        if(borde.getBordeExterno() != null && borde.getBordeInterno() != null) {
            ConfiguradorExtensible vCompuesto = factoría.extensiblePestañas();
            vCompuesto.setNombre(BordeConfigurable.COMPUESTO);
            pestañas.agregar(vCompuesto);

            Configurador cBordeExterno = ConfiguradorBorde.configurar(borde.getBordeExterno());
            Configurador cBordeInterno = ConfiguradorBorde.configurar(borde.getBordeInterno());

            cBordeExterno.agregarAcción(valor -> {
                if (borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
            });
            cBordeInterno.agregarAcción(valor -> {
                if (borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
            });

            vCompuesto.agregar(cBordeExterno);
            vCompuesto.agregar(cBordeInterno);
        }

        //Grabado
        ConfiguradorExtensible vGrabado = factoría.extensibleVertical();
        vGrabado.setNombre(BordeConfigurable.GRABADO);

        Configurador cSubtipo2 = configuradorSubtipo(borde);
        vGrabado.agregar(cSubtipo2);

        Configurador cPrincipal2 = configuradorColorPrincipal(borde);
        vGrabado.agregar(cPrincipal2);

        Configurador cSecundario2 = configuradorColorSecundario(borde);
        vGrabado.agregar(cSecundario2);

        factoría.agregarBarras(vGrabado);
        pestañas.agregar(vGrabado);

        //Lineal
        ConfiguradorExtensible vLineal = factoría.extensibleVertical();
        vLineal.setNombre(BordeConfigurable.LINEAL);

        Configurador cGrosor = factoría.desplazador("Grosor", new ConfiguradorInt(borde.getGrosor(),0,10) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                borde.setGrosor(getValor());
            }
        });

        cGrosor.agregarAcción(valor -> {
            borde.getMuestra().setGrosor(((Number)valor).intValue());
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });
        vLineal.agregar(cGrosor);

        Configurador cPrincipal3 = configuradorColorPrincipal(borde);
        vLineal.agregar(cPrincipal3);

        factoría.agregarBarras(vLineal);
        pestañas.agregar(vLineal);

        //Colores Defecto

        Configurador cColoresDefecto = factoría.casilla("Colores por Defecto",
            new ConfiguradorBoolean(borde.isColoresDefecto()) {
                @Override
                public String validar() {
                    return null;
                }

                @Override
                public void salvar() {
                    borde.setColoresDefecto(getValor());
                }
            });


        // Conectamos

        cPrincipal1.subscribir(cPrincipal2);
        cPrincipal2.subscribir(cPrincipal3);
        cPrincipal3.subscribir(cPrincipal1);

        cSecundario1.subscribir(cSecundario2);
        cSecundario2.subscribir(cSecundario1);

        cSubtipo1.subscribir(cSubtipo2);
        cSubtipo2.subscribir(cSubtipo1);

        cColoresDefecto.agregarAcción(valor -> {
            borde.getMuestra().setColoresDefecto((Boolean) valor);
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });

        cTipo.agregarAcción(valor -> {
            borde.getMuestra().setTipo((String) valor);
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });
        cTipo.comunicar();

        //Creamos el panel vertical final que contendrá cColoresDefecto si no es subborde.
        ConfiguradorExtensible vertical = factoría.extensibleVertical();
        vertical.setNombre(pestañas.getNombre());
        vertical.agregar(pestañas);

        if(! borde.esSubBorde())
            vertical.agregar(cColoresDefecto);

        return vertical;
    }

    /**
     * Crea un {@link Configurador} para la configuración del subtipo de un
     * {@link BordeConfigurable}.
     *
     * @param borde {@link BordeConfigurable}
     * @return {@link Configurador}
     */
    private static Configurador configuradorSubtipo(BordeConfigurable borde) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        String [] nombres = new String[] {BordeConfigurable.PROFUNDIDAD,BordeConfigurable.RESALTE};

        Configurador subtipo = factoría.lista("Subtipo", new ConfiguradorString(Utilidades.getÍndice(nombres,borde.getSubtipo().toString()),nombres) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                borde.setSubtipo(getValor());
            }
        });

        subtipo.agregarAcción(valor -> {
            borde.getMuestra().setSubtipo((String) valor);
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });

        subtipo.comunicar();

        return subtipo;
    }

    /**
     * Obtiene y comunica un {@link Configurador} para la configuración del color principal
     * de un {@link BordeConfigurable}.
     *
     * @param borde {@link BordeConfigurable}
     * @return {@link Configurador}
     */
    private static Configurador configuradorColorPrincipal(BordeConfigurable borde) {
        Configurador cPrincipal = ConfiguradorColor.configurar( borde.getColorPrincipal());

        cPrincipal.agregarAcción(valor -> {
            borde.getMuestra().setColorPrincipal(ColorConfigurable.convertir((List<Object>) valor));
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });

        cPrincipal.comunicar();

        return cPrincipal;
    }

    /**
     * Obtiene y comunica un {@link Configurador} para la configuración del color secundario
     * de un {@link BordeConfigurable}.
     *
     * @param borde {@link BordeConfigurable}
     * @return {@link Configurador}
     */
    private static Configurador configuradorColorSecundario(BordeConfigurable borde) {
        Configurador cSecundario = ConfiguradorColor.configurar( borde.getColorSecundario());

        cSecundario.agregarAcción(valor -> {
            borde.getMuestra().setColorSecundario(ColorConfigurable.convertir((List<Object>) valor));
            if(borde.getMuestrario() != null) borde.getMuestra().aplicarBorde(borde.getMuestrario());
        });
        cSecundario.comunicar();

        return cSecundario;
    }
}
