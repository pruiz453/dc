package dc;

import dc.comunicador.Comunicativo;

import javax.swing.*;

/**
 * Interfaz que implementan las clases con capacidad para configurar un valor.
 *
 * <p>Para configurar un valor, es necesario crear y mantener una instancia del mismo
 * independiente del valor original, para que no se produzcan modificaciones no deseadas.
 * En este contexto, la clase posee métodos para validar {@link Configurador#validar()}
 * y, en caso que sea válido, salvar {@link Configurador#salvar()} dicho valor.</p>
 *
 * <p>Para que el valor de configuración sea modificable, la clase tiene asoaciado un
 * componente gráfico y en la interfaz constan sus métodos selectores y mutadores.</p>
 *
 * <p>Un {@link Configurador} poseerá además un nombre. Al ser inicializado o modificado,
 * dicho nombre será aplicado también al componente gráfico.</p>
 *
 * <p>Todo configurador posee la capacidad de comunicarse con otros configuradores o con
 * elementos gráficos. Por ello la interfaz hereda de {@link Comunicativo}.</p>
 *
 * <p>Finalmente, posee un método que permite obviar el argumento al comunicar
 * {@link Configurador#comunicar()} y un selector para el valor del configurador:
 * {@link Configurador#obtenerValor()}.</p>
 *
 * Para inicializar configuradores y enlazarlos con sus elementos gráficos, véase
 * {@link FactoríaConfiguración}.
 *
 * @author Pablo Ruiz Sánchez
 */
public interface Configurador extends Comunicativo {

    /**
     * Valida el valor de un {@link Configurador}.
     *
     * <br>
     * <p><b>Se considerará válido si:</b></p>
     * <p>El resultado es <i>null</i> o una cadena vacía.</p>
     *
     * <br>
     * <p><b>Se considerará inválido si:</b></p>
     * <p>El resultado contiene texto con el motivo que lo invalida.</p>
     *
     * @return null, "" o {@link String} con el motivo de invalidez.
     */
    String validar();

    /**
     * Salva el valor de un {@link Configurador}.
     *
     * <p>Es llamado tras {@link Configurador#validar()} si la validación es exitosa.</p>
     */
    void salvar();

    /**
     * Selector componente gráfico de un {@link Configurador}.
     * @return {@link JComponent}.
     */
    JComponent getComponenteGráfico();

    /**
     * Mutador componente gráfico de un {@link Configurador}.
     * @param componenteGráfico {@link JComponent}.
     */
    void setComponenteGráfico(JComponent componenteGráfico);

    /**
     * Selector nombre del configurador.
     * @return {@link String}.
     *
     */
    String getNombre();

    /**
     * Mutador nombre del configurador.
     *
     */
    void setNombre(String nombre);

    /**
     * Permite obviar el parámetro de {@link Comunicativo#comunicar(Object)}.
     */
    void comunicar();

    /**
     * Obtiene el valor del {@link Configurador} polimórficamente encapsulado
     * en un {@link Object}.
     * @return Valor del {@link Configurador}.
     */
    Object obtenerValor();
}
