package dc;

/**
 * Configurador compatible con una casilla de verificación:
 * {@link dc.gui.paneles.simples.Casilla}.
 *
 * <p>Aunque en la versión presentada solo la implemente
 * {@link ConfiguradorBoolean}, la interfaz ha sido creada por analogía
 * a los demás casos y ante posibbles ampliaciones.</p>
 *
 * <p>Esta interfaz es usada por la factoría para comunicar
 * el {@link Configurador} con su componente gráfico.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface ConfiguradorCasilla extends Configurador {

	/**
	 * Dado el estado de la casilla, devuelve el valor del configurador.
	 *
	 * <p>Por defecto devuelve el mismo argumento, pero puede ser sobrescrito.</p>
	 *
	 * @param marcada Estado de la casilla.
	 * @return Valor del configurador.
	 */
	default Object convertirCasilla(boolean marcada) {
		return marcada;
	}

	/**
	 * Indica si la casilla debe o no estar marcada. Dependiente del valor del configurador.
	 *
	 * @return true si debe estar marcada.
	 */
	boolean getCasilla();
}
