package dc;

import dc.comunicador.Reacción;

/**
 * Configuración de valores booleanos.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorBoolean extends ConfiguradorSimple implements ConfiguradorLista, ConfiguradorCasilla {

	private static final String FALSO = "No";
	private static final String VERDADERO = "Sí";

	private static final String[] OPCIONES = new String[] {FALSO,VERDADERO};

	/**
	 * Valor actual del configurador.
	 */
	private boolean valor;


	//////////// Construcción ////////////

	/**
	 * Constructor inicializado en falso.
	 */
	public ConfiguradorBoolean() {
		inicializar(false);
	}

	/**
	 * Constructor que permite ajustar el valor inicial.
	 *
	 * @param valorInicial Valor inicial del configurador.
	 */
	public ConfiguradorBoolean(boolean valorInicial) {
		inicializar(valorInicial);
	}

	/**
	 * Código común a ambos constructores.
	 *
	 * @param valorInicial
	 */
	private void inicializar(boolean valorInicial) {
		this.valor = valorInicial;
	}


	//////////// Métodos Sobrescritos ////////////


	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(mensaje instanceof Boolean) {
			Boolean valorNuevo = (Boolean) mensaje;
			Boolean valorAnterior = this.valor;

			this.valor = valorNuevo;

			if(! valorAnterior.equals(valorNuevo)) {
				//Avisamos al contenedor padre
				actualizar();

				return Reacción.Propagar;
			}else {
				return Reacción.No_Propagar;
			}
		}else {
			throw new RuntimeException("Valor inválido en recibirValor de ConfiguradorBoolean: " + mensaje);
		}
	}

	@Override
	public Object convertirOpción(String opción) {
		return opción.equals(VERDADERO);
	}

	@Override
	public String getOpción() {
		if((Boolean) valor) {
			return VERDADERO;
		}else {
			return FALSO;
		}
	}

	@Override
	public String[] getOpciones() {
		return OPCIONES;
	}

	@Override
	public boolean getCasilla() {
		return valor;
	}


	/// Selectores y Mutadores ///

	/**
	 * Selector del valor del configurador.
	 *
	 * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
	 * y {@link Configurador#salvar()}.</p>
	 *
	 * @return El valor del configurador.
	 */
	public boolean getValor() {
		return valor;
	}
}
