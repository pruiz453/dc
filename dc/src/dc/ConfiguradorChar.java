package dc;


import dc.comunicador.Reacción;

import javax.swing.*;

/**
 * Configuración de valores char.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorChar extends ConfiguradorSimple implements ConfiguradorTextual, ConfiguradorLista {

	/**
	 * Valor del configurador.
	 */
	private char valor;

	private char[] opciones;

	private int índice;


	/**
	 * Constructor por defecto inicializado en 'a'.
	 *
	 */
	public ConfiguradorChar(){
		inicializar('a', new char[0]);
	}

	/**
	 * Constructor que permite la introducción del valor inicial.
	 * @param valorInicial Valor inicial del configurador.
	 */
	public ConfiguradorChar(char valorInicial){
		inicializar(valorInicial, new char[0]);
	}

	/**
	 * Constructor que permite dar una lista de opciones. No se aceptarán carácteres
	 * fuera de dicha lista.
	 *
	 * @param opciones Vector de carácteres posibles.
	 */
	public ConfiguradorChar(char[] opciones) {
		inicializar(opciones[0],opciones);
		índice = 0;
	}

	/**
	 * Constructor que permite dar una lista de opciones y el índice inicial. No se aceptarán carácteres
	 * fuera de dicha lista.
	 *
	 * <p>El Configurador se inicializará con <s>opciones[índice]</s></p>
	 *
	 * <p>Si el índice está fuera de rango, se generará {@link RuntimeException}.</p>
	 *
	 * @param índice Índice del carácter inicial.
	 * @param opciones Vector de carácteres posibles.
	 */
	public ConfiguradorChar(int índice, char[] opciones) {
		if(índice >= opciones.length)
			throw new RuntimeException("Índice fuera de rango en constructor de ConfiguradorChar.");

		inicializar(opciones[índice],opciones);
		this.índice = índice;
	}

	/**
	 * Inicialización común a los constructores.
	 *
	 * @param valorInicial Valor inicial.
	 * @param opciones Vector de carácteres posibles.
	 */
	private void inicializar(char valorInicial, char[] opciones) {
		this.valor = valorInicial;
		this.opciones = opciones;
	}

	///// Métodos sobrescritos /////

	@Override
	public Object convertirTexto(String texto) {

		switch (texto.length()) {
			case 0:
				return ' ';
			case 1:
				return texto.charAt(0);
			default:
				throw new RuntimeException("Texto en convertirTexto de ConfiguradorChar de longitud incorrecta: " + texto);
		}
	}

	@Override
	public String getTexto() {
		return valor + "";
	}

	@Override
	public String getOpción() {
		return valor + "";
	}

	@Override
	public Object convertirOpción(String opción) {

		return convertirTexto(opción);
	}

	@Override
	public String[] getOpciones() {
		String[] resultado = new String[opciones.length];

		for(int i = 0; i < opciones.length; i++) {
			resultado[i] = opciones[i] + "";
		}

		return resultado;
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {
		if(!(mensaje instanceof Character)) throw new RuntimeException("Argumento inválido en recibirValor de ConfiguradorChar.");

		char valorNuevo = (Character) mensaje;

		//Comprobamos si pertenece a opciones
		if(opciones != null && opciones.length != 0) {
			boolean valorEncontrado = false;

			for(int i = 0; i < opciones.length; i++) {
				if(valorNuevo == opciones[i]) {
					valorEncontrado = true;
					break;
				}
			}

			if(!valorEncontrado) {
				JOptionPane.showMessageDialog(null,"El valor '" + valorNuevo + "' no es una opción válida.");
				//Comunicamos para mantener la consistencia
				comunicar();
				return Reacción.Bloquear;
			}
		}

		if(setValor((Character) mensaje)) {
			//Avisamos al contenedor padre
			actualizar();

			return Reacción.Propagar;
		}else {
			return Reacción.No_Propagar;
		}
	}

	/// Selectores y Mutadores ///

	/**
	 * Mutador privado.
	 *
	 * @param valorNuevo Nuevo valor del Configurador.
	 * @return true si ha habido un cambio efectivo.
	 */
	private boolean setValor(char valorNuevo) {
		//Mutamos
		if(valor != valorNuevo) {
			valor = valorNuevo;
			return true;
		}

		return false;
	}

	/**
	 * Selector del valor del configurador.
	 *
	 * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
	 * y {@link Configurador#salvar()}.</p>
	 *
	 * @return El valor del configurador.
	 */
	public char getValor() {
		return valor;
	}

}