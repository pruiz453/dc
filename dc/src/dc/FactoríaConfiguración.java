package dc;

import dc.gui.*;
import dc.gui.distribuciones.Alineación;
import dc.gui.distribuciones.DistribuciónMatricial;
import dc.gui.paneles.PanelBotones;
import dc.gui.paneles.PanelImagenFondo;
import dc.gui.paneles.PanelMatricial;
import dc.gui.paneles.extensibles.*;
import dc.gui.paneles.simples.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;

/**
 * Creación y configuración de instancias {@link Configurador} y
 * {@link ConfiguradorExtensible}, así como de sus componentes gráficos
 * asociados.
 *
 * <p>La conexión entre los configuradores y sus elementos gráficos se
 * realizará mediante {@link dc.comunicador.Comunicador}, usando el
 * modo <b>Local</b>. Utiliza los métodos de las distintas
 * interfaces ({@link ConfiguradorCasilla}, {@link ConfiguradorLista},
 * {@link ConfiguradorDesplazador} y {@link ConfiguradorTextual}) para realizar
 * las transformaciones ({@link dc.comunicador.Comunicador.Transformación})
 * necesarias.</p>
 *
 * <p>La compatibilidad entre un componente gráfico y un configurador viene dada
 * por la implementación de dichas interfaces.</p>
 *
 * <p><IMG SRC="../Imágenes/compatibilidad.png"/></p>
 *
 * Por otro lado, los {@link ConfiguradorExtensible} agrupan instancias {@link Configurador},.
 *
 * <p><IMG SRC="../Imágenes/extensibles.png"/></p>
 *
 * <p>Además de los métodos mostrados en el siguiente diagrama, la clase posee Selectores
 * y Mutadores para cada atributo, de forma que pueden ser modificados. Si se desea realizar
 * una configuración gráfica, véase {@link ConfiguradorFactoría}.</p>
 *
 * <IMG SRC="../Imágenes/diagramas/Factoría.jpg"/>
 *
 */
public class FactoríaConfiguración extends Clonable {

	/////////////////////////////////////////////////////////
	//
	//	Atributos Estáticos
	//
	/////////////////////////////////////////////////////////


	/**
	 * Singleton
	 */
	private static FactoríaConfiguración instancia;

	/**
	 * Singleton
	 * @return {@link FactoríaConfiguración}
	 */
	public static FactoríaConfiguración getInstancia() {
		if (instancia == null) return instancia = new FactoríaConfiguración();
		else return instancia;
	}

	/**
	 * Permite el intercambio de código entre clases, pasado como argumento
	 * o atributo.
	 *
	 * @author Pablo Ruiz Sánchez
	 */
	public interface Acción extends Serializable {
		void ejecutar();
	}


	/////////////////////////////////////////////////////////
	//
	//	Atributos de instancia
	//
	/////////////////////////////////////////////////////////


	/////// Configuración Marco ///////

	/// Aspecto Marco ///
	private final AspectoConfigurable aspectoMarco = new AspectoConfigurable(
			new BordeConfigurable(BordeConfigurable.COMPUESTO,
					BordeConfigurable.RESALTE,new ColorConfigurable(), new ColorConfigurable(),1,
					new BordeConfigurable(BordeConfigurable.BISELADO, BordeConfigurable.RESALTE),
					new BordeConfigurable(BordeConfigurable.BISELADO, BordeConfigurable.PROFUNDIDAD)),

			new FondoConfigurable(new ColorConfigurable(50,50,50),
					new ColorConfigurable(240,240,240)),

			null);

	private int espacioBotones = PanelBotones.ESPACIO_BOTONES_DEFECTO;
	private int margenMarco = PanelBotones.MARGEN_DEFECTO;


	/////// Configuración Paneles Simples ///////

	/// Aspecto Paneles Simples ///
	private final AspectoConfigurable aspectoPanelesSimples =
			new AspectoConfigurable(
					new BordeConfigurable(BordeConfigurable.BISELADO,
							BordeConfigurable.RESALTE),

					new FondoConfigurable(new ColorConfigurable(150,150,150),
							new ColorConfigurable(240,240,240)),

					new FuenteConfigurable(Font.SANS_SERIF,
							FuenteConfigurable.getNEGRITA(), 14, new ColorConfigurable()));

	/// Dimensiones ///

	private int margenPaneles = DistribuciónMatricial.MARGEN_DEFECTO;
	private int gananciaAltura = DistribuciónMatricial.GANANCIA_ALTURA_DEFECTO;

	/// Casilla ///
	private Casilla.Estilo estiloCasilla = Casilla.ESTILO_DEFECTO;

	/// Lista ///
	private int anchoMínimoLista = Lista.ANCHO_MÍNIMO_LISTA_DEFECTO;

	/// Recuadros ///
	private int altoTextoRecuadro = Recuadro.ALTO_TEXTO_DEFECTO;
	private Alineación alineaciónTítuloRecuadro = Recuadro.ALINEACIÓN_TÍTULO_DEFECTO;

	/// Renglón ///
	private int anchoNormalRenglón = Renglón.ANCHO_NORMAL_DEFECTO;
	private int anchoGrandeRenglón = Renglón.ANCHO_GRANDE_DEFECTO;



	/////// Configuración Títulos ///////
	private final AspectoConfigurable aspectoTítulos =
			new AspectoConfigurable(
					new BordeConfigurable(BordeConfigurable.BISELADO,
							BordeConfigurable.PROFUNDIDAD),

					new FondoConfigurable(new ColorConfigurable(150,150,150),
							new ColorConfigurable(240,240,240)),

					new FuenteConfigurable(Font.SANS_SERIF,
							FuenteConfigurable.getNEGRITA(), 14, new ColorConfigurable()));

	private int margenTítulos = PanelTítulo.MARGEN;



	/////// Configuración Extensibles ///////

	public final static Boolean EXTENSIBLES_OPACOS_DEFECTO = false;
	public final static Boolean VERTICAL_TITULADO_DEFECTO = true;
	private Boolean extensiblesOpacos = EXTENSIBLES_OPACOS_DEFECTO;
	private Boolean verticalTitulado = VERTICAL_TITULADO_DEFECTO;


	/// Árbol ///
	private float porcentajeMáximoAnchoÁrbol = PanelÁrbol.PORCENTAJE_MÁXIMO_ANCHO_ÁRBOL_DEFECTO;
	private float porcentajeApariciónÁrbol = PanelÁrbol.PORCENTAJE_APARICIÓN_ÁRBOL_DEFECTO;
	private int anchoMínimoÁrbol = PanelÁrbol.ANCHO_MÍNIMO_ÁRBOL_DEFECTO;

	/// Barras ///
	private float porcentajeAltoBarras = PanelBarras.PORCENTAJE_ALTO_PANTALLA_DEFECTO;


	/////////////////////////////////////////////////////////
	//
	//	Métodos Privados
	//
	/////////////////////////////////////////////////////////


	/**
	 * Asocia el componenta al configurador, establece el nombre de ambos y llama a
	 * {@link Configurador#comunicar()}, para inicializar el componente gráfico.
	 *
	 * <p>Además configura el aspecto del componente dependiendo de si es simple o
	 * extensible.</p>
	 *
	 * @param configurador
	 * @param componente
	 * @param nombre
	 */
	private void configuraciónComún(Configurador configurador, JComponent componente, String nombre) {

		configurador.setComponenteGráfico(componente);
		configurador.setNombre(nombre);
		configurador.comunicar();

		//Aspecto Paneles Simples
		if(componente instanceof PanelSimple) {
			getAspectoPanelesSimples().aplicar(componente);
			((PanelMatricial) componente).getDistribución().setMargen(getMargenPaneles());
			((PanelMatricial) componente).getDistribución().setGananciaAltura(getGananciaAltura());
		}

		//Aspecto extensibles
		if(configurador instanceof ConfiguradorExtensible)
			componente.setOpaque(getExtensiblesOpacos());

	}

	/**
	 * Configuración común a los hijos de {@link PanelTextual}:
	 * {@link Renglón} y {@link Recuadro}.
	 *
	 * <p>Si se configura valores char, agrega un {@link java.awt.event.KeyListener}
	 * adicional, que consigue procesar el texto antes de que aparezca el segundo
	 * carácter, logrando una mejor apariencia visual.</p>
	 *
	 * @param nombre
	 * @param panel
	 * @param configurador
	 */
	private void configuraciónPanelTextual(String nombre, PanelTextual panel, ConfiguradorTextual configurador) {
		panel.setProcesador(valor -> configurador.limpiarTexto(valor));

		panel.subscribir(configurador, valor -> configurador.convertirTexto((String) valor));
		configurador.subscribir(panel, valor -> configurador.getTexto());

		if(configurador instanceof ConfiguradorChar) {
			panel.getComponenteTextual().addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					panel.getComponenteTextual().setText("");
				}
			});
		}

		configuraciónComún(configurador,panel,nombre);
	}

	/////////////////////////////////////////////////////////
	//
	//	Métodos Públicos
	//
	/////////////////////////////////////////////////////////

	
	public JFrame marco(String título, Configurador configurador, Acción terminar, Acción cancelar) {
		MarcoConfigurador marco = new MarcoConfigurador(título, configurador) {
			@Override
			public void terminar() {
				if(terminar != null) terminar.ejecutar();
			}

			@Override
			public void cancelar() {
				if(cancelar != null) cancelar.ejecutar();
			}
		};

		getAspectoMarco().aplicar((PanelImagenFondo) marco.getContentPane());
		((PanelBotones)marco.getContentPane()).setMargen(getMargenMarco());
		((PanelBotones)marco.getContentPane()).setEspacioBotones(getEspacioBotones());
		
		return marco;
	}

	/**
	 * Crea un diálogo con un panel central de texto no editable, con posibles desplazadores, y
	 * un botón de aceptar centrado en la pantalla.
	 *
	 * @param título
	 * @param contenido
	 */
	public void mensaje(String título, String contenido, Acción acción){
		MarcoMensaje mensaje = new MarcoMensaje(título,contenido) {
			@Override
			protected void acción() {
				if(acción != null) acción.ejecutar();
			}
		};

		getAspectoPanelesSimples().aplicar((JComponent) mensaje.getContentPane());

		mensaje.setVisible(true);
	}

	/**
	 * Devuelve un {@link PanelTítulo} configurado (con aspecto y margen).
	 *
	 * @param título
	 * @return {@link PanelTítulo}
	 */
	public PanelTítulo título(String título) {
		PanelTítulo panel = new PanelTítulo(título);

		getAspectoTítulos().aplicar(panel);

		panel.setMargen(margenTítulos);

		return panel;
	}

	/**
	 * Escribe los atributos de la instancia en orden en un archivo.
	 *
	 * <p>Agrega la extensión ".dc" a la ruta.</p>
	 *
	 * @param ruta Ruta donde se creará el archivo.
	 * @throws IOException En caso de ruta inválida.
	 */
	public void escribirEnDisco(String ruta) throws IOException {
		FileOutputStream fos = new FileOutputStream(ruta + ".dc");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		aspectoMarco.escribirEnDisco(oos);
		oos.write(margenMarco);
		oos.write(espacioBotones);

		aspectoPanelesSimples.escribirEnDisco(oos);
		oos.write(margenPaneles);
		oos.write(gananciaAltura);
		oos.writeObject(estiloCasilla);
		oos.write(anchoMínimoLista);
		oos.write(altoTextoRecuadro);
		oos.writeObject(alineaciónTítuloRecuadro);
		oos.write(anchoNormalRenglón);
		oos.write(anchoGrandeRenglón);

		aspectoTítulos.escribirEnDisco(oos);
		oos.write(margenTítulos);

		oos.writeObject(extensiblesOpacos);
		oos.writeObject(verticalTitulado);

		oos.writeFloat(porcentajeApariciónÁrbol);
		oos.writeFloat(porcentajeMáximoAnchoÁrbol);
		oos.write(anchoMínimoÁrbol);

		oos.writeFloat(porcentajeAltoBarras);

		oos.close();
	}

	/**
	 * Lee los atributos de la clase de un archivo creado por
	 * {@link FactoríaConfiguración#escribirEnDisco(String)}.
	 *
	 * @param ruta
	 * @throws IOException En caso de ruta inválida.
	 * @throws ClassNotFoundException En caso de archivo inválido o corrupto.
	 */
	public void leerDeDisco(String ruta) throws IOException, ClassNotFoundException {
		FileInputStream fos = new FileInputStream(ruta);
		ObjectInputStream ois = new ObjectInputStream(fos);

		aspectoMarco.leerDeDisco(ois);
		margenMarco = ois.read();
		espacioBotones = ois.read();

		aspectoPanelesSimples.leerDeDisco(ois);
		margenPaneles = ois.read();
		gananciaAltura = ois.read();
		estiloCasilla = (Casilla.Estilo) ois.readObject();
		anchoMínimoLista = ois.read();
		altoTextoRecuadro = ois.read();
		alineaciónTítuloRecuadro = (Alineación) ois.readObject();
		anchoNormalRenglón = ois.read();
		anchoGrandeRenglón = ois.read();

		aspectoTítulos.leerDeDisco(ois);
		margenTítulos = ois.read();

		extensiblesOpacos = (Boolean) ois.readObject();
		verticalTitulado = (Boolean) ois.readObject();

		porcentajeApariciónÁrbol = ois.readFloat();
		porcentajeMáximoAnchoÁrbol = ois.readFloat();
		anchoMínimoÁrbol = ois.read();

		porcentajeAltoBarras = ois.readFloat();

		ois.close();
	}

	/**
	 * Inicia una ventana de selección de ruta {@link JFileChooser}
	 * para llamar posteriormente a
	 * {@link FactoríaConfiguración#escribirEnDisco(String)}.
	 *
	 * <p>De producirse una excepción informa llamando a
	 * {@link FactoríaConfiguración#mensaje(String, String, Acción)}</p>
	 *
	 * @param padre
	 */
	public void escribirEnDisco(JFrame padre) {
		JFileChooser chooser = new JFileChooser();

		int returnVal = chooser.showOpenDialog(padre);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			try{
				escribirEnDisco(chooser.getSelectedFile().getPath());
			}catch (IOException ex) {
				mensaje("Error","Ruta no válida.\n\n" +
						ex.getMessage(),null);
			}
		}
	}

	/**
	 * Inicia una ventana de selección de ruta {@link JFileChooser}
	 * para llamar posteriormente a
	 * {@link FactoríaConfiguración#leerDeDisco(String)}.
	 *
	 * <p>De producirse una excepción informa llamando a
	 * {@link FactoríaConfiguración#mensaje(String, String, Acción)}</p>
	 *
	 * @param padre
	 * @return true si se ha leído con éxito.
	 */
	public boolean leerDeDisco(JFrame padre) {
		JFileChooser chooser = new JFileChooser();

		int returnVal = chooser.showOpenDialog(padre);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			try{
				leerDeDisco(chooser.getSelectedFile().getPath());
			}catch (IOException ex) {
				mensaje("Error","Ruta no válida.\n\n" +
						ex.getMessage(),null);
			}catch (ClassNotFoundException ex) {
				mensaje("Error","Archivo incorrecto o corrupto.",null);
			}
			return true;
		}
		return false;
	}

	/////////////////////////////////////////////////////////
	//
	//	Configuradores Simples
	//
	/////////////////////////////////////////////////////////


	/**
	 * Entrada de texto de una sola línea para configurar valores numéricos, String o char.
	 *
	 * <p><IMG SRC="../Imágenes/renglón.png"/></p>
	 *
	 * <p>El ancho de la entrada de texto es configurable. En concreto posee dos tamaños
	 * configurables. El menor, para la configuración de byte, short, int y char;
	 * el mayor para long, float, double y {@link String}. Véase
	 * {@link FactoríaConfiguración#setAnchoNormalRenglón(int)} y
	 * {@link FactoríaConfiguración#setAnchoGrandeRenglón(int)}.</p>
	 *
	 * @param nombre Texto del panel y su nombre.
	 * @param configurador {@link ConfiguradorTextual}
	 * @return {@link Configurador}
	 */
	public Configurador renglón(String nombre, ConfiguradorTextual configurador) {
		Renglón renglón = new Renglón();

		if(configurador instanceof ConfiguradorFlotante ||
				configurador instanceof ConfiguradorString) {
			renglón.setAnchoTexto(Renglón.AnchoTexto.Grande);
		}

		configuraciónPanelTextual(nombre,renglón,configurador);

		renglón.setAnchoTextoNormal(getAnchoNormalRenglón());
		renglón.setAnchoTextoGrande(getAnchoGrandeRenglón());

		return configurador;
	}

	/**
	 * Entrada de texto de varias líneas para configurar valores numéricos, String o char.
	 *
	 * <p><IMG SRC="../Imágenes/recuadro.png"/></p>
	 *
	 * <p>La altura de la entrada de texto es configurable:
	 * {@link FactoríaConfiguración#setAltoTextoRecuadro(int)}</p>
	 *
	 * @param nombre Texto del panel y su nombre.
	 * @param configurador {@link ConfiguradorTextual}
	 * @return {@link Configurador}
	 */
	public Configurador recuadro(String nombre, ConfiguradorTextual configurador) {
		Recuadro recuadro = new Recuadro();

		configuraciónPanelTextual(nombre,recuadro,configurador);

		recuadro.setAltoTexto(getAltoTextoRecuadro());
		recuadro.setAlineaciónTítulo(getAlineaciónTítuloRecuadro());

		return configurador;
	}

	/**
	 * Desplazador para la configuración de valores numéricos.
	 *
	 * <p><IMG SRC="../Imágenes/desplazador.png"/></p>
	 *
	 * @param nombre Texto del panel y su nombre.
	 * @param configurador {@link ConfiguradorDesplazador}
	 * @return {@link Configurador}
	 */
	public Configurador desplazador(String nombre, ConfiguradorDesplazador configurador) {
		Desplazador desplazador = new Desplazador();

		desplazador.subscribir(configurador,
				valor -> configurador.convertirDesplazamiento((Integer) valor, desplazador.desplazamientoMáximo()));
		configurador.subscribir(desplazador, valor -> configurador.getDesplazamiento(valor,desplazador.desplazamientoMáximo()));

		configurador.agregarAcción(valor -> {
			desplazador.setTexto(configurador.getTextoDesplazador());
		});

		configuraciónComún(configurador,desplazador,nombre);

		return configurador;
	}


	/**
	 * Lista desplegable para la configuración de valores char, boolean y String.
	 *
	 * <p><IMG SRC="../Imágenes/lista.png"/></p>
	 *
	 * <p>El ancho mínimo de la lista es configurable: 
	 * {@link FactoríaConfiguración#setAnchoMínimoLista(int)}</p>
	 * 
	 * @param nombre Texto del panel y su nombre.
	 * @param configurador {@link ConfiguradorLista}
	 * @return {@link Configurador}
	 */
	public Configurador lista(String nombre, ConfiguradorLista configurador) {

		Lista lista = new Lista();

		lista.setOpciones(configurador.getOpciones());

		lista.subscribir(configurador, valor -> configurador.convertirOpción((String) valor));
		configurador.subscribir(lista, valor -> configurador.getOpción());

		configuraciónComún(configurador,lista,nombre);

		lista.setAnchoMínimoLista(getAnchoMínimoLista());

		return configurador;
	}

	/**
	 * Casilla de verificación para la configuración de valores boolean.
	 *
	 * <p>El estilo de la casilla ({@link Casilla.Estilo}) es configurable.</p>
	 *
	 * <p><IMG SRC="../Imágenes/casilla1.png"/></p>
	 *
	 * <p><IMG SRC="../Imágenes/casilla2.png"/></p>
	 *
	 * @param nombre Texto del panel y su nombre.
	 * @param configurador {@link ConfiguradorCasilla}
	 * @return {@link Configurador}
	 */
	public Configurador casilla(String nombre, ConfiguradorCasilla configurador) {

		Casilla casilla = new Casilla();

		casilla.subscribir(configurador, valor -> configurador.convertirCasilla((Boolean) valor));
		configurador.subscribir(casilla, valor -> configurador.getCasilla());

		configuraciónComún(configurador,casilla,nombre);

		casilla.setEstilo(getEstiloCasilla());

		return configurador;
	}


	/////////////////////////////////////////////////////////
	//
	//	Extensibles
	//
	/////////////////////////////////////////////////////////


	//Vertical

	/**
	 * {@link ConfiguradorExtensible} asociado a {@link PanelVertical},
	 * que dispone los elementos gráficos de las instancias
	 * {@link Configurador} que se le agregen de forma vertical, siendo el primero el
	 * superior, sin márgenes por defecto.
	 *
	 * <p>Su nombre aparecerá al ser añadido a un extensible con pestañas o de
	 * árbol. Puede
	 * modificarse con {@link Configurador#setNombre(String)}}.</p>
	 *
	 * <p>Es configurable su opaciedad y si el método
	 * {@link FactoríaConfiguración#extensibleVertical(String)} debe agregar o no
	 * títulos automáticamente.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleVertical() {

		ConfiguradorExtensible configurador = new ListaConfiguradores();

		configuraciónComún(configurador,new PanelVertical(),"");

		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensibleVertical()}.
	 *
	 * <p>Permite la introducción de un margen entre cada componente.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleVertical(int margen) {
		PanelVertical panelVertical = new PanelVertical();
		panelVertical.getDistribución().setMargenVertical(margen);

		ConfiguradorExtensible configurador = new ListaConfiguradores();

		configuraciónComún(configurador,panelVertical,"");


		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensibleVertical()}.
	 *
	 * <p>Si la opción {@link FactoríaConfiguración#verticalTitulado} está activada,
	 * se le agregará un {@link PanelTítulo} al extensible vertical cuyo título sea
	 * el nombre argumento.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleVertical(String nombre) {
		PanelVertical panelVertical = new PanelVertical();
		ConfiguradorExtensible configurador = new ListaConfiguradores();

		configuraciónComún(configurador,panelVertical,nombre);

		if(getVerticalTitulado()) {
			panelVertical.add(título(nombre));
		}

		return configurador;
	}

	//Horizontal

	/**
	 * {@link ConfiguradorExtensible} asociado a {@link PanelVertical},
	 * que dispone los elementos gráficos de las instancias
	 * {@link Configurador} que se le agregen de forma horizontal, estando el
	 * primero en el extremo izquierdo, sin márgenes por defecto.
	 *
	 * <p>Su nombre aparecerá al ser añadido a un extensible con pestañas o de
	 * árbol. Puede
	 * modificarse con {@link Configurador#setNombre(String)}}.</p>
	 *
	 * <p>Es configurable su opaciedad</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleHorizontal() {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		JPanel panelHorizontal = new PanelHorizontal();

		configuraciónComún(configurador,panelHorizontal,"");

		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensibleHorizontal()}.
	 *
	 * <p>Permite la introducción de un margen entre cada componente.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleHorizontal(int margen) {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		PanelHorizontal panelHorizontal = new PanelHorizontal();
		panelHorizontal.getDistribución().setMargenHorizontal(margen);

		configuraciónComún(configurador,panelHorizontal,"");

		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensibleHorizontal()}.
	 *
	 * <p>Permite establecer un nombre.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleHorizontal(String nombre) {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		JPanel panelHorizontal = new PanelHorizontal();

		configuraciónComún(configurador,panelHorizontal,nombre);

		return configurador;
	}

	//Pestañas

	/**
	 * {@link ConfiguradorExtensible} que dispone los elementos gráficos de las instancias
	 * {@link Configurador} que se le agregen en un espacio con pestañas:
	 * ({@link PanelPestañas}).
	 *
	 * <p>El nombre de cada pestaña será el del {@link Configurador} asociado (y el
	 * de su componente gráfico).</p>
	 *
	 * <p><IMG SRC="../Imágenes/pestañas.png"/></p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensiblePestañas() {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		PanelPestañas panelPestañas = new PanelPestañas();

		configuraciónComún(configurador,panelPestañas,"");

		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensiblePestañas()}.
	 *
	 * <p>Permite establecer un nombre.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensiblePestañas(String nombre) {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		PanelPestañas panelPestañas = new PanelPestañas();

		configuraciónComún(configurador,panelPestañas,nombre);

		return configurador;
	}

	/**
	 * Extensible pestañas (véase {@link FactoríaConfiguración#extensiblePestañas()})
	 * con capacidad para configurar valores como una lista desplegable.
	 *
	 * <p>Para realizar comunicaciones cuyo contenido sea la pestaña escogida,
	 * utilizar la instancia {@link ConfiguradorLista} argumento. Para realizar
	 * comunicaciones con todo el contenido del {@link ConfiguradorExtensible},
	 * usar la instancia devuelta.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensiblePestañas(String nombre, ConfiguradorLista configurador) {
		PanelPestañas pestañas = new PanelPestañas();
		ConfiguradorExtensible configuradorExtensible = new ListaConfiguradores();

		pestañas.subscribir(configurador, valor -> configurador.convertirOpción((String) valor));
		configurador.subscribir(pestañas, valor -> configurador.getOpción());

		configuraciónComún(configuradorExtensible,pestañas,nombre);

		configuradorExtensible.agregar(configurador);

		return configuradorExtensible;
	}

	//Árbol

	/**
	 * {@link ConfiguradorExtensible} que dispone los elementos gráficos de las instancias
	 * {@link Configurador} que se le agregen en una estructura de árbol: ({@link PanelÁrbol}).
	 *
	 * <p>El nombre de cada nodo hoja será el del {@link Configurador} asociado (y el
	 * de su componente gráfico). El nombre del directorio raíz es el del
	 * {@link ConfiguradorExtensible}. </p>
	 *
	 * <p>Si se agrega un extensible con forma de árbol a otro, el árbol del segundo aparecerá
	 * como un directorio en el primero.</p>
	 *
	 * <p><IMG SRC="../Imágenes/árbol.png"/></p>
	 *
	 * <p>El ancho automático, el máximo y el mínimo del panel con estructura de árbol,
	 * es configurable.</p>
	 *
	 * @return {@link ConfiguradorExtensible}.
	 */
	public ConfiguradorExtensible extensibleÁrbol() {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		PanelÁrbol panelÁrbol = new PanelÁrbol();

		configuraciónComún(configurador,panelÁrbol,"");

		panelÁrbol.setPorcentajeApariciónÁrbol(getPorcentajeApariciónÁrbol());
		panelÁrbol.setPorcentajeMáximoAnchoÁrbol(getPorcentajeMáximoAnchoÁrbol());
		panelÁrbol.setAnchoMínimoÁrbol(getAnchoMínimoÁrbol());

		return configurador;
	}

	/**
	 * Véase {@link FactoríaConfiguración#extensibleÁrbol()}.
	 *
	 * <p>Permite la introducción de un nombre.</p>
	 *
	 * @param nombre {@link String}. Nombre del {@link ConfiguradorExtensible}.
	 * @return
	 */
	public ConfiguradorExtensible extensibleÁrbol(String nombre) {
		ConfiguradorExtensible configurador = new ListaConfiguradores();
		PanelÁrbol panelÁrbol = new PanelÁrbol(nombre);

		configuraciónComún(configurador,panelÁrbol,nombre);

		panelÁrbol.setPorcentajeApariciónÁrbol(getPorcentajeApariciónÁrbol());
		panelÁrbol.setPorcentajeMáximoAnchoÁrbol(getPorcentajeMáximoAnchoÁrbol());
		panelÁrbol.setAnchoMínimoÁrbol(getAnchoMínimoÁrbol());

		return configurador;
	}

	//Barras

	/**
	 * Agrega barras de desplazamiento ({@link PanelBarras)} al elemento gráfico de un
	 * {@link Configurador}.
	 *
	 * @param configurador {@link Configurador} a cuyo elemento gráfico se le agregarán barras.
	 */
	public void agregarBarras(Configurador configurador) {
		JComponent componente = configurador.getComponenteGráfico();

		PanelBarras barras = new PanelBarras(componente);
		barras.setName(componente.getName());

		configurador.setComponenteGráfico(barras);

		barras.setPorcentajeAltoPantalla(porcentajeAltoBarras);
	}


	/////////////////////////////////////////////////////////
	//
	//	Selectores y Mutadores
	//
	/////////////////////////////////////////////////////////


	public Boolean getExtensiblesOpacos() {
		return extensiblesOpacos;
	}

	public void setExtensiblesOpacos(Boolean extensiblesOpacos) {
		this.extensiblesOpacos = extensiblesOpacos;
	}

	public Boolean getVerticalTitulado() {
		return verticalTitulado;
	}

	public void setVerticalTitulado(Boolean verticalTitulado) {
		this.verticalTitulado = verticalTitulado;
	}

	public float getPorcentajeMáximoAnchoÁrbol() {
		return porcentajeMáximoAnchoÁrbol;
	}

	public void setPorcentajeMáximoAnchoÁrbol(float porcentajeMáximoAnchoÁrbol) {
		this.porcentajeMáximoAnchoÁrbol = porcentajeMáximoAnchoÁrbol;
	}

	public float getPorcentajeApariciónÁrbol() {
		return porcentajeApariciónÁrbol;
	}

	public void setPorcentajeApariciónÁrbol(float porcentajeApariciónÁrbol) {
		this.porcentajeApariciónÁrbol = porcentajeApariciónÁrbol;
	}

	public int getAnchoMínimoÁrbol() {
		return anchoMínimoÁrbol;
	}

	public void setAnchoMínimoÁrbol(int anchoMínimoÁrbol) {
		this.anchoMínimoÁrbol = anchoMínimoÁrbol;
	}

	public float getPorcentajeAltoBarras() {
		return porcentajeAltoBarras;
	}

	public void setPorcentajeAltoBarras(float porcentajeAltoBarras) {
		this.porcentajeAltoBarras = porcentajeAltoBarras;
	}

	public AspectoConfigurable getAspectoMarco() {
		return aspectoMarco;
	}

	public int getEspacioBotones() {
		return espacioBotones;
	}

	public void setEspacioBotones(int espacioBotones) {
		this.espacioBotones = espacioBotones;
	}

	public int getMargenMarco() {
		return margenMarco;
	}

	public void setMargenMarco(int margenMarco) {
		this.margenMarco = margenMarco;
	}

	public AspectoConfigurable getAspectoPanelesSimples() {
		return aspectoPanelesSimples;
	}

	public int getMargenPaneles() {
		return margenPaneles;
	}

	public void setMargenPaneles(int margenPaneles) {
		this.margenPaneles = margenPaneles;
	}

	public int getGananciaAltura() {
		return gananciaAltura;
	}

	public void setGananciaAltura(int gananciaAltura) {
		this.gananciaAltura = gananciaAltura;
	}

	public Casilla.Estilo getEstiloCasilla() {
		return estiloCasilla;
	}

	public void setEstiloCasilla(Casilla.Estilo estiloCasilla) {
		this.estiloCasilla = estiloCasilla;
	}

	public int getAnchoMínimoLista() {
		return anchoMínimoLista;
	}

	public void setAnchoMínimoLista(int anchoMínimoLista) {
		this.anchoMínimoLista = anchoMínimoLista;
	}

	public int getAltoTextoRecuadro() {
		return altoTextoRecuadro;
	}

	public void setAltoTextoRecuadro(int altoTextoRecuadro) {
		this.altoTextoRecuadro = altoTextoRecuadro;
	}

	public Alineación getAlineaciónTítuloRecuadro() {
		return alineaciónTítuloRecuadro;
	}

	public void setAlineaciónTítuloRecuadro(Alineación alineaciónTítuloRecuadro) {
		this.alineaciónTítuloRecuadro = alineaciónTítuloRecuadro;
	}

	public int getAnchoNormalRenglón() {
		return anchoNormalRenglón;
	}

	public void setAnchoNormalRenglón(int anchoNormalRenglón) {
		this.anchoNormalRenglón = anchoNormalRenglón;
	}

	public int getAnchoGrandeRenglón() {
		return anchoGrandeRenglón;
	}

	public void setAnchoGrandeRenglón(int anchoGrandeRenglón) {
		this.anchoGrandeRenglón = anchoGrandeRenglón;
	}

	public AspectoConfigurable getAspectoTítulos() {
		return aspectoTítulos;
	}

	public int getMargenTítulos() {
		return margenTítulos;
	}

	public void setMargenTítulos(int margenTítulos) {
		this.margenTítulos = margenTítulos;
	}

}

