package dc;

/**
 * Configurador compatible con un elemento gráfico con entrada de texto. En el
 * proyecto se asocia con {@link dc.gui.paneles.simples.PanelTextual}.
 *
 * <p>Esta interfaz es usada por la factoría para comunicar
 * el {@link Configurador} con su componente gráfico.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface ConfiguradorTextual extends Configurador {

	/**
	 * Procesa un texto modificándolo potencialmente.
	 *
	 * @param texto Texto a procesar.
	 * @return Texto procesado.
	 */
	default String limpiarTexto(String texto) {
		return texto;
	}

	/**
	 * Dado un texto, devuelve el valor del configurador.
	 *
	 * @param texto Texto introducido en el componente gráfico.
	 * @return Valor del configurador.
	 */
	Object convertirTexto(String texto);

	/**
	 * Texto que debe tener el componente gráfico en función del valor en el que
	 * se encuentre el configurador.
	 * @return Texto del componente gráfico.
	 */
	String getTexto();

}
