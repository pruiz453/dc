package dc;

import dc.comunicador.Reacción;

import javax.swing.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link ConfiguradorExtensible} que constituye una lista de {@link Configurador}.
 *
 * <p>La clase hereda de {@link ConfiguradorSimple}, aplicando el patrón <i>Composite</i>.
 * Luego posee {@link ListaConfiguradores#listaConfiguradores} como atributo.</p>
 *
 * <p>Obsérvese que implementa todos los métodos de {@link ConfiguradorExtensible} y
 * no es una clase abstracta.</p>
 *
 * <p>Al agregar un {@link Configurador} nos subscrubimos a sus mensajes por
 * {@link ConfiguradorSimple#CANAL_ACTUALIZACIÓN}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
class ListaConfiguradores extends ConfiguradorSimple implements ConfiguradorExtensible {

	private List<Configurador> listaConfiguradores;

	/**
	 * Su valor determina la comunicación de actualizaciones.
	 */
	private boolean atenderActualizaciones = true;

	/**
	 * Constructor
	 */
	public ListaConfiguradores() {
		listaConfiguradores = new LinkedList<>();
	}

	@Override
	public void agregar(JComponent componente) {
		if(componente!= null) getComponenteGráfico().add(componente);
	}

	@Override
	public void agregar(Configurador configurador) {
		int índice = listaConfiguradores.size();

		listaConfiguradores.add(configurador);
		agregar(configurador.getComponenteGráfico());

		//Nos subscribimos a sus mensajes de actualización.
		configurador.subscribir(CANAL_ACTUALIZACIÓN,this);
	}

	@Override
	public void agregar(Collection<? extends ConfiguradorExtensible> colección) {

		ConfiguradorExtensible anterior = this;

		for(ConfiguradorExtensible configurador : colección) {

			anterior.agregar(configurador);
			anterior = configurador;
		}
	}


	@Override
	public String validar() {
		StringBuilder sb = new StringBuilder();

		for (Configurador configurador : listaConfiguradores) {
			String validación = configurador.validar();

			if (validación != null && validación.length() != 0) {
				sb.append(validación);
				sb.append("\n");
			}
		}

		return sb.toString();
	}

	@Override
	public void salvar() {
		for (Configurador configurador : listaConfiguradores) {
			configurador.salvar();
		}
	}

	@Override
	public Reacción procesarMensaje(Object mensaje) {

		//Al recibir un mensaje de tipo lista, mandamos un mensaje a cada uno
		//de los configuradores contenidos
		if(mensaje instanceof List) {

			boolean propagar = false;

			List<Object> lista = (List<Object>) mensaje;

			if(lista.size() != listaConfiguradores.size())
				throw new RuntimeException("Lista con número de elementos inválidos en ConfiguradorExtensible");

			int índice = 0;
			atenderActualizaciones = false;
			for (Configurador configurador : listaConfiguradores) {
				Reacción reacción = configurador.procesarMensaje(lista.get(índice++));

				if((reacción == Reacción.Propagar)){
					configurador.comunicar();
					propagar = true;
				}
				if(reacción == Reacción.Bloquear) {
					return Reacción.Bloquear;
				}
			}
			atenderActualizaciones = true;

			if(propagar) {
				//Se ha producido un cambio efectivo, avisamos al contenedor padre
				actualizar();

				return Reacción.Propagar;
			}

			return Reacción.No_Propagar;

		}else if (mensaje instanceof MensajeActualización){
			//Si el mensaje es de actualización, se ha producido un cambio en un
			//configurador hijo. Lo comunicamos dependiendo del booleano atenderActualizaciones

			if(atenderActualizaciones) {

				//Comunicamos con los posibles ConfiguradorExtensible conectados
				comunicar();

				return Reacción.Propagar;
			}

			return Reacción.No_Propagar;
		}else {
			throw new RuntimeException("Valor inválido en recibirValor de ConfiguradorExtensible: " + mensaje);
		}
	}

	@Override
	public Object obtenerValor() {
		List<Object> lista = new LinkedList<>();

		for(Configurador configurador : listaConfiguradores) {
			lista.add(configurador.obtenerValor());
		}

		return lista;
	}

	@Override
	public Collection<Configurador> getConfiguradores() {
		return listaConfiguradores;
	}
}
