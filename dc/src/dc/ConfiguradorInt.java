package dc;


/**
 * Configuración de valores int.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorInt extends ConfiguradorEntero {


    /**
     * Constructor por defecto inicializado en 0.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     */
    public ConfiguradorInt() {
        super((int)0,Integer.MIN_VALUE,Integer.MAX_VALUE);
    }

    /**
     * Constructor que permite la inicialización.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     *
     * @param valorInicial Valor para la inicialización
     */
    public ConfiguradorInt(int valorInicial) {
        super(valorInicial,Integer.MIN_VALUE,Integer.MAX_VALUE);
    }

    /**
     * Constructor que permite la introducción de límites.
     * El valor inicial será el punto medio.
     *
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorInt(int mínimo, int máximo) {
        super((mínimo + máximo)/2,mínimo,máximo);
    }

    /**
     * Constructor general. Permite la introducción de límites y
     * del valor de inicialización.
     *
     * <p>Si valorInicial no está en el intervalo permitido se terminará
     * la ejecución del programa.</p>
     *
     * @param valorInicial Valor para la inicialización
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorInt(int valorInicial, int mínimo, int máximo) {
        super(valorInicial, mínimo, máximo);
    }

    /// Selectores y Mutadores ///

    /**
     * Selector del valor del configurador.
     *
     * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
     * y {@link Configurador#salvar()}.</p>
     *
     * @return El valor del configurador.
     */
    public int getValor()
    {
        return (int) valor;
    }
}