package dc;


/**
 * Configuración de valores float.
 *
 * @author Pablo Ruiz Sánchez
 */
public abstract class ConfiguradorFloat extends ConfiguradorFlotante {

    /**
     * Constructor por defecto inicializado en 0.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     */
    public ConfiguradorFloat() {
        super((float)0,-Float.MAX_VALUE,Float.MAX_VALUE);
    }

    /**
     * Constructor que permite la inicialización.
     *
     * <p> Los valores límites serán los mínimo y máximo asociados al tipo numérico.</p>
     *
     * @param valorInicial Valor para la inicialización
     */
    public ConfiguradorFloat(float valorInicial) {
        super(valorInicial,-Float.MAX_VALUE,Float.MAX_VALUE);
    }

    /**
     * Constructor que permite la introducción de límites.
     * El valor inicial será el punto medio.
     *
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorFloat(float mínimo, float máximo) {
        super((mínimo + máximo)/2,mínimo,máximo);
    }

    /**
     * Constructor general. Permite la introducción de límites y
     * del valor de inicialización.
     *
     * <p>Si valorInicial no está en el intervalo permitido se terminará
     * la ejecución del programa.</p>
     *
     * @param valorInicial Valor para la inicialización
     * @param mínimo Mínimo valor posible del configurador.
     * @param máximo Máximo valor posible del configurador.
     */
    public ConfiguradorFloat(float valorInicial, float mínimo, float máximo) {
        super(valorInicial, mínimo, máximo);
    }

    /// Selectores y Mutadores ///

    /**
     * Selector del valor del configurador.
     *
     * <p>Debe ser llamado para definir los métodos {@link Configurador#validar()}
     * y {@link Configurador#salvar()}.</p>
     *
     * @return El valor del configurador.
     */
    public float getValor(){
        return (float) valor;
    }
}