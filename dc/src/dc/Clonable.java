package dc;

/**
 * Las instancias {@link Clonable} poseen la capacidad de duplicarse. El método
 * {@link Clonable#clonar()} devuelve una instancia con sus mismos atributos.
 *
 * <p>Para que una instancia sea clonable, todos sus atributos
 * deben serlo. Una vez que se satisface dicho requisito, y comprobado que la copia
 * tiene éxtio, es redundante que un tercero incluya el bloque try/catch en su código.
 * Esta clase evita esa redundancia.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public class Clonable implements Cloneable {
    /**
     * Devuelve un clon de la instancia.
     *
     * @return Copia de la instancia.
     */
    public Clonable clonar() {
        try {
            return (Clonable)clone();
        }catch (CloneNotSupportedException ex) {
            //No debería de producirse
            System.out.println(ex.toString());
            System.exit(-1);
        }
        return null;
    }
}
