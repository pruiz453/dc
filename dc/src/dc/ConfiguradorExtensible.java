package dc;

import javax.swing.*;
import java.util.Collection;

/**
 * {@link Configurador} con capacidad para incorporar otras instancias
 * {@link Configurador}.
 *
 * <p>Para que un componente gráfico pueda ser asociado con un {@link ConfiguradorExtensible}
 * debe de mostrar de forma adecuada los diferentes elementos que se le agregen sin
 * que sea necesarios indicar posiciones.</p>
 *
 * <p>El paquete {@link dc.gui.paneles.extensibles}</p> posee un conjunto de paneles
 * compatibles y que pueden ser asociados con {@link FactoríaConfiguración}.</p>
 *
 * <p>Posee un método para agregar configuradores, componentes gráficos o una colección
 * de {@link ConfiguradorExtensible}.</p>
 *
 * @author Pablo Ruiz Sánchez
 */
public interface ConfiguradorExtensible extends Configurador{

    /**
     * Agrega un {@link Configurador}.
     *
     * @param configurador {@link Configurador} a agregar.
     */
    void agregar(Configurador configurador);

    /**
     * Agrega el primer elemento de la colección con
     * {@link ConfiguradorExtensible#agregar(Configurador)}. Luego recorre la colección
     * agregando a cada uno al anterior formando una cadena.
     * @param colección {@link Collection} de {@link ConfiguradorExtensible}.
     */
    void agregar(Collection<? extends ConfiguradorExtensible> colección);

    /**
     * Agrega un elemento gráfico al suyo. Útil para agregar elementos que no
     * configuran valores (no poseen {@link Configurador} asociado).
     * @param componente Componente gráfico a agregar.
     */
    void agregar(JComponent componente);

    /**
     * Devuelve la colección de {@link Configurador} que contiene.
     * @return
     */
    Collection<Configurador> getConfiguradores();
}
