package pruebas;


import dc.*;
public class D implements Configurable {

    private byte byte1;
    private short short1;
    private int entero1;
    private long long1;
    private float float1;
    private double doble1;

    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        ConfiguradorExtensible configuradorD = factoría.extensibleVertical("Panel D");


        Configurador pByte = factoría.desplazador("Byte1", new ConfiguradorByte() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setByte1(getValor());
            }
        });

        Configurador pShort = factoría.desplazador("Short1", new ConfiguradorShort() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setShort1(getValor());
            }
        });
        Configurador pEntero = factoría.desplazador("Entero1", new ConfiguradorInt() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setEntero1(getValor());
            }
        });
        Configurador pLong = factoría.desplazador("Long1", new ConfiguradorLong() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setLong1(getValor());
            }
        });
        Configurador pFloat = factoría.desplazador("Float1", new ConfiguradorFloat() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setFloat1(getValor());
            }
        });
        Configurador pDoble = factoría.desplazador("Doble1", new ConfiguradorDouble() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setDoble1(getValor());
            }
        });

        pByte.emisiónGlobal("PanelD");
        pByte.subscripciónGlobal("PanelD");
        pShort.emisiónGlobal("PanelD");
        pShort.subscripciónGlobal("PanelD");
        pEntero.emisiónGlobal("PanelD");
        pEntero.subscripciónGlobal("PanelD");



        configuradorD.agregar(pByte);
        configuradorD.agregar(pShort);
        configuradorD.agregar(pEntero);
        configuradorD.agregar(pLong);
        configuradorD.agregar(pFloat);
        configuradorD.agregar(pDoble);

        return configuradorD;
    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase D" +
                "\nByte1:\t" + byte1 +
                "\nShort1:\t" + short1 +
                "\nEntero1:\t" + entero1 +
                "\nLong1:\t" + long1 +
                "\nFloat1:\t" + float1 +
                "\nDouble1:\t" + doble1;
    }

    public byte getByte1() {
        return byte1;
    }

    public void setByte1(byte byte1) {
        this.byte1 = byte1;
    }

    public short getShort1() {
        return short1;
    }

    public void setShort1(short short1) {
        this.short1 = short1;
    }

    public int getEntero1() {
        return entero1;
    }

    public void setEntero1(int entero1) {
        this.entero1 = entero1;
    }

    public long getLong1() {
        return long1;
    }

    public void setLong1(long long1) {
        this.long1 = long1;
    }

    public float getFloat1() {
        return float1;
    }

    public void setFloat1(float float1) {
        this.float1 = float1;
    }

    public double getDoble1() {
        return doble1;
    }

    public void setDoble1(double doble1) {
        this.doble1 = doble1;
    }
}
