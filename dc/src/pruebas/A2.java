package pruebas;

import dc.*;

public class A2 extends A1 {
    private java.lang.String cadena2;
    private String cadena3;
    private boolean booleano2;
    private boolean booleano3;
    private char caracter2;

    @Override
    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        
        ConfiguradorExtensible configuradorA2 = factoría.extensibleVertical("Panel A2");

        ConfiguradorExtensible configuradorHorizontal = factoría.extensibleHorizontal();
        configuradorA2.agregar(configuradorHorizontal);

        Configurador configuradorA1 = super.getConfigurador();
        configuradorHorizontal.agregar(configuradorA1);

        ConfiguradorExtensible camposA2 = factoría.extensibleVertical("Atributos A2");
        configuradorHorizontal.agregar(camposA2);

        Configurador configuradorCadena2 = factoría.recuadro("Cadena2", new ConfiguradorString() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setCadena2(getValor());
            }
        });
        camposA2.agregar(configuradorCadena2);

        Configurador configuradorCadena3;
        camposA2.agregar(configuradorCadena3 = factoría.renglón("Cadena3", new ConfiguradorString() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setCadena3(getValor());
            }
        }));


        Configurador booleano2 = factoría.casilla("Booleano2", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setBooleano2(getValor());
            }
        });
        camposA2.agregar(booleano2);

        Configurador booleano3 = factoría.lista("Booleano3", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setBooleano3(getValor());

            }
        });
        camposA2.agregar(booleano3);

        Configurador configuradorCaracter2 = factoría.lista("Carácter2", new ConfiguradorChar(new char[]{'a','b','c'}) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setCaracter2(getValor());
            }
        });
        camposA2.agregar(configuradorCaracter2);

        getPanelCaracter1().subscribir(configuradorCaracter2);
        configuradorCaracter2.subscribir(getPanelCaracter1());

        booleano2.subscribir(booleano3, valor -> ! ((Boolean)valor));
        booleano3.subscribir(booleano2, valor -> ! ((Boolean)valor));

        configuradorCadena3.subscribir(configuradorCadena2, valor -> ((String) valor).toUpperCase());
        configuradorCadena2.subscribir(configuradorCadena3, valor -> ((String) valor).toLowerCase());

        return configuradorA2;
    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase A2" +
                super.toString() +
                "\n  Atributos A2" +
                "\nCadena2:\t" + cadena2+
                "\nCadena3:\t" + cadena3+
                "\nBooleano2:\t" + booleano2+
                "\nBooleano3:\t" + booleano3+
                "\nCaracter2:\t" + caracter2;
    }


    public String getCadena2() {
        return cadena2;
    }

    public void setCadena2(String cadena2) {
        this.cadena2 = cadena2;
    }

    public String getCadena3() {
        return cadena3;
    }

    public void setCadena3(String cadena3) {
        this.cadena3 = cadena3;
    }

    public boolean isBooleano2() {
        return booleano2;
    }

    public void setBooleano2(boolean booleano2) {
        this.booleano2 = booleano2;
    }

    public boolean isBooleano3() {
        return booleano3;
    }

    public void setBooleano3(boolean booleano3) {
        this.booleano3 = booleano3;
    }

    public char getCaracter2() {
        return caracter2;
    }

    public void setCaracter2(char caracter2) {
        this.caracter2 = caracter2;
    }
}
