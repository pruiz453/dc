package pruebas;

import dc.*;

public class A implements Configurable {
    private int entero1;
    private char caracter1;
    private float flotante1;
    private int entero2;
    private float flotante2;

    private Configurador panelCaracter1;

    public A() {
    }

    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        ConfiguradorExtensible configuradorA = factoría.extensibleVertical("Configuración A");

        Configurador entero1 = factoría.desplazador("Entero1",
                new ConfiguradorInt(20,100) {
            public String validar() {
                if(getValor() %2 != 0) {
                    return "El valor Entero1 de A debe ser par.";
                }else{
                    return "";
                }
            }
            public void salvar(){
                setEntero1(getValor());
            }
        });

        configuradorA.agregar(entero1);

        Configurador entero2 = factoría.renglón("Entero2",
                new ConfiguradorInt(-100,100) {
                    public String validar() {
                        return "";
                    }
                    public void salvar(){
                        setEntero2(getValor());
                    }
                });
        configuradorA.agregar(entero2);


        configuradorA.agregar(panelCaracter1 = factoría.renglón("Carácter1", new ConfiguradorChar('y') {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setCaracter1(getValor());
            }
        }));


        configuradorA.agregar(factoría.renglón("Flotante1", new ConfiguradorFloat() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setFlotante1(getValor());
            }
        }));

        Configurador flotante2 = factoría.desplazador("Flotante2", new ConfiguradorFloat(-20,150) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setFlotante2((float) getValor());
            }
        });
        configuradorA.agregar(flotante2);


//        entero1.subscribir(flotante2);
//        entero1.subscribir(entero2);
//        entero2.subscribir(entero1);
//        flotante2.subscribir(entero1);


        return configuradorA;


    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase A" +
                "\nEntero1:\t" + entero1 +
                "\nEntero2:\t" + entero2 +
                "\nCaracter1:\t" + caracter1 +
                "\nFlotante1:\t" + flotante1 +
                "\nFlotante2:\t" + flotante2;
    }

    public Configurador getPanelCaracter1() {
        return panelCaracter1;
    }

    public int getEntero1() {
        return entero1;
    }

    public void setEntero1(int entero1) {
        this.entero1 = entero1;
    }

    public char getCaracter1() {
        return caracter1;
    }

    public void setCaracter1(char caracter1) {
        this.caracter1 = caracter1;
    }

    public float getFlotante1() {
        return flotante1;
    }

    public void setFlotante1(float flotante1) {
        this.flotante1 = flotante1;
    }

    public int getEntero2() {
        return entero2;
    }

    public void setEntero2(int entero2) {
        this.entero2 = entero2;
    }

    public float getFlotante2() {
        return flotante2;
    }

    public void setFlotante2(float flotante2) {
        this.flotante2 = flotante2;
    }
}
