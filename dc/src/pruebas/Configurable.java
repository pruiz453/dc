package pruebas;

import dc.Configurador;

public interface Configurable {

	Configurador getConfigurador();

}
