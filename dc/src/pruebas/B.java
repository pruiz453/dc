package pruebas;

import dc.*;
import dc.gui.ColorConfigurable;
import dc.gui.ConfiguradorColor;

public class B implements Configurable {
    private A1 a1;
    private A2 a2;
    private C c;
    private ColorConfigurable color1;
    private ColorConfigurable color2;

    public B() {
        a1 = new A1();
        a2 = new A2();
        c = new C();
        color1 = new ColorConfigurable();
        color2 = new ColorConfigurable();
    }

    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        
        ConfiguradorExtensible configuradorB = factoría.extensibleÁrbol("Clase B");
        configuradorB.agregar(a1.getConfigurador());
        configuradorB.agregar(a2.getConfigurador());
        configuradorB.agregar(c.getConfigurador());

        

        ConfiguradorExtensible camposB = factoría.extensibleVertical("Atributos B");

        color1.setNombre("Color 1");
        color2.setNombre("Color 2");

        Configurador pColor1 = ConfiguradorColor.configurar(color1);
        Configurador pColor2 = ConfiguradorColor.configurar(color2);

        camposB.agregar(pColor1);
        camposB.agregar(pColor2);

        pColor1.emisiónGlobal("PanelB");
        pColor1.subscripciónGlobal("PanelB");
        pColor2.emisiónGlobal("PanelB");
        pColor2.subscripciónGlobal("PanelB");

        factoría.agregarBarras(camposB);
        configuradorB.agregar(camposB);

        return configuradorB;
    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase B\n" +
                a1.toString() + "\n" +
                a2.toString() + "\n" +
                c.toString() + "\n" +
                "\n  Atributos B" +
                "\nColor1:\t" + color1 +
                "\nColor2:\t" + color2;
    }

    public A1 getA1() {
        return a1;
    }

    public void setA1(A1 a1) {
        this.a1 = a1;
    }

    public A2 getA2() {
        return a2;
    }

    public void setA2(A2 a2) {
        this.a2 = a2;
    }

    public C getC() {
        return c;
    }

    public void setC(C c) {
        this.c = c;
    }


}
