package pruebas;

import dc.*;

public class A1 extends A {
    
    private boolean booleano1;
    private java.lang.String cadena1;

    @Override 
    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        
        ConfiguradorExtensible extensibleA1 = factoría.extensibleVertical("Panel A1");

        ConfiguradorExtensible extensibleHorizontal = factoría.extensibleHorizontal();
        extensibleA1.agregar(extensibleHorizontal);

        Configurador extensibleA = super.getConfigurador();
        extensibleHorizontal.agregar(extensibleA);

        ConfiguradorExtensible camposA1 = factoría.extensibleVertical("Atributos A1");

        camposA1.agregar(factoría.casilla("Booleano1", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setBooleano1(getValor());
            }
        }));



        Configurador extensibleCadena1 = factoría.renglón("Cadena1", new ConfiguradorString() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setCadena1(getValor());
            }
        });

        camposA1.agregar(extensibleCadena1);

        extensibleHorizontal.agregar(camposA1);

        return extensibleA1;
    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase A1" +
                super.toString() +
                "\n  Atributos A1" +
                "\nDoble1:\t" + booleano1 +
                "\nCadena1:\t" + cadena1;
    }


    public boolean isBooleano1() {
        return booleano1;
    }

    public void setBooleano1(boolean booleano1) {
        this.booleano1 = booleano1;
    }

    public java.lang.String getCadena1() {
        return cadena1;
    }

    public void setCadena1(java.lang.String cadena2) {
        this.cadena1 = cadena2;
    }
}
