package pruebas;

import dc.*;
public class C implements Configurable {
    private D d;
    private byte byte2;
    private short short2;
    private int entero2;
    private long long2;

    public C() {
        d = new D();
    }

    public Configurador getConfigurador(){

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();


        ConfiguradorExtensible configuradorC = factoría.extensiblePestañas("Panel C");

        ConfiguradorExtensible camposC = factoría.extensibleVertical("Atributos C");

        configuradorC.agregar(camposC);
        configuradorC.agregar(d.getConfigurador());

        Configurador pByte;
        Configurador pShort;
        Configurador pEntero;
        Configurador pLong;

        camposC.agregar(pByte = factoría.renglón("Byte2", new ConfiguradorByte() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setByte2(getValor());
            }
        }));

        camposC.agregar(pShort = factoría.renglón("Short2", new ConfiguradorShort() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setShort2(getValor());
            }
        }));

        camposC.agregar(pEntero = factoría.renglón("Entero2", new ConfiguradorInt(-1000,1000) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setEntero2(getValor());
            }
        }));

        camposC.agregar(pLong = factoría.renglón("Long2", new ConfiguradorLong(-1000,1000) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setLong2(getValor());
            }
        }));

        pByte.emisiónGlobal("PanelC");
        pByte.subscripciónGlobal("PanelC");
        pShort.emisiónGlobal("PanelC");
        pShort.subscripciónGlobal("PanelC");
        pEntero.emisiónGlobal("PanelC");
        pEntero.subscripciónGlobal("PanelC");
        pLong.emisiónGlobal("PanelC");
        pLong.subscripciónGlobal("PanelC");

        return configuradorC;
    }

    @Override
    public java.lang.String toString() {
        return "\n  Clase C" +
                d.toString() +
                "\n  AtributosC"+
                "\nByte2:\t" + byte2 +
                "\nShort2:\t" + short2 +
                "\nEntero2:\t" + entero2 +
                "\nLong2:\t" + long2;
    }

    public D getD() {
        return d;
    }

    public void setD(D d) {
        this.d = d;
    }

    public byte getByte2() {
        return byte2;
    }

    public void setByte2(byte byte2) {
        this.byte2 = byte2;
    }

    public short getShort2() {
        return short2;
    }

    public void setShort2(short short2) {
        this.short2 = short2;
    }

    public int getEntero2() {
        return entero2;
    }

    public void setEntero2(int entero2) {
        this.entero2 = entero2;
    }

    public long getLong2() {
        return long2;
    }

    public void setLong2(long long2) {
        this.long2 = long2;
    }
}
