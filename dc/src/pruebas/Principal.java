package pruebas;

import dc.*;
import dc.comunicador.Comunicador;
import dc.gui.paneles.simples.Casilla;

import javax.swing.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Principal {
    public static void main(String[] args) {
        new Principal();
    }

    private String nombrePrueba;
    private int iteración;

    public Principal() {
//        Comunicador.setModoDepuraciónGlobal(true);

        ConfiguradorFactoría.configurar(
                () -> configurarPrueba(),
                () -> System.exit(1) );
    }

    private  void configurarPrueba() {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        String[] pruebas = new String[] {"A","A1","A2","B","C","D","Árbol","Pestañas"};
        ConfiguradorExtensible verticalPruebas = factoría.extensibleVertical("Configuración Prueba");

        verticalPruebas.agregar(factoría.lista("Nombre Prueba", new ConfiguradorString(3,pruebas) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setNombrePrueba(getValor());
            }
        }));

        verticalPruebas.agregar(factoría.desplazador("Iteraciones", new ConfiguradorInt(1,10) {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
                setIteración(getValor());
            }
        }));

        JFrame marco = factoría.marco("Configurar Prueba", verticalPruebas,
                ()->probar(),
                () -> System.exit(1));

        marco.setVisible(true);
    }

    private  void probar() {
        if (nombrePrueba.equals("A")) pruebaConfigurable(new A());
        if (nombrePrueba.equals("A1")) pruebaConfigurable(new A1());
        if (nombrePrueba.equals("A2")) pruebaConfigurable(new A2());
        if (nombrePrueba.equals("B")) pruebaConfigurable(new B());
        if (nombrePrueba.equals("C")) pruebaConfigurable(new C());
        if (nombrePrueba.equals("D")) pruebaConfigurable(new D());
        if(nombrePrueba.equals("Árbol")) pruebaÁrbol();
        if(nombrePrueba.equals("Pestañas")) pruebaPestañas();
    }

    private  void pruebaConfigurable(Configurable v){
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        JFrame marco = factoría.marco("Prueba Visualizable", v.getConfigurador(),()->{
            factoría.mensaje("Éxito", v.toString(),() -> System.exit(1));

        },()->System.exit(1));

        marco.setVisible(true);
    }



    private  void pruebaComunicación() {
        pruebaConfigurable(new B());
        pruebaConfigurable(new B());
    }


    private  void pruebaÁrbol() {

        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        ConfiguradorExtensible d = factoría.extensibleÁrbol("Categoría 1");
        rellenar(d,iteración);

        JFrame marco = factoría.marco("Prueba Árbol", d,null,()->System.exit(1));

        marco.setVisible(true);
    }

    private  void pruebaPestañas() {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        ConfiguradorExtensible d = factoría.extensiblePestañas("");
        rellenar(d,iteración);

        JFrame marco = factoría.marco("Prueba Pestañas", d,null,()->System.exit(1));

        marco.setVisible(true);
    }

    private  void rellenar(ConfiguradorExtensible configurador, int iteración) {
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();
        Random r = new Random();
        int vAletorio;
        int numHijos = 5;



        if(iteración > 0) {

            List<ConfiguradorExtensible> lista = new LinkedList<>();

            for(int i = 0; i < numHijos; i++) {

                vAletorio = Math.abs(r.nextInt() % 4);

                switch (vAletorio) {
                    case 0:
                        ConfiguradorExtensible extensibleHorizontal = factoría.extensibleHorizontal(i + "");
                        extensibleHorizontal.agregar(factoría.título(i + ""));
                        lista.add(extensibleHorizontal);
                        break;
                    case 1:
                        lista.add(factoría.extensibleVertical(i + ""));
                        break;
                    case 2:
                        lista.add(factoría.extensiblePestañas(i + ""));
                        break;
                    case 3:
                        lista.add(factoría.extensibleÁrbol(i + ""));
                        break;
                }
            }

            rellenar(lista.get(lista.size()-1),0);

            configurador.agregar(lista);

            rellenar(configurador,iteración - 1);

        }else {
            vAletorio = Math.abs(r.nextInt()) % 2;

            switch (vAletorio) {
                case 0:
                    configurador.agregar( new AProblemático().getConfigurador());
                    break;

                case 1:
                    configurador.agregar( new A1().getConfigurador());
                    break;

                default:
                    System.exit(1);
                    ;
            }
        }
    }

    public void pruebaGuía() {
        //Obtenemos la instancia por defecto
        FactoríaConfiguración factoría = FactoríaConfiguración.getInstancia();

        //Cargamos configuración
        try{
            factoría.leerDeDisco("configuración.dc");
        }catch (IOException ex) {
            ex.printStackTrace();
        }catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        //Clonamos la instancia
        FactoríaConfiguración factoríaClonada = (FactoríaConfiguración) factoría.clonar();

        //Modificamos el estilo de las casillas de verificación
        factoríaClonada.setEstiloCasilla(Casilla.Estilo.Redonda);

        //Creamos un extensible vertical
        ConfiguradorExtensible vertical = factoríaClonada.extensibleVertical("Clase A");

        //Creamos un configurador casilla
        Configurador casilla = factoríaClonada.casilla("Casilla", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
            }
        });

        //Creamos un configurador Lista
        Configurador lista = factoríaClonada.lista("Lista", new ConfiguradorBoolean() {
            @Override
            public String validar() {
                return null;
            }

            @Override
            public void salvar() {
            }
        });

        //Agregamos los Configuradores al ConfiguradorExtensible vertical
        vertical.agregar(casilla);
        vertical.agregar(lista);

        //Creamos un extensible horizontal que contendrá la configuración de ClaseA y de claseB (En este caso A1).
        ConfiguradorExtensible horizontal = factoríaClonada.extensibleHorizontal("Clase A");

        //Agregamos el configurador vertical creado anteriormente
        horizontal.agregar(vertical);

        //Agregamos el configurador de A1. Sopondremos que tiene visibilidad de atributo y un Selector getA1(). El método getConfigurador() de A1 devuelve el Configurador de la clase.
        horizontal.agregar(new A1().getConfigurador());

        //Antes de finalizar, comunicaremos los Configuradores casilla y lista para que sean opuestos
        casilla.subscribir(lista, valor -> !((Boolean)valor));
        lista.subscribir(casilla, valor -> !((Boolean)valor));

        casilla.comunicar();

        JFrame marco = factoríaClonada.marco("Clase A", horizontal,
                () ->
                        factoríaClonada.mensaje("Éxito","Configuración Aceptada", () -> System.exit(0)),

                () ->
                        factoríaClonada.mensaje("Cancelación","Configuración Cancelada", () -> System.exit(1))
        );

        marco.setVisible(true);
    }

    public String getNombrePrueba() {
        return nombrePrueba;
    }

    public void setNombrePrueba(String nombrePrueba) {
        this.nombrePrueba = nombrePrueba;
    }

    public int getIteración() {
        return iteración;
    }

    public void setIteración(int iteración) {
        this.iteración = iteración;
    }
}
