#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\usepackage{algorithmic}
\usepackage{algorithm}
\floatname{algorithm}{Algoritmo}
\floatname{table}{Tabla}

\usepackage{url}
\usepackage{hyperref}
\usepackage{fancyhdr}
\fancyhf{}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\makeatletter
\let\sv@endpart\@endpart
\def\@endpart{\thispagestyle{empty}\sv@endpart}
\makeatother
\end_preamble
\use_default_options true
\master maestro-pfg.lyx
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Índice
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
renewcommand{
\backslash
appendixname}{Anexo}
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Caracteres no ASCII en el código
\begin_inset CommandInset label
LatexCommand label
name "Anexo:Caracteres-no-ASCII"

\end_inset


\end_layout

\begin_layout Section
Problema y Solución
\end_layout

\begin_layout Standard
El código con caracteres no ASCII como tildes o ñ puede ser conflictivo
 con respecto la internacionalización del proyecto (¿Cómo invocar el método
 clase.tamaño() si mi teclado no tiene ñ?).
 Esta pequeña vicisitud puede ser soslayada con un sencillo script o programa
 que partiendo del directorio raíz recorra los archivos fuente sustituyendo
 á -> a, Á -> A, ñ -> n...
 y así sucesivamente.
 Aunque ésta no es la forma más eficiente de hacerlo, el siguiente sencillo
 script quitarTildes en bash eliminaría dichos caracteres del código: 
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "language=Java,float=p,numbers=left,basicstyle={\ttfamily},breaklines=true,tabsize=4"
inline false
status open

\begin_layout Plain Layout

\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "lis:Codigo-de-edit"

\end_inset

Código de quitarTildes
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

     #!/bin/bash
\end_layout

\begin_layout Plain Layout

	 if [ -d "$1" ]; then
\end_layout

\begin_layout Plain Layout

	 for i in ./$1/*; 
\end_layout

\begin_layout Plain Layout

		do 
\end_layout

\begin_layout Plain Layout

			quitarTildes $i;
\end_layout

\begin_layout Plain Layout

		done
\end_layout

\begin_layout Plain Layout

 	else
\end_layout

\begin_layout Plain Layout

		sed -i 's/á/a/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Á/A/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/é/e/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/É/e/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/í/i/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Í/I/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ó/o/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ó/O/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ú/u/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ú/U/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ñ/n/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ñ/N/g' $1
\end_layout

\begin_layout Plain Layout

	fi
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Chapter
Diferencias con libqew 
\begin_inset CommandInset label
LatexCommand label
name "chap:Diferencias-con-libqwe"

\end_inset


\end_layout

\begin_layout Standard
La principal diferencia en lo referente al diseño con 
\begin_inset Formula $libqew$
\end_inset

 se halla en que 
\begin_inset Formula $libqew$
\end_inset

 consiera que un
\begin_inset Newline linebreak
\end_inset

 
\begin_inset Formula $QewExtensibleDialog$
\end_inset

 es un diálogo usual con la capacidad de albergar otros diálogos.
 Si hubiésemos aplicado el mismo esquema a 
\begin_inset Formula $DC$
\end_inset

, un 
\begin_inset Formula $Configurador$
\end_inset

 sería un 
\begin_inset Formula $ConfiguradorExtensible$
\end_inset

, y debería poseer una variable 
\begin_inset Formula $enum$
\end_inset

 que especificara el tipo de extensión en caso de añadírsele un diálogo.
 En el momento en el que se agregara el primer diálogo, el 
\begin_inset Formula $Configurador$
\end_inset

 cambiaría su componente gráfico asociado por un 
\begin_inset Formula $PanelExtensible$
\end_inset

 que contendría su anterior 
\begin_inset Formula $PanelSimple$
\end_inset

 y el panel del 
\begin_inset Formula $Configurador$
\end_inset

 agregado.
 Es ahora cuando surge el verdadero problema, mi 
\begin_inset Formula $Configurador$
\end_inset

 inicial se ha transformado en uno extensible, y si yo quisiera comunicarlo
 con otro (pongamos que configure valores enteros), ¿cómo accedo a él? Y
 si decido que el 
\begin_inset Formula $Configurador$
\end_inset

 mantenga su identidad para poder comunicarlo, ¿cómo comunico el conjunto
 de los dos configuradores agregados (es decir el 
\begin_inset Formula $ConfiguradorExtensible$
\end_inset

 que forman) con otro conjunto?
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
De tal forma que un 
\begin_inset Formula $Configurador$
\end_inset

 y un 
\begin_inset Formula $ConfiguradorExtensible$
\end_inset

 deben ser identidades distintas.
 Su relación debe ser tal y como se ha especificado, de herencia, aplicando
 el patrón 
\begin_inset Formula $Composite$
\end_inset

.
 Así mismo, un 
\begin_inset Formula $ConfiguradorExtensible$
\end_inset

 es un 
\begin_inset Formula $Configurador$
\end_inset

 y a su vez un contenedor ordenado de 
\begin_inset Formula $Configurador$
\end_inset

 que existe y se crea con independencia de instancias específicas.
 
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
Otra diferencia se halla en el método 
\begin_inset Formula $validar$
\end_inset

.
 En 
\begin_inset Formula $libqew$
\end_inset

 devuelve un valor 
\begin_inset Formula $bool$
\end_inset

, mientras que en 
\begin_inset Formula $DC$
\end_inset

, devuelve un 
\begin_inset Formula $String$
\end_inset

 con los motivos por el cual el hipotético valor es inválido (de forma que
 al estar vacío o ser nulo, se considerará válido).
 Una alternativa a este diseño sería crear una clase 
\begin_inset Formula $Validación$
\end_inset

 que poseyera un booleano y una cadena con un posbile motivo, sin embargo
 sería redundante, y el valor del booleano deducible a partir de la cadena.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
Finalmente destacar las diferencias asociadas a los distintos lenguajes
 de programación para las que fueron diseñadas las librerías.
 Mientras que 
\begin_inset Formula $libqew$
\end_inset

 posee clases asociadas con diálogos, como 
\begin_inset Formula $QewExtensibleDialog$
\end_inset

, 
\begin_inset Formula $DC$
\end_inset

 posee clases que representan marcos (hijas de 
\begin_inset Formula $JFrame$
\end_inset

) y paneles (hijas de 
\begin_inset Formula $JPanel$
\end_inset

).
 
\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Chapter
Integración con Netbeans
\begin_inset CommandInset label
LatexCommand label
name "chap:Integración-con-Netbeans"

\end_inset


\end_layout

\begin_layout Standard
Todos los elementos gráficos de 
\begin_inset Formula $DC$
\end_inset

, salvo 
\begin_inset Formula $PanelBarras$
\end_inset

, poseen un constructor sin argumentos, de manera que pueden ser integrados
 en el editor 
\begin_inset Formula $Netbeans$
\end_inset

.
 Para ello, es necesario agregar los paneles de 
\begin_inset Formula $DC$
\end_inset

 a la paleta de 
\begin_inset Formula $Netbeans$
\end_inset

 a través de la entrada en el menú 
\begin_inset Formula $Tools\,-\,Palette\,-\,Swing/AWT\,Components$
\end_inset

.
 Se abrerá el menú que muestra la captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 1"
plural "false"
caps "false"
noprefix "false"

\end_inset

, donde se pueden crear una categoría para paneles simples y otra para extensibl
es.
\begin_inset Newline newline
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/1.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 1
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 1"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Tras ello, pulsar el botón 
\begin_inset Formula $"Add\,from\,jar"$
\end_inset

 y seleccionar el jar de la aplicación.
 Se abrirá la ventana que muestra la captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 2"
plural "false"
caps "false"
noprefix "false"

\end_inset

, donde se ven seleccionados los paneles simples que se añadirán a su correspond
iente categoría.
 La captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 3"
plural "false"
caps "false"
noprefix "false"

\end_inset

 hace lo propio con los paneles extensibles.
 Obsérvese que no aparece 
\begin_inset Formula $PanelBarras$
\end_inset

, ya que no tiene constructor sin argumentos.
\begin_inset Newline newline
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/2.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 2
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 2"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En la siguiente captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 4"
plural "false"
caps "false"
noprefix "false"

\end_inset

, se aprecia la paleta de 
\begin_inset Formula $Netbeans$
\end_inset

 con sus dos nuevas categorías y cómo se ha creado un nuevo 
\begin_inset Formula $JPanel$
\end_inset

, que se está actualmente diseñando.
 En dicho 
\begin_inset Formula $JPanel$
\end_inset

 se ha incuido un 
\begin_inset Formula $PanelVertical$
\end_inset

 en el centro.
 Para añadir elementos en el 
\begin_inset Formula $PanelVertical$
\end_inset

, se ha de hacer doble click en el recuadro creado, tras lo cual se actualizará
 el entorno a la edición del 
\begin_inset Formula $PanelVertical$
\end_inset

.
 
\begin_inset Newline newline
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/3.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 3
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 3"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En la captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 5"
plural "false"
caps "false"
noprefix "false"

\end_inset

 ya se han añadido algunos paneles al 
\begin_inset Formula $PanelVertical$
\end_inset

.
 Como podemos ver, aparecen sin nombre, pero podemos asignarle uno buscando
 el campo 
\begin_inset Formula $name$
\end_inset

 del recuadro 
\begin_inset Formula $Properties$
\end_inset

 debajo de la paleta.
 
\begin_inset Formula $Netbeans$
\end_inset

 no actualiza la visualización de manera inmediata, pero en la siguiente
 interacción con el diseñador, aparecerá el nombre adecuadamente.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/4.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 4
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 4"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
En la captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 6"
plural "false"
caps "false"
noprefix "false"

\end_inset

 podemos ver que hemos hecho lo propio con un 
\begin_inset Formula $PanelPestañas$
\end_inset

.
 Y en la captura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:IntegraciónNetbeans - 7"
plural "false"
caps "false"
noprefix "false"

\end_inset

 con un 
\begin_inset Formula $PanelÁrbol$
\end_inset

.
 Sin embargo, la integración de éste último no es perfecta, ya que 
\begin_inset Formula $Netbeans$
\end_inset

 no edita paneles que no se hayen vacíos, por lo que debemos añadir sus
 nodos manualmente.
\begin_inset Newline newline
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/5.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 5
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 5"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Finalmente, para integrar el nuevo panel a la librería se ha de aportar
 una clase que implemente la interfaz 
\begin_inset Formula $Configurador$
\end_inset

 (puede ser el mismo panel) e implementar los métodos que aparecen en 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Métodos"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/6.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 6
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 6"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/7.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - 7
\begin_inset CommandInset label
LatexCommand label
name "fig:IntegraciónNetbeans - 7"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename IntegraciónNetbeans/8.png
	lyxscale 50
	scale 25

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Integración Netbeans - Métodos
\begin_inset CommandInset label
LatexCommand label
name "fig:Métodos"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
