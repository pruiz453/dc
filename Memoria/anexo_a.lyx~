#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\usepackage{algorithmic}
\usepackage{algorithm}
\floatname{algorithm}{Algoritmo}
\floatname{table}{Tabla}

\usepackage{url}
\usepackage{hyperref}
\usepackage{fancyhdr}
\fancyhf{}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\makeatletter
\let\sv@endpart\@endpart
\def\@endpart{\thispagestyle{empty}\sv@endpart}
\makeatother
\end_preamble
\use_default_options true
\master maestro-pfg.lyx
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Índice
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
renewcommand{
\backslash
appendixname}{Anexo}
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Caracteres no ASCII en el código
\end_layout

\begin_layout Standard
<...>
\end_layout

\begin_layout Section
Problema y Solución
\end_layout

\begin_layout Standard
El código con caracteres no ASCII como tildes o ñ puede ser conflictivo
 con respecto la internacionalización del proyecto (¿Cómo invocar el método
 clase.tamaño() si mi teclado no tiene ñ?).
 Esta pequeña vicisitud puede ser soslayada con un sencillo script o programa
 que partiendo del directorio raíz recorra los archivos fuente sustituyendo
 á -> a, Á -> A, ñ -> n...
 y así sucesivamente.
 Aunque ésta no es la forma más eficiente de hacerlo, el siguiente sencillo
 script quitarTildes en bash eliminaría dichos caracteres del código: 
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "language=Java,float=p,numbers=left,basicstyle={\ttfamily},breaklines=true,tabsize=4"
inline false
status open

\begin_layout Plain Layout

\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "lis:Codigo-de-edit"

\end_inset

Código de quitarTildes
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

     #!/bin/bash
\end_layout

\begin_layout Plain Layout

	 if [ -d "$1" ]; then
\end_layout

\begin_layout Plain Layout

	 for i in ./$1/*; 
\end_layout

\begin_layout Plain Layout

		do 
\end_layout

\begin_layout Plain Layout

			quitarTildes $i;
\end_layout

\begin_layout Plain Layout

		done
\end_layout

\begin_layout Plain Layout

 	else
\end_layout

\begin_layout Plain Layout

		sed -i 's/á/a/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Á/A/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/é/e/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/É/e/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/í/i/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Í/I/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ó/o/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ó/O/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ú/u/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ú/U/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/ñ/n/g' $1
\end_layout

\begin_layout Plain Layout

		sed -i 's/Ñ/N/g' $1
\end_layout

\begin_layout Plain Layout

	fi
\end_layout

\end_inset


\end_layout

\end_body
\end_document
